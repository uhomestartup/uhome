/**
 * Development environment settings
 *
 * This file can include shared settings for a development team,
 * such as API keys or remote database passwords.  If you're using
 * a version control solution for your Sails app, this file will
 * be committed to your repository unless you add it to your .gitignore
 * file.  If your repository will be publicly viewable, don't add
 * any private information to this file!
 *
 */

module.exports = {

  /***************************************************************************
   * Set the default database connection for models in the development       *
   * environment (see config/connections.js and config/models.js )           *
   ***************************************************************************/

  models: {
      connection: 'developerUhomeMongoDb'
  },

    facebookStrategyConf: {
        clientID: "224035918000164",
        clientSecret: "17245b66e92ad5bf5c91cd2166f40e14",
        callbackURL: "http://uhomelocal.com:1337/auth/facebook/callback",
        scope : [ 'public_profile', 'email' ],
        profileFields: ['id', 'first_name', 'last_name', 'email', 'gender', 'picture']
    },

    googleStrategyConf: {
        clientID: '624478516859-f8pscl5t5g2fu5rlq7t1b2h142c3p869.apps.googleusercontent.com',
        clientSecret: 'nSaN8gjvuHH3Ka4Qjx8e8bJH',
        callbackURL: 'http://uhomelocal.com:1337/auth/google/callback',

    },

    hostname : 'http://uhomelocal.com:1337/'


};
