/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes map URLs to views and controllers.
 *
 * If Sails receives a URL that doesn't match any of the routes below,
 * it will check for matching files (images, scripts, stylesheets, etc.)
 * in your assets directory.  e.g. `http://localhost:1337/images/foo.jpg`
 * might match an image file: `/assets/images/foo.jpg`
 *
 * Finally, if those don't match either, the default 404 handler is triggered.
 * See `api/responses/notFound.js` to adjust your app's 404 logic.
 *
 * Note: Sails doesn't ACTUALLY serve stuff from `assets`-- the default Gruntfile in Sails copies
 * flat files from `assets` to `.tmp/public`.  This allows you to do things like compile LESS or
 * CoffeeScript for the front-end.
 *
 * For more information on configuring custom routes, check out:
 * http://sailsjs.org/#!/documentation/concepts/Routes/RouteTargetSyntax.html
 */

module.exports.routes = {

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` (or `views/homepage.jade`, *
  * etc. depending on your default view engine) your home page.              *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/

  // General Routes
  'GET /': 'PageController.showHomePage',

  // Signup Form
  'GET /signup': {view: 'common/signup'},

  // Login Form
  'GET /login': {view: 'common/login'},

  // Login Submit
  'POST /login': 'PageController.login',

  // Logout
  'GET /logout': 'PageController.logout',

  // Resend activation email
  'POST /resendEmail': 'ResendEmailController.resendEmail',

  // Signup
  'POST /signup': 'SignupController.signup',

  // Recovery page
  'GET /recovery': {view: 'common/recovery'},

  // Recovery POST
  'POST /recovery': 'PageController.recovery',

  // Recovery Callback
  'GET /recover': 'PageController.recover',

  // Recovery Callback
  'POST /resetpassword': 'PageController.resetpassword',

  // Gets all services
  'GET /service': 'ServiceController.get',

  // Gets zipcode details
  'POST /getzipcodedetails': 'UserController.zipcode',

  // Public profile page URL
  'GET /publicpage/:id': 'PageController.publicProfile',

  // Error`s Routes

  // Invitation deprecated
  'GET /invitDepr': {view: 'common/invitationDeprecated'},


  // Contractor's Routes

  // Registration Routes
  // Profile completion URL
  'GET /bregister': 'Contractor.register',

  'POST /getLangWLinesAccr': 'Contractor.getLanguagesWorklinesAccreditationTypes',

  // Profile completion URL
  'POST /bcompleteprofile': 'Contractor.activateAccount',

  // Profile page URL
  'GET /speditprofile': 'Contractor.editProfilePage',

  // Our team page URL
  'GET /ourteam': 'Contractor.ourteam',

  // Reviews page URL
  'GET /spreviews': 'Contractor.reviews',

  // Our projects page URL
  'GET /ourprojects': 'Contractor.ourprojects',

  // Our projects page URL
  'POST /addBBBUrl': 'Contractor.addBBBUrl',

  // Our projects page URL
  'POST /addYearFounded': 'Contractor.addYearFounded',

  // Our projects page URL
  'POST /addLanguages': 'Contractor.addLanguages',

  // Our projects page URL
  'POST /addLicense': 'Contractor.addLicense',

  // Our projects page URL
  'POST /addServicesOffered': 'Contractor.addServicesOffered',

  'POST /addTeamMember': 'Contractor.addTeamMember',

  'GET /editTeam': 'Contractor.editTeam',

  'POST /deleteTeam': 'Contractor.deleteTeam',

  'POST /updateContractorProfile': 'Contractor.updateProfile',

  'GET /myjobs': 'Contractor.myjobs',

  'GET /spprojects': 'Contractor.spprojects',

  'GET /spmessages': 'Contractor.spmessages',

  'GET /spreviews': 'Contractor.spreviews',

  'GET /spads': 'Contractor.spads',

  'GET /spverifications': 'Contractor.spverifications',

  'GET /spbadges': 'Contractor.spbadges',

  'GET /sprecommendations': 'Contractor.sprecommendations',





  // Service Seeker's Routes
  // Email Return URL
  'GET /ssactivate': 'Serviceseeker.register',

  'POST /profileComplete': 'Serviceseeker.activateAccount',

  'POST /getSerSubSerStartTime': 'Serviceseeker.getServiceSubServiceStartTimeForSSProject',

  // Update user profile

  'POST /updateProfile': 'Serviceseeker.update',

  // Profile page URL
  'GET /sseditprofile': 'Serviceseeker.editProfilePage',

    // Messages page URL
  'GET /messages': 'Serviceseeker.messages',

  // My Projects URL/ Get All Projects
  'GET /ssmyprojects': 'Serviceseeker.myProjects',

  // Gets Sub-services by Serivce ID
  'POST /getSubServicesByService': 'SubService.getSubServicesByService',

  // Add residence year
  'POST /addResidenceYear': 'Serviceseeker.addResidenceYear',

  // Loads gallery view
  'GET /gallery': 'Serviceseeker.gallery',

  // Loads gallery view
  'GET /recommendations': 'Serviceseeker.recommendations',

  // Loads gallery view
  'GET /groups': 'Serviceseeker.groups',

  'GET /findBusiness': 'Serviceseeker.findBusiness',

  'GET /newsFeed': 'Serviceseeker.newsFeed',

  'GET /projectDetails/:id': 'Serviceseeker.projectDetails',

  // Projects routes
  'GET /newProject': 'Project.newProjectPage',

  'GET /editProject/:id': 'Project.editProjectPage',

  'POST /createProject': 'Project.createProject',

  'POST /updateProject': 'Project.updateProject',

  'POST /deleteProject': 'Project.deleteProject',

  'GET /getAllProjects': 'Project.getAllProjects',

  'GET /projectReview/:id': 'PageController.publicProjectPage'


  /***************************************************************************
  *                                                                          *
  * Custom routes here...                                                    *
  *                                                                          *
  * If a request to a URL doesn't match any of the custom routes above, it   *
  * is matched against Sails route blueprints. See `config/blueprints.js`    *
  * for configuration options and examples.                                  *
  *                                                                          *
  ***************************************************************************/

};
