/**
 * Default model configuration
 * (sails.config.models)
 *
 * Unless you override them, the following properties will be included
 * in each of your models.
 *
 * For more info on Sails models, see:
 * http://sailsjs.org/#!/documentation/concepts/ORM
 */

module.exports.models = {

  /***************************************************************************
  *                                                                          *
  * Your app's default connection. i.e. the name of one of your app's        *
  * connections (see `config/connections.js`)                                *
  *                                                                          *
  ***************************************************************************/
  //connection: 'someMongodbServer',

  models: {
      connection: 'defaultMongodbServer'
  },

    facebookStrategyConf: {
        clientID: "1202154256547054",
        clientSecret: "23a80d2f2935e9046179d93fa8ab348c",
        callbackURL: "http://localhost:1337/auth/facebook/callback",
        scope : [ 'public_profile', 'email' ],
        profileFields: ['id', 'first_name', 'last_name', 'email', 'gender', 'picture']
    },

    googleStrategyConf: {
        clientID: '624478516859-f8pscl5t5g2fu5rlq7t1b2h142c3p869.apps.googleusercontent.com',
        clientSecret: 'nSaN8gjvuHH3Ka4Qjx8e8bJH',
        callbackURL: 'http://localhost:1337/auth/google/callback',
    },

    hostname : 'http://localhost:1337/',

  /***************************************************************************
  *                                                                          *
  * How and whether Sails will attempt to automatically rebuild the          *
  * tables/collections/etc. in your schema.                                  *
  *                                                                          *
  * See http://sailsjs.org/#!/documentation/concepts/ORM/model-settings.html  *
  *                                                                          *
  ***************************************************************************/
  migrate: 'alter'

};
