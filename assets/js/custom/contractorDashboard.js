$(document).ready(function() {

  $("#submitBBBUrl").click(function(e) {
    
    if ($("#bbbLink").val() == '') {
      toastr.error("Can't submit empty URL", 'Error');
      return;
    }

    $.post('./addBBBUrl', {
      url: $("#bbbLink").val()
    }, function(data) {
      if (data.msg === "Updated") {
        toastr.success(data.msg, 'Success');
        $('#BBBModal').modal('toggle');
        $(".bbbURLContainer").addClass('hidden');
        return true;
      } else {
        toastr.error(data.msg, 'Error');
        return false;
      }
    }).fail(function(err) {
      toastr.error(err.msg, 'Error');
      return false;
    });
  });

  $("#yearFoundedSubmit").click(function(e) {
    
    if ($("#yearFounded").val() == '') {
      toastr.error("Can't submit empty URL", 'Error');
      return;
    }

    $.post('./addYearFounded', {
      year: $("#yearFounded").val()
    }, function(data) {
      if (data.msg === "Updated") {
        toastr.success(data.msg, 'Success');
        $('#yearModal').modal('toggle');
        $(".yearFoundedContainer").addClass('hidden');
        return true;
      } else {
        toastr.error(data.msg, 'Error');
        return false;
      }
    }).fail(function(err) {
      toastr.error(err.msg, 'Error');
      return false;
    });
  });

  $("#licensingSubmit").click(function(e) {
    
    if ($("#states").val() == '') {
      toastr.error("Please select a state", 'Error');
      return;
    }
    
    if ($("#accreditation").val() == '') {
      toastr.error("Please select an accreditation", 'Error');
      return;
    }
    
    if ($("#accreditation_number").val() == '') {
      toastr.error("Please enter accreditation number", 'Error');
      return;
    }
    
    if ($("#expiration").val() == '') {
      toastr.error("Please enter the expiration date", 'Error');
      return;
    }

    $.post('./addLicense', {
      states: $("#states").val(),
      accreditation: $("#accreditation").val(),
      accreditation_number: $("#accreditation_number").val(),
      expiration: $("#expiration").val()
    }, function(data) {
      if (data.msg === "Updated") {
        toastr.success(data.msg, 'Success');
        $('#licensingModal').modal('toggle');
        $(".licenseContainer").addClass('hidden');
        return true;
      } else {
        toastr.error(data.msg, 'Error');
        return false;
      }
    }).fail(function(err) {
      toastr.error(err.msg, 'Error');
      return false;
    });
  });

  $("#languageSubmit").click(function(e) {
    var languages = $(".tags.eachLanguage").text().split(' ');
    languages.pop();

    if (languages.length < 1) {
      toastr.error('Please select atleast one language!', 'Error');
      return false;;
    }

    $.post('./addLanguages', {
      languages: languages
    }, function(data) {
      if (data.msg === "Updated") {
        toastr.success(data.msg, 'Success');
        $('#languageModal').modal('toggle');
        $(".languagesContainer").addClass('hidden');
        return true;
      } else {
        toastr.error(data.msg, 'Error');
        return false;
      }
    }).fail(function(err) {
      toastr.error(err.msg, 'Error');
      return false;
    });
  });

  $("#languages").change(function(e) {
    var currentValue = $(this).val();
    $(".languagesTagContainer").append('<span class="tags eachLanguage font-size-12 font-opensans text-white radius-50 padding-y-7 bg-theme-blue padding-x-10 margin-right-5 '+currentValue+'">'+currentValue+' <i class="fa fa-times removeLanguage" data-id="'+currentValue+'"></i></span>');
    $("#languages option[value='"+currentValue+"']").hide();
    removeLanguageItem();
  });

  $("#service").change(function(e) {
    var currentValue = $(this).val();
    var currentValueText = $("#service option:selected").text();
    $(".serviceTagContainer").append('<span class="tags eachService font-size-12 font-opensans text-white radius-50 padding-y-7 bg-theme-blue padding-x-10 margin-right-5 '+currentValue+'" data-id="'+currentValue+'">'+currentValueText+' <i class="fa fa-times removeService" data-id="'+currentValue+'" data-value="'+currentValueText+'"></i></span>');
    $("#service option[value='"+currentValue+"']").hide();
    removeServiceItem();
  });

  $("#serviceSubmit").click(function(e) {
    var services = [];
    $.each($(".eachService"), function(key, service) {
      services[key] = $(service).data('id');
    });

    if (services.length < 1) {
      toastr.error('Please select atleast one service!', 'Error');
      return false;;
    }

    $.post('./addServicesOffered', {
      services: services
    }, function(data) {
      if (data.msg === "Updated") {
        toastr.success(data.msg, 'Success');
        $('#serviceModal').modal('toggle');
        $(".servicesOfferedContainer").addClass('hidden');
        return true;
      } else {
        toastr.error(data.msg, 'Error');
        return false;
      }
    }).fail(function(err) {
      toastr.error(err.msg, 'Error');
      return false;
    });
  });

  $('.date-own').datepicker({
    minViewMode: 2,
    format: 'yyyy',
    endDate: '+0y',
    autoclose: true
  });

  //$('#calendar').fullCalendar({
  //  header: {
  //    right: 'next',
  //    center: 'title',
  //    left: 'prev'
  //  }
  //});
  //$("#calendar").fullCalendar('today');
});

function removeLanguageItem() {
  $(".removeLanguage").unbind('click').on('click', function(e) {
    var currentValue = $(this).data('id');
    $("#languages option[value='"+currentValue+"']").show();
    $("."+currentValue).remove();
  });
}

function removeServiceItem() {
  $(".removeService").unbind('click').on('click', function(e) {
    var currentValue = $(this).data('id');
    $("#service option[value='"+currentValue+"']").show();
    $("."+currentValue).remove();
  });
}