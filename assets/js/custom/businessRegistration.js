//$(document).ready(function () {
//
//    $('.date-own').datepicker({
//      minViewMode: 2,
//      format: 'yyyy',
//      autoclose: true
//    });
//
//    $('#calendar').fullCalendar({
//      defaultDate: '2016-06-12',
//      editable: true,
//      header: {
//        right: 'next',
//        center: 'title',
//        left: 'prev'
//      }
//    });
//
//    $.each($(".business-days"), function (index, value) {
//      if ($(this).children(".weekday").val() != 'Close') {
//        $(this).children(".day").css('background-color', '#f07246');
//      }
//    });
//
//    $(".weekday").change(function () {
//      var day = $(this).data('id');
//      if ($(this).val() == "Close") {
//        $("." + day).css('background-color', '#bdbdbd');
//        $("." + day + "-end").addClass('hidden');
//      } else {
//        $("." + day).css('background-color', '#f07246');
//        $("." + day + "-end").removeClass('hidden');
//      }
//    });
//
//    $(".btn-company").click(function () {
//      $(".btn-company").addClass('active');
//      $(".btn-user").removeClass('active');
//      $(".btn-company img").attr('src', '/images/signup/company-selected.png');
//      $(".btn-user img").attr('src', '/images/signup/user.png');
//      $(".btn-companyRadio").prop('checked', 'checked');
//      $(".btn-userRadio").prop('checked', false);
//      $(".profileImageContiner").removeClass('col-md-6').addClass('col-md-3');
//      $(".logoImageContainer").removeClass('col-md-6').addClass('col-md-3');
//      return false;
//    });
//
//    $(".btn-user").click(function () {
//      $(".btn-user").addClass('active');
//      $(".btn-company").removeClass('active');
//      $(".btn-user img").attr('src', '/images/signup/user-selected.png');
//      $(".btn-company img").attr('src', '/images/signup/company.png');
//      $(".btn-userRadio").prop('checked', 'checked');
//      $(".btn-companyRadio").prop('checked', false);
//      $(".profileImageContiner").removeClass('col-md-3').addClass('col-md-6');
//      $(".logoImageContainer").removeClass('col-md-6').addClass('col-md-3 hidden');
//      return false;
//    });
//
//    $("#start-registration").click(function () {
//      if (
//        !(
//          $(".btn-user").hasClass('active') ||
//          $(".btn-company").hasClass('active')
//        )
//      ) {
//        toastr.error('You need to select an option!');
//        return false;
//      } else {
//        if ($(".btn-user").hasClass('active')) {
//          $(".signup-bodys").addClass('hidden');
//          $(".signup-body-step2-user").removeClass('hidden');
//          return false;
//        } else {
//          $(".signup-bodys").addClass('hidden');
//          $(".signup-body-step2-company").removeClass('hidden');
//          return false;
//        }
//      }
//    });
//
//    $("#step2-company-submit").click(function () {
//      var error = false;
//      if ($("input[name=companyFirstName]").val() == "") {
//        error = true;
//        var msg = 'Missing first name!';
//      } else if ($("input[name=companyLastName]").val() == "") {
//        error = true;
//        var msg = 'Missing last name!';
//      } else if ($("input[name=companyStreetAddress]").val() == "") {
//        error = true;
//        var msg = 'Missing street address!';
//      } else if ($("input[name=companyUnit]").val() == "") {
//        error = true;
//        var msg = 'Missing unit #!';
//      } else if ($("input[name=companyBusinessName]").val() == "") {
//        error = true;
//        var msg = 'Missing business name!';
//      } else if ($("input[name=companyZipcode]").val() == "" || $("input[name=companyZipcodeStatus]").val() == 'false') {
//        error = true;
//        var msg = 'Invalid/Missing zipcode!';
//      } else if ($("input[name=companyCity]").val() == "") {
//        error = true;
//        var msg = 'Missing city name!';
//      } else if ($("input[name=companyState]").val() == "") {
//        error = true;
//        var msg = 'Missing state!';
//      } else if ($("input[name=companyBusinessPhone]").val() == "") {
//        error = true;
//        var msg = 'Missing business phone!';
//      } else if ($("input[name=companyBusinessEIN]").val() == "" || !(/[\d]{2}\-[\d]{7}/.test($("input[name=companyBusinessEIN]").val()))) {
//        error = true;
//        var msg = 'Missing/Invalid business EIN!';
//      } else if ($("input[name=companyHourlyRate]").val() == "") {
//        error = true;
//        var msg = 'Missing hourly rates!';
//      } else if ($("input[name=companyWebsiteURL]").val() == "" || !isUrlValid($("input[name=companyWebsiteURL]").val())) {
//        error = true;
//        var msg = 'Invalid/Missing website url!';
//      } else if ($("select[name=companySunStart]").val() == "") {
//        error = true;
//        var msg = 'Missing Sunday start time!';
//      } else if ($("select[name=companyMonStart]").val() == "") {
//        error = true;
//        var msg = 'Missing Monday start time!';
//      } else if ($("select[name=companyTueStart]").val() == "") {
//        error = true;
//        var msg = 'Missing Tuesday start time!';
//      } else if ($("select[name=companyWedStart]").val() == "") {
//        error = true;
//        var msg = 'Missing Wednesday start time!';
//      } else if ($("select[name=companyThuStart]").val() == "") {
//        error = true;
//        var msg = 'Missing Thursday start time!';
//      } else if ($("select[name=companyFriStart]").val() == "") {
//        error = true;
//        var msg = 'Missing Friday start time!';
//      } else if ($("select[name=companySatStart]").val() == "") {
//        error = true;
//        var msg = 'Missing Saturday start time!';
//      } else if ($("select[name=companySunEnd]").val() == "") {
//        error = true;
//        var msg = 'Missing Sunday end time!';
//      } else if ($("select[name=companyMonEnd]").val() == "") {
//        error = true;
//        var msg = 'Missing Monday end time!';
//      } else if ($("select[name=companyTueEnd]").val() == "") {
//        error = true;
//        var msg = 'Missing Tuesday end time!';
//      } else if ($("select[name=companyWedEnd]").val() == "") {
//        error = true;
//        var msg = 'Missing Wednesday end time!';
//      } else if ($("select[name=companyThuEnd]").val() == "") {
//        error = true;
//        var msg = 'Missing Thursday end time!';
//      } else if ($("select[name=companyFriEnd]").val() == "") {
//        error = true;
//        var msg = 'Missing Friday end time!';
//      } else if ($("select[name=companySatEnd]").val() == "") {
//        error = true;
//        var msg = 'Missing Saturday end time!';
//      }
//
//      if (error) {
//        toastr.error(msg, 'Error');
//        return false;
//      }
//      $(".signup-body-step2-company").addClass('hidden');
//      $(".signup-body-step3").removeClass('hidden');
//      return false;
//    });
//
//    $("#step2-user-submit").click(function () {
//      var error = false;
//      if ($("input[name=userFirstName]").val() == "") {
//        error = true;
//        var msg = 'Missing first name!';
//      } else if ($("input[name=userLastName]").val() == "") {
//        error = true;
//        var msg = 'Missing last name!';
//      } else if ($("input[name=userStreetAddress]").val() == "") {
//        error = true;
//        var msg = 'Missing street address!';
//      } else if ($("input[name=userZipcode]").val() == "" || $("input[name=userZipcodeStatus]").val() == 'false') {
//        error = true;
//        var msg = 'Invalid/Missing zipcode!';
//      } else if ($("input[name=userCity]").val() == "") {
//        error = true;
//        var msg = 'Missing city name!';
//      } else if ($("input[name=userState]").val() == "") {
//        error = true;
//        var msg = 'Missing state!';
//      } else if ($("input[name=userBusinessPhone]").val() == "") {
//        error = true;
//        var msg = 'Missing business phone!';
//      } else if ($("input[name=userHourlyRate]").val() == "") {
//        error = true;
//        var msg = 'Missing hourly rates!';
//      } else if ($("input[name=userWebsiteURL]").val() == "" || !isUrlValid($("input[name=userWebsiteURL]").val())) {
//        error = true;
//        var msg = 'Invalid/Missing website url!';
//      } else if ($("select[name=userSunStart]").val() == "") {
//        error = true;
//        var msg = 'Missing Sunday start time!';
//      } else if ($("select[name=userMonStart]").val() == "") {
//        error = true;
//        var msg = 'Missing Monday start time!';
//      } else if ($("select[name=userTueStart]").val() == "") {
//        error = true;
//        var msg = 'Missing Tuesday start time!';
//      } else if ($("select[name=userWedStart]").val() == "") {
//        error = true;
//        var msg = 'Missing Wednesday start time!';
//      } else if ($("select[name=userThuStart]").val() == "") {
//        error = true;
//        var msg = 'Missing Thursday start time!';
//      } else if ($("select[name=userFriStart]").val() == "") {
//        error = true;
//        var msg = 'Missing Friday start time!';
//      } else if ($("select[name=userSatStart]").val() == "") {
//        error = true;
//        var msg = 'Missing Saturday start time!';
//      } else if ($("select[name=userSunEnd]").val() == "") {
//        error = true;
//        var msg = 'Missing Sunday start time!';
//      } else if ($("select[name=userMonEnd]").val() == "") {
//        error = true;
//        var msg = 'Missing Monday end time!';
//      } else if ($("select[name=userTueEnd]").val() == "") {
//        error = true;
//        var msg = 'Missing Tuesday end time!';
//      } else if ($("select[name=userWedEnd]").val() == "") {
//        error = true;
//        var msg = 'Missing Wednesday end time!';
//      } else if ($("select[name=userThuEnd]").val() == "") {
//        error = true;
//        var msg = 'Missing Thursday end time!';
//      } else if ($("select[name=userFriEnd]").val() == "") {
//        error = true;
//        var msg = 'Missing Friday end time!';
//      } else if ($("select[name=userSatEnd]").val() == "") {
//        error = true;
//        var msg = 'Missing Saturday end time!';
//      }
//      if (error) {
//        toastr.error(msg, 'Error');
//        return false;
//      }
//      $(".signup-body-step2-user").addClass('hidden');
//      $(".signup-body-step3").removeClass('hidden');
//      return false;
//    });
//
//    $("#step3-submit").click(function () {
//      var error = false;
//      if ($("textarea[name=aboutUs]").val() == "") {
//        error = true;
//        var msg = 'Missing about us description!';
//      }
//      if (error) {
//        toastr.error(msg, 'Error');
//        return false;
//      }
//      $(".signup-body-step3").addClass('hidden');
//      $(".signup-body-step4").removeClass('hidden');
//      return false;
//    });
//
//    $(".cover-banner-upload").click(function () {
//      $("#coverBanner").trigger('click');
//    });
//
//    $(".logo-upload").click(function () {
//      $("#logoPhoto").trigger('click');
//    });
//
//    $(".profile-upload").click(function () {
//      $("#profilePhoto").trigger('click');
//    });
//
//    $("#emailHidden").attr('ng-init', "bregisterForm.email='"+getURLParameter('email')+"'");
//    $("#tokenHidden").attr('ng-init', "bregisterForm.token='"+getURLParameter('token')+"'");
//});
//
//function getURLParameter(name) {
//	return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [null, ''])[1].replace(/\+/g, '%20')) || null;
//}
//function isUrlValid(url) {
//	return /(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(url);
//}