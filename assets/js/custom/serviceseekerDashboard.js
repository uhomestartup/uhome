$(document).ready(function() {

  $("#addResidenceYear").click(function(e) {
    
    if ($("#residenceYear").val() == '') {
      toastr.error("Please select an year", 'Error');
      return;
    }

    $.post('./addResidenceYear', {
      year: $("#residenceYear").val()
    }, function(data) {
      if (data.msg === "Updated") {
        toastr.success(data.msg, 'Success');
        $('#exampleModal').modal().hide();
        $(".resident, .resident-height").addClass('hidden');
        $(".modal-backdrop").remove();
        return true;
      } else {
        toastr.error(data.msg, 'Error');
        return false;
      }
    }).fail(function(err) {
      toastr.error(err.msg, 'Error');
      return false;
    });
  });

  $('.date-own').datepicker({
    minViewMode: 2,
    format: 'yyyy',
    autoclose: true,
    endDate: '+0y'
  });
  $(function () {
    $('[data-toggle="tooltip"]').tooltip()
  });
  $('#myTabs a').click(function (e) {
    e.preventDefault()
    $(this).tab('show')
  });

  $(".timeline-icon").click(function() {
    $(".page-content").removeClass('hidden').css('display','block');
  });
  $(".notification-icon").click(function() {
    $(".message-body, .page-content, .search-body, .right-panel-body").addClass('hidden').css('display','none');
    $(".notification-body").removeClass('hidden').css('display','block');
  });
  $(".messages-icon").click(function() {
    $(".notification-body, .page-content, .search-body, .right-panel-body").addClass('hidden').css('display','none');
    $(".message-body").removeClass('hidden').css('display','block');
  });
  $(".search-icon").click(function() {
    $(".notification-body, .page-content, .message-body, .right-panel-body").addClass('hidden').css('display','none');
    $(".search-body").removeClass('hidden').css('display','block');
  });
  $(".right-panel").click(function() {
    $(".notification-body, .page-content, .message-body, .search-body").addClass('hidden').css('display','none');
    $(".right-panel-body").removeClass('hidden').css('display','block');
  });
  $(".message-body li").mouseover(function() {
    $(".message-body").children('li').removeClass('active');
    $(this).addClass('active');
  });
  $(".message-body li").mouseleave(function() {
    $(".message-body li").removeClass('active');
  });
  $("#mini-search-box").click(function() {
    $(".mini-dialoug").removeClass('hidden');
  });
  $(".mini-dialoug").mouseleave(function() {
    $(".mini-dialoug").addClass('hidden');
  });
  $(".mobile-icons").click(function() {
    $(".mobile-icons").removeClass('active');
    $(this).addClass('active');
  });
  $(".notification-body, .message-body, .search-body, .right-panel-body").addClass('hidden').css('display','none');
});