'use strict';
App.factory('datepicker',
    ['$http','$location',
        function ( $http,$location) {

            let date = {};

            date.profileDatepicker = function () {
                $('.date-own').datepicker({
                    timepicker: false,
                    minViewMode: 2,
                    format: 'yyyy',
                    autoclose: true,
                    endDate: "today"
                });
            };


            return date;
        }
    ]
);