'use strict';
App.factory('api',
    ['$http','$location', '$rootScope', 'config', 'toastr',
        function ( $http,$location, $rootScope, config, toastr) {
            var that                = {};
            //var isStartRefreshing   = false;
            //var refreshCalbacks     = [];
            //that.tokenId            = $location.search()['id'];
            //that.user               = { empty: true};
            //that.linkAuto           = {};
            //that.tokenInvitation    = localStorage.getItem("tokenInvitation");
            //that.url                = '';
            //that.periodPublish      = '';
            //that.videoShowLibrari   = true;
            //that.playListShowLibrari = false;
            //var initListeners       = [];

            //angular.element('.mainNext').removeClass('displayNone');
            //
            //function callInitListeners(){
            //    initListeners.forEach(function(item){ item()});
            //}
            //
            //that.initProfile = function(callback){
            //    that.user.empty ? initListeners.push(callback) : callback();
            //};
            //
            //that.logout = function(){
            //    sessionStorage.setItem('zoom','');
            //    localStorage.setItem('refresh_token','');
            //    localStorage.setItem('access_token', '');
            //    that.user = {};
            //};
            //
            //that.login = function (callback) {
            //    $http.get(API_URL.auth+'/oauth2/authorize/youtube')
            //        .success(function (response) {
            //            window.location = response['authorise_url'];
            //        }).error(function(err){
            //            if(callback) callback(err);
            //        });
            //};
            //
            //that.callbackUri = function (callbackUri, callback) {
            //    if (callbackUri != undefined) {
            //        if(!that.tokenInvitation){
            //            that.url = API_URL.auth+'/oauth2/token/youtube?code='+callbackUri;
            //        }else{
            //            that.url = API_URL.auth+'/oauth2/token/youtube?code='+callbackUri+'&token='+that.tokenInvitation;
            //        }
            //        $http({
            //            "async": true,
            //            "crossDomain": true,
            //            method: 'POST',
            //            url: that.url,
            //            data:''
            //        }).success(function (response) {
            //            response['status']= true;
            //            localStorage.setItem('refresh_token', response['refresh_token']);
            //            localStorage.setItem('access_token', response['access_token']);
            //            localStorage.setItem('tokenInvitation', '');
            //            callback(response);
            //        }).error(function (err) {
            //            $rootScope.checkPreloaderBlock = false;
            //            $rootScope.errorLoginYoutube = true;
            //            $rootScope.errorMassege = err.message;
            //        });
            //    }
            //};
            //
            //that.refreshingToken = function(callback){
            //    refreshCalbacks.push(callback);
            //    if(isStartRefreshing){
            //        return;
            //    } else {
            //        isStartRefreshing =true;
            //    }
            //    $http({
            //        "async": true,
            //        "crossDomain": true,
            //        method: 'GET',
            //        url:  API_URL.auth+'/oauth/v2/token?'+$.param({'grant_type':'refresh_token',
            //            'refresh_token':localStorage.getItem('refresh_token'),
            //            'client_id':AUTH.client_id,
            //            'client_secret':AUTH.client_secret})
            //    }).success(function (response) {
            //        localStorage.setItem('refresh_token', response['refresh_token']);
            //        localStorage.setItem('access_token', response['access_token']);
            //        isStartRefreshing = false;
            //        refreshCalbacks.forEach(function(action){action()});
            //    }).error(function(data, status){
            //        isStartRefreshing = false;
            //    });
            //};

            that.call = function(method,url,data,done,error,isFromEncoding){
                var targetUrl = $location.path();
                var tryCount = 0;
                var params = {
                    "async": true,
                    "crossDomain": true,
                    method: method,
                    url: config.local+'/'+url
                };
                if(data){
                    params.data = data;
                }
                $http(params)
                    .then(function onSuccess(response) {
                        if(done)done(response);
                    })
                    .catch(function onError(sailsResponse) {
                        toastr.error(sailsResponse.data.msg, 'Error');
                        return;
                    });
            };

            that.get = function(url,done,error){
                that.call('GET',url,null,done,error,true);
            };

            that.post = function(url,data,done,error){
                that.call('POST',url,data,done,error,false);
            };
            that.put = function(url,data,done,error){
                that.call('PUT',url,data,done,error,false);
            };
            that.patch = function(url,data,done,error){
                that.call('PATCH',url,data,done,error,false);
            };
            that.link = function(url,data,done,error){
                that.call('LINK',url,data,done,error,false);
            };
            that.delete = function(url,data,done,error){
                that.call('DELETE',url,data,done,error,false);
            };
            that.unlink = function(url,data,done,error){
                that.call('UNLINK',url,data,done,error,false);
            };

            //that.myProfile = function(done){
            //    that.get('me', function (resp) {
            //        that.user.team_member = resp.team_member;
            //        that.user.admin = resp.admin;
            //        that.user.is_member = resp.is_member;
            //        that.user.email = resp.email;
            //        that.user.username = resp.username;
            //        that.user.sourcesImg = resp.avatar_url;
            //        that.user.empty = false;
            //        $rootScope.userTeamMember = resp.team_member;
            //        $rootScope.userAdmin = resp.admin;
            //        $rootScope.userIsMember = resp.is_member;
            //        $rootScope.avatar_url = resp.avatar ? "background-image :  url('"+API_URL.avatar_uri+resp.avatar+"')" : that.user.sourcesImg ? "background-image :  url('"+that.user.sourcesImg+"')" : "background-image : url('/img/profile_photo.png')";
            //        $rootScope.userAvatarUrl = resp.avatar_url?resp.avatar_url:resp.avatar;
            //        callInitListeners();
            //        if(done)done(resp);
            //    });
            //};
            //that.token = function(token){
            //    if(!token) return window.localStorage['access_token'];
            //    window.localStorage.setItem('access_token',token);
            //};
            //that.refreshToken = function(token){
            //    if(!token) return window.localStorage['refresh_token'];
            //    window.localStorage.setItem('refresh_token',token);
            //};
            return that;
        }]);

