App.controller('bissenesProfile',
    ['$scope', '$rootScope', '$http', 'toastr', '$q', '$location', 'config', 'businessHours', 'datepicker',
        function ($scope, $rootScope, $http, toastr, $q, $location, config, businessHours, datepicker) {
            $scope.files                            = [];
            $scope.outputSequense                   = [];
            $scope.output                           = [];
            $scope.firstName                        = '12313123';
            $scope.close                            = false;
            $scope.bregisterForm                    = [];
            $scope.isDisabled                       = false;
            $scope.serviceArray                     = [];
            $scope.userAlwaysOpen                   = false;
            $scope.companyAlwaysOpen                = false;
            $scope.bregisterForm.userAccreditation  = '';
            $scope.bregisterForm.userWorkLine       = '';
            $scope.bregisterForm.userLanguage       = '';
            $scope.searchWorkLines                  = '';
            $scope.workLinesActive                  = '';
            $scope.showSelect                       = false;
            $scope.selectElement                    = false;
            $scope.hourlyRate                       = businessHours.hourlyRate;
            $scope.states                           = businessHours.states;
            $scope.identificationCustomer           = 'business';

            datepicker.profileDatepicker();

            $scope.hoursPushInArray = function (key) {
                if (key == false) {
                    $scope.businessHours = businessHours.arrayHours;
                } else {
                    $scope.businessHours = businessHours.arrayIs24Hours;
                }
                $scope.teamForm = {
                    loading: false
                };
            };

            /*Regular*/
            $scope.firstLastNamePattern = /^[a-z]{2,65}$/i;
            $scope.zipcodePattern = /^[0-9]{0,5}$/;
            $scope.businessNamePattern = /^[a-z ]{2,65}$/i;
            $scope.expiration = /^[0-9 /\//]{0,10}$/;
            $scope.accreditationNumber = /^[a-z 0-9]{0,62}$/;
            $scope.streetAddressPattern = /^[a-z 0-9 /\// , # _ . -]{0,165}$/i;
            $scope.bbbLinkPattern = /^[a-z ]{0,65}$/i;
            $scope.WebsiteUrlPattern =  /(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i;

            /*Regular*/

            $scope.languages = function (language) {
                $http.post('/getLangWLinesAccr')
                .then(function onSuccess(response) {
                    $scope.language = response.data.resultData.languages;
                    $scope.worklines = response.data.resultData.worklines;
                    $scope.accreditations = response.data.resultData.accreditation;
                    if($scope.language.length>0){
                        $scope.language.forEach(function (item) {
                            if(item.languagesName == language){
                                $scope.bregisterForm.userLanguage = item;
                            }
                        });
                    }else{
                        $scope.bregisterForm.userLanguage = '';
                    }

                    if($scope.worklines.length>0) {
                        $scope.worklines.forEach(function (item) {
                            if (item.workLines == response.data.resultData.contractorWorkLine) {
                                $scope.workLinesActive = item.name;
                                $scope.clickButs($scope.workLinesActive);
                            }
                        });
                    }else{
                        $scope.bregisterForm.userWorkLine = '';
                    }
                    if($scope.accreditations.length>0) {
                        $scope.accreditations.forEach(function (item) {
                            if (item.name == response.data.resultData.contractorAccreditation) {
                                $scope.bregisterForm.userAccreditation = item;
                            }
                        });
                    }else{
                        $scope.bregisterForm.userAccreditation = '';
                    }
                })
                .catch(function onError(sailsResponse) {
                    toastr.error(sailsResponse.data.msg, 'Error');
                    return;
                })
            };

            $scope.showSelectFunction = function (key) {
                var input = $("#inputCategory");
                $scope.showSelect = key;
                if(key){
                    input.show().focus();
                }else{
                    input.hide();
                }
            };

            $scope.clickButs = function (name) {
                $scope.workLinesActive  = name;
                $scope.searchWorkLines  = name;
            };

            $scope.clickSelectClose = function () {
                $scope.businessHoursSunEnd = $scope.businessHoursSunEnd!='Close'?'05:00 PM':'Close';
                $scope.businessHoursMonEnd = $scope.businessHoursMonEnd!='Close'?'05:00 PM':'Close';
                $scope.businessHoursTueEnd = $scope.businessHoursTueEnd!='Close'?'05:00 PM':'Close';
                $scope.businessHoursWedEnd = $scope.businessHoursWedEnd!='Close'?'05:00 PM':'Close';
                $scope.businessHoursThuEnd = $scope.businessHoursThuEnd!='Close'?'05:00 PM':'Close';
                $scope.businessHoursFriEnd = $scope.businessHoursFriEnd!='Close'?'05:00 PM':'Close';
                $scope.businessHoursSatEnd = $scope.businessHoursSatEnd!='Close'?'05:00 PM':'Close';
            };

            $scope.allTime = function (event) {
                if(event){
                    $scope.businessHoursSunStart = $scope.businessHoursSunStart!='Close'?'24hr':'Close';
                    $scope.businessHoursMonStart = $scope.businessHoursMonStart!='Close'?'24hr':'Close';
                    $scope.businessHoursTueStart = $scope.businessHoursTueStart!='Close'?'24hr':'Close';
                    $scope.businessHoursWedStart = $scope.businessHoursWedStart!='Close'?'24hr':'Close';
                    $scope.businessHoursThuStart = $scope.businessHoursThuStart!='Close'?'24hr':'Close';
                    $scope.businessHoursFriStart = $scope.businessHoursFriStart!='Close'?'24hr':'Close';
                    $scope.businessHoursSatStart = $scope.businessHoursSatStart!='Close'?'24hr':'Close';

                    $scope.businessHoursSunEnd = $scope.businessHoursSunEnd!='Close'?'24hr':'Close';
                    $scope.businessHoursMonEnd = $scope.businessHoursMonEnd!='Close'?'24hr':'Close';
                    $scope.businessHoursTueEnd = $scope.businessHoursTueEnd!='Close'?'24hr':'Close';
                    $scope.businessHoursWedEnd = $scope.businessHoursWedEnd!='Close'?'24hr':'Close';
                    $scope.businessHoursThuEnd = $scope.businessHoursThuEnd!='Close'?'24hr':'Close';
                    $scope.businessHoursFriEnd = $scope.businessHoursFriEnd!='Close'?'24hr':'Close';
                    $scope.businessHoursSatEnd = $scope.businessHoursSatEnd!='Close'?'24hr':'Close';
                }else{
                    $scope.businessHoursSunStart = $scope.businessHoursSunStart!='Close'?'09:00 AM':'Close';
                    $scope.businessHoursMonStart = $scope.businessHoursMonStart!='Close'?'09:00 AM':'Close';
                    $scope.businessHoursTueStart = $scope.businessHoursTueStart!='Close'?'09:00 AM':'Close';
                    $scope.businessHoursWedStart = $scope.businessHoursWedStart!='Close'?'09:00 AM':'Close';
                    $scope.businessHoursThuStart = $scope.businessHoursThuStart!='Close'?'09:00 AM':'Close';
                    $scope.businessHoursFriStart = $scope.businessHoursFriStart!='Close'?'09:00 AM':'Close';
                    $scope.businessHoursSatStart = $scope.businessHoursSatStart!='Close'?'09:00 AM':'Close';

                    $scope.businessHoursSunEnd = $scope.businessHoursSunEnd!='Close'?'05:00 PM':'Close';
                    $scope.businessHoursMonEnd = $scope.businessHoursMonEnd!='Close'?'05:00 PM':'Close';
                    $scope.businessHoursTueEnd = $scope.businessHoursTueEnd!='Close'?'05:00 PM':'Close';
                    $scope.businessHoursWedEnd = $scope.businessHoursWedEnd!='Close'?'05:00 PM':'Close';
                    $scope.businessHoursThuEnd = $scope.businessHoursThuEnd!='Close'?'05:00 PM':'Close';
                    $scope.businessHoursFriEnd = $scope.businessHoursFriEnd!='Close'?'05:00 PM':'Close';
                    $scope.businessHoursSatEnd = $scope.businessHoursSatEnd!='Close'?'05:00 PM':'Close';
                }
                if (event) {
                    $scope.businessHours = businessHours.arrayIs24Hours;
                } else {
                    $scope.businessHours = businessHours.arrayHours;
                }

                $scope.teamForm = {
                    loading: false
                };
            };

            $scope.addService = function () {
                if($scope.newService==''){
                    return false;
                }
                $scope.serviceArray.push({name: $scope.newService});
                $scope.bregisterForm.serviceArray = $scope.serviceArray;
                angular.element("#serviceAdd").attr('value', '');
                $scope.newService='';
            };
            $scope.deleteService = function (key) {
                $scope.serviceArray.splice(key, 1);
            };

            $scope.servicePushInArray = function (array) {
                if(array.length>0){
                    let arrays = array.split(',');
                    arrays.forEach(function (item) {
                        $scope.serviceArray.push({name: item});
                    });
                    $scope.bregisterForm.serviceArray = $scope.serviceArray;
                }
            };

            $(".cover-banner-upload").click(function () {
                $("#coverBanner").trigger('click');
            });

            $(".profile-upload").click(function () {
                $("#profilePhoto").trigger('click');
            });

            $(".logo-upload").click(function () {
                $("#logoPhoto").trigger('click');
            });

            $scope.submitChangedProfile = function () {
                    var data = {
                        licenseState: $scope.bregisterForm.userLicenseState != undefined?$scope.bregisterForm.userLicenseState:'', //"Some licene state",
                        accreditation: $scope.bregisterForm.userAccreditation.name != undefined?$scope.bregisterForm.userAccreditation.name:'', //"Accr. data",
                        accreditationNumber: $scope.bregisterForm.userAccreditationNumber != undefined?$scope.bregisterForm.userAccreditationNumber:'', //"1342",
                        licenseExpiration: $scope.bregisterForm.userExpiration != undefined?$scope.bregisterForm.userExpiration:'', //"10/02/2018",
                        bbbLink: $scope.bregisterForm.userBbbLink != undefined?$scope.bregisterForm.userBbbLink:'' ,//"BBB link",
                        yearFounded: $scope.bregisterForm.userYearFounded != undefined?$scope.bregisterForm.userYearFounded:'', //"1990",
                        language: $scope.bregisterForm.userLanguage.languagesName != undefined?$scope.bregisterForm.userLanguage.languagesName:'', //"Russian",
                        workLine: $scope.workLinesActive?$scope.workLinesActive:'', //"Music",
                        services: $scope.bregisterForm.serviceArray != undefined?$scope.bregisterForm.serviceArray:[], //[{"name": "Sex"},{"name": "Drugs"},{"name": "Rock-n-Roll"}],
                        firstName: $scope.bregisterForm.userFirstName,
                        lastName: $scope.bregisterForm.userLastName,
                        address: $scope.bregisterForm.userStreetAddress,
                        zipcode: $scope.bregisterForm.userZipcode,
                        city: $scope.bregisterForm.userCity,
                        lat: $scope.bregisterForm.userLat,
                        lng: $scope.bregisterForm.userLng,
                        state: $scope.bregisterForm.userState,
                        phone: $('#userWorkPhone').val(),
                        hourlyRate: $scope.bregisterForm.userHourlyRate,
                        url: $scope.bregisterForm.userWebsiteURL,
                        sundayStart: $scope.businessHoursSunStart,
                        sundayEnd: $scope.businessHoursSunStart == '24hr'?'24hr':($scope.businessHoursSunStart == 'Close')?'Close':$scope.businessHoursSunEnd,
                        mondayStart: $scope.businessHoursMonStart,
                        mondayEnd: $scope.businessHoursMonStart == '24hr'?'24hr':($scope.businessHoursMonStart == 'Close')?'Close':$scope.businessHoursMonEnd,
                        tuesdayStart: $scope.businessHoursTueStart,
                        tuesdayEnd: $scope.businessHoursTueStart == '24hr'?'24hr':($scope.businessHoursTueStart == 'Close')?'Close':$scope.businessHoursTueEnd,
                        wednesdayStart: $scope.businessHoursWedStart,
                        wednesdayEnd: $scope.businessHoursWedStart == '24hr'?'24hr':($scope.businessHoursWedStart == 'Close')?'Close':$scope.businessHoursWedEnd,
                        thursdayStart: $scope.businessHoursThuStart,
                        thursdayEnd: $scope.businessHoursThuStart == '24hr'?'24hr':($scope.businessHoursThuStart == 'Close')?'Close':$scope.businessHoursThuEnd,
                        fridayStart: $scope.businessHoursFriStart,
                        fridayEnd: $scope.businessHoursFriStart == '24hr'?'24hr':($scope.businessHoursFriStart == 'Close')?'Close':$scope.businessHoursFriEnd,
                        saturdayStart: $scope.businessHoursSatStart,
                        saturdayEnd: $scope.businessHoursSatStart == '24hr'?'24hr':($scope.businessHoursSatStart == 'Close')?'Close':$scope.businessHoursSatEnd,
                        is24Hours: $scope.userAlwaysOpen,
                        aboutUs: $scope.bregisterForm.aboutUs
                    };
                if ($scope.userType == 'Business') {
                    data.businessEIN = $('#userBusinessEIN').val();
                    data.unit = $scope.bregisterForm.userUnit;
                }
                console.log(data);
                $http.post('/updateContractorProfile', data)
                .then(function onSuccess(data) {
                    toastr.info('Changes saved successfully', 'Processing');
                    console.log(data);
                    return;
                   //}
                })
                .catch(function onError(sailsResponse) {
                   toastr.error('Changes was not saved', 'Error');
                   return;
                })
                .finally(function eitherWay() {
                   $scope.bregisterForm.loading = false;
                });
            };


            angular.element('#profilePhoto').change(function() {
                toastr.info('Please wait while processing your request', 'Processing');
                if (document.getElementById('profilePhoto').files.length > 0) {
                    var closeProfilePhoto = setInterval(function(){
                        if(!$scope.statusUploadedTrue){
                            $scope.files.push(uploadImage(document.getElementById('profilePhoto').files[0], 'profilePhoto'));
                            $scope.statusUploadedTrue = true;
                            $q.all($scope.files)
                                .then(function (response) {
                                    console.log(response);
                                    $scope.respInfo = response;
                                    $scope.respInfo.forEach(function(item){
                                        if(item.data.imageType == 'profilePhoto'){
                                            $scope.profilePhoto = item.data.url;
                                            $scope.statusUploadedTrue = false;
                                            clearInterval(closeProfilePhoto)
                                        }
                                    });
                                    $scope.profilePhotoGif = false;
                                }).catch(function onError(sailsResponse) {
                                    toastr.error(sailsResponse.data.msg, sailsResponse.data.toBigImageError);
                                    return;
                                });
                            $scope.outputSequense.push('profilePhoto');
                        }
                    },1000);
                }
            });

            angular.element('#logoPhoto').change(function() {
                toastr.info('Please wait while processing your request', 'Processing');
                if (document.getElementById('logoPhoto').files.length > 0) {
                    var closeImageLogo = setInterval(function(){
                        if(!$scope.statusUploadedTrue){
                            $scope.files.push(uploadImage(document.getElementById('logoPhoto').files[0], 'imageLogo'));
                            $scope.statusUploadedTrue = true;
                            $q.all($scope.files)
                                .then(function (response) {
                                    console.log(response);
                                    $scope.respInfo = response;
                                    $scope.respInfo.forEach(function(item){
                                        if(item.data.imageType == 'imageLogo'){
                                            $scope.imageLogo = item.data.url;
                                            $scope.statusUploadedTrue = false;
                                            clearInterval(closeImageLogo)
                                        }
                                    });
                                    $scope.imageLogoGif = false;
                                }).catch(function onError(sailsResponse) {
                                    toastr.error(sailsResponse.data.msg, sailsResponse.data.toBigImageError);
                                    return;
                                });
                            $scope.outputSequense.push('logoPhoto');
                        }
                    },1000);
                }
            });

            angular.element('#coverBanner').change(function() {
                toastr.info('Please wait while processing your request', 'Processing');
                if (document.getElementById('coverBanner').files.length > 0) {
                    var closeCoverBanner = setInterval(function(){
                        if(!$scope.statusUploadedTrue){
                            $scope.files.push(uploadImage(document.getElementById('coverBanner').files[0], 'coverBanner'));
                            $scope.statusUploadedTrue = true;
                            $q.all($scope.files)
                                .then(function (response) {
                                    $scope.respInfo = response;
                                    $scope.respInfo.forEach(function(item){
                                        if(item.data.imageType == 'coverBanner'){
                                            $scope.coverBanner = item.data.url;
                                            $scope.statusUploadedTrue = false;
                                            clearInterval(closeCoverBanner)
                                        }
                                    });
                                    $scope.coverBannerGif = false;
                                }).catch(function onError(sailsResponse) {
                                    toastr.error(sailsResponse.data.msg, sailsResponse.data.toBigImageError);
                                    return;
                                });
                            $scope.outputSequense.push('coverBanner');
                        }
                    },1000);
                }
            });

            var uploadImage = function (file, name) {
                if(name == 'profilePhoto'){
                    $scope.profilePhotoGif = true;
                }else if(name == 'coverBanner'){
                    $scope.coverBannerGif  = true;
                }else{
                    $scope.imageLogoGif  = true;
                }
                var url = config.local+'/upload/upload?token='+sessionStorage.getItem('token')+'&imageType='+name;
                var fd = new FormData();
                fd.append(name, file);
                return $http.post(url, fd, {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined}
                });
            };

            $scope.validateZipcode = function () {
                var zipcode = $scope.bregisterForm.userZipcode;
                $http.post('/getzipcodedetails', {
                    zipcode: zipcode
                })
                .then(function onSuccess(response) {
                    if (response.status == 200) {
                        $scope.bregisterForm.userState = response.data.state;
                        $scope.bregisterForm.userCity = response.data.city;
                        $scope.bregisterForm.userLng = response.data.lng;
                        $scope.bregisterForm.userLat = response.data.lat;
                    } else {
                        $scope.bregisterForm.userState = '';
                        $scope.bregisterForm.userCity = '';
                        $scope.bregisterForm.userLng = '';
                        $scope.bregisterForm.userLat = '';
                    }
                })
                .catch(function onError(sailsResponse) {
                    toastr.error(sailsResponse.data.msg, 'Error');
                    return;
                });
            };
        }
    ]
);