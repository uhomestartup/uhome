'use strict';

App.controller('userProfile',
    ['$scope', '$http', 'toastr', 'config', 'api',
        function($scope, $http, toastr, config, api) {
            $scope.aboutEdit                = false;
            $scope.profilePhotoGif          = false;
            $scope.coverBannerGif           = false;
            $scope.aboutUser                = '';
            $scope.files                    = [];
            $scope.myLatLng                 = {};
            $scope.lat                      = 0;
            $scope.lng                      = 0;
            $scope.identificationCustomer   = 'user';
            $scope.sregisterForm            = {};


            /*Regular*/
            $scope.firstLastNamePattern = /^[a-z]{2,65}$/i;
            $scope.zipcodePattern = /^[0-9]{0,5}$/;
            $scope.streetAddressPattern = /^[a-z 0-9 /\// , # _ . -]{0,165}$/i;
            // $scope.WebsiteUrlPattern =  /(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i;
            /*Regular*/

            $scope.submitChangedProfile = function () {
                $scope.aboutEdit = false;
                let data = {
                    firstName: $scope.sregisterForm.firstName,
                    lastName: $scope.sregisterForm.lastName,
                    phone: $('#phone').val(),
                    address: $scope.sregisterForm.streetAddress,
                    zipcode: $scope.sregisterForm.zipcode,
                    city: $scope.sregisterForm.city,
                    state: $scope.sregisterForm.state,
                    aboutMe: $scope.aboutUs,
                    lng: $scope.sregisterForm.lng,
                    lat: $scope.sregisterForm.lat
                };
                console.log(data);
                api.post('updateProfile', data, function(respons) {
                    if(respons){
                        toastr.info('Changes saved successfully', 'Processing');
                    }

                })
            };

            angular.element('#profilePhoto').change(function() {
                $scope.profilePhotoGif = true;
                toastr.info('Please wait while processing your request', 'Processing');
                if (document.getElementById('profilePhoto').files.length > 0) {
                    var url = config.local+'/upload/upload?token='+sessionStorage.getItem('token')+'&imageType=profilePhoto';
                    var fd = new FormData();
                    fd.append('profilePhoto', document.getElementById('profilePhoto').files[0]);
                    return $http.post(url, fd, {
                        transformRequest: angular.identity,
                        headers: {'Content-Type': undefined}
                    }).then(function onSuccess(response) {
                        $scope.profilePhotoGif = false;
                        $scope.profilePhoto =  response.data.url;
                    }).catch(function onError(sailsResponse) {
                        toastr.error(sailsResponse.data.msg, sailsResponse.data.toBigImageError);
                        return;
                    });
                }
            });

            angular.element('#userBanner').change(function() {
                $scope.coverBannerGif  = true;
                toastr.info('Please wait while processing your request', 'Processing');
                if (document.getElementById('userBanner').files.length > 0) {
                    var url = config.local+'/upload/upload?token='+sessionStorage.getItem('token')+'&imageType=coverBanner';
                    var fd = new FormData();
                    fd.append('coverBanner', document.getElementById('userBanner').files[0]);
                    return $http.post(url, fd, {
                        transformRequest: angular.identity,
                        headers: {'Content-Type': undefined}
                    }).then(function onSuccess(response) {
                        $scope.coverBannerGif = false;
                        $scope.bannerPhoto =  response.data.url;
                    }).catch(function onError(sailsResponse) {
                        toastr.error(sailsResponse.data.msg, sailsResponse.data.toBigImageError);
                        return;
                    });
                }
            });

            $scope.validateZipcode = function () {

                var zipcode = $scope.sregisterForm.zipcode;

                $http.post('/getzipcodedetails', {
                    zipcode: zipcode
                })
                    .then(function onSuccess(response) {
                        if (response.status == 200) {
                            $scope.sregisterForm.state = response.data.state;
                            $scope.sregisterForm.city = response.data.city;
                            $scope.sregisterForm.lng = response.data.lng;
                            $scope.sregisterForm.lat = response.data.lat;
                        } else {
                            toastr.error(sailsResponse.data.msg, 'Error');
                            return;
                        }
                    })
                    .catch(function onError(sailsResponse) {
                        toastr.error(sailsResponse.data.msg, 'Error');
                        return;
                    });
            }
        }
    ]
);