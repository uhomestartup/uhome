angular.module('App', ['ui.bootstrap','toastr', 'ui.mask', 'ngMap',]).controller('TodoController', ['$scope', '$rootScope', '$http', 'toastr', '$q', '$location',
    function($scope, $rootScope, $http, toastr, $q, $location) {
        $scope.contractorss = [];
        $scope.itemsPerPage = 5;
        $scope.currentPage = 1;

        $scope.$watch('contractors', function() {
            $scope.todos = $scope.contractors;
            $scope.figureOutTodosToDisplay();
        });
        $scope.figureOutTodosToDisplay = function() {
            var begin = (($scope.currentPage -1) * $scope.itemsPerPage);
            var end = begin + $scope.itemsPerPage;
            $scope.contractorss = $scope.todos.slice(begin, end);
        };
        $scope.pageChanged = function() {
            $scope.figureOutTodosToDisplay();
        };
    }
]);