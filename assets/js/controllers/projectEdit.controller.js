App.controller('projectEdit', ['$scope', '$http', '$q', 'toastr', 'businessHours',
    function($scope, $http, $q, toastr, businessHours) {
        $scope.identificationCustomer   = 'serviceSeeker';
        $scope.files                    = [];
        $scope.output                   = [];
        $scope.successCreate            = false;
        $scope.token                    = sessionStorage.getItem('token');
        $scope.postprojectForm          = {};
        $scope.postprojectFormParse     = '';
        $scope.budget = /^[0-9 $ -]{0,100}$/,

        $scope.$watch('postprojectFormParse', function () {
            console.log($scope.postprojectFormParse);
            // $scope.workLineProject = postprojectFormParse.service;
        });

        $http.post('/getSerSubSerStartTime', {projectId: sessionStorage.getItem('projectId')})
        .then(function(response) {
            $scope.workLineProject = response;
            console.log($scope.workLineProject);
        })
        .catch(function(sailsResponse) {
        });

        // angular.element('#attachment').change(function() {
        //     let files = document.getElementById('attachment').files;
        //     files  = Object.keys(files).map(function(index){return files[index]});
        //     if (files.length > 1) {
        //         files.forEach(function (item, i) {
        //             $scope.files[i] = item;
        //         });
        //         // $scope.registerForm(files);
        //     }
        //     // $scope.registerForm(files);
        // });

        $scope.registerForm = function (files) {
            $q.all(files)
                .then(function (response) {
                    console.log(response);
                    var i = 0;
                    angular.forEach($scope.outputSequense, function (index, value) {
                        var obj = {};
                        obj[index] = response[i].data.url;
                        $scope.output.push(obj);
                        i++;
                    });
                }).catch(function (err) {
            });
        };

        $scope.submitPostprojectForm = function (images) {
            $scope.postprojectForm.loading = true;
            let url = '/updateProject?imageType=attachment[]';
            // let url = '/createProject';
            let fd = new FormData();
            let files = document.getElementById('attachment').files;
            files  = Object.keys(files).map(function(index){return files[index]});

            fd.append('service', $scope.postprojectForm.category);
            fd.append('subService', $scope.postprojectForm.subcategory);
            fd.append('jobName', $scope.postprojectForm.jobTitle);
            fd.append('jobDescription', $scope.postprojectForm.jobDescription);
            fd.append('estimatedBudget', $('#estimatedBudget').val());
            fd.append('startTime', $scope.postprojectForm.startTime);
            fd.append('projectId', sessionStorage.getItem('projectId'));
            if(files.length>0){
                files.forEach(function (items) {
                    fd.append('attachment[]', items);
                });
            }

            // let data = {
            //     service: $scope.postprojectForm.category,
            //     subService: $scope.postprojectForm.subcategory,
            //     jobName: $scope.postprojectForm.jobTitle,
            //     jobDescription: $scope.postprojectForm.jobDescription,
            //     estimatedBudget: $('#estimatedBudget').val(),
            //     startTime: $scope.postprojectForm.startTime,
            //     projectId: sessionStorage.getItem('projectId')
            // };
            //
            // console.log(data);
            $http({
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined},
                url: url,
                method: 'POST',
                data:fd
            })
                .then(function onSuccess(sailsResponse) {
                    $scope.successCreate = true;
                })
                .catch(function onError(sailsResponse) {
                    toastr.error(sailsResponse.data.msg, sailsResponse.data.toBigImageError);
                    return;
                })
                .finally(function eitherWay() {
                    $scope.successCreate = true;
                });
        };

        $scope.updateSubServices = function() {
            $http.post('/getSubServicesByService', {

                serviceId: $scope.postprojectForm.category

            })
                .then(function onSuccess(sailsResponse) {
                    $scope.subservices = sailsResponse.data;
                })
                .catch(function onError(sailsResponse) {
                    var invalidCredentials = sailsResponse.status == 404;
                    if (invalidCredentials) {
                        toastr.error('Request failed.' , 'Error');
                        return;
                    }
                })
                .finally(function eitherWay() {
                    $scope.postprojectForm.loading = false;
                });
        }

    }
]);