App.controller('userDashboard', ['$scope', '$http', 'toastr',
    function($scope, $http, toastr) {
        $scope.identificationCustomer   = 'user';

        $scope.token = document.cookie.match( '(^|;) ?token=([^;]*)(;|$)' );
        if ($scope.token != null) {
            sessionStorage.setItem('token', unescape($scope.token[2]));
        }
    }
]);