App.controller('signUp', ['$scope', '$http', 'toastr', 'config', function($scope, $http, toastr, config) {
    $scope.userType = 'user';
    $scope.userType = sessionStorage.getItem('userType');
    //console.log(api.local);
    $scope.regexEmail = /^[a-z0-9-_.]+@[a-z0-9-]+\.[a-z]{0,65}$/i;

    $scope.submitSignupForm = function () {
        if($scope.signupForm.password != $scope.signupForm.confirmPassword){
            toastr.error('Password does not match the confirm password', 'Error');
            return false;
        }else {
            $scope.signupForm.loading = true;
            $scope.master = {userType: 'user'};

            $http.post('/signup', {
                userType: $scope.userType,
                email: $scope.signupForm.email,
                password: $scope.signupForm.password
            })
            .then(function onSuccess(sailsResponse) {
                if (sailsResponse.data.id) {
                    toastr.success('Confirmation email sent!', 'Account created successfully');
                    $scope.signupForm.userType = 'user';
                    $scope.signupForm.name = '';
                    $scope.signupForm.email = '';
                    $scope.signupForm.password = '';
                    $scope.signupForm.confirmPassword = '';
                    $scope.signup.$setPristine();
                    return;
                }
            })
            .catch(function onError(sailsResponse) {
                var emailAddressAlreadyInUse = sailsResponse.status == 409;
                if (emailAddressAlreadyInUse) {
                    toastr.error('That email address has already been taken, please try again.', 'Error');
                    return;
                }
            })
            .finally(function eitherWay() {
                $scope.signupForm.loading = false;
            });
        }
    };
    function get_cookie ()
    {
        $scope.authType = document.cookie.match( '(^|;) ?authType=([^;]*)(;|$)' );
        $scope.token = document.cookie.match( '(^|;) ?token=([^;]*)(;|$)' );
        $scope.email = document.cookie.match('(^|;) ?email=([^;]*)(;|$)');
        $scope.emailError = document.cookie.match( '(^|;) ?emailError=([^;]*)(;|$)' );
        if($scope.emailError != null){
            sessionStorage.setItem('emailError', unescape($scope.emailError[2]));
        }
        if($scope.token != null){
            sessionStorage.setItem('token', unescape($scope.token[2]));
        }
        sessionStorage.setItem('authType', $scope.authType);
        window.location = config.local+'/ssactivate?token='+unescape($scope.token[2])+'&email='+unescape($scope.email[2]);

        //console.log($scope.token, $scope.email);
    }


    $scope.loginBtn = function(name) {
        if('facebook'==name){
            var win = window.open('/auth/facebook', "_blank", "toolbar=no, scrollbars=no, resizable=no, top=100, left=100, width=600, height=600");
        }else{
            var win = window.open('/auth/google', "_blank", "toolbar=no, scrollbars=no, resizable=no, top=100, left=100, width=600, height=600");
        }

        var closeCheck = setInterval(function (type) {
            try {
                (win == null || win.closed) && (clearInterval(closeCheck), get_cookie ());
            } catch (ex) { }
        },500);
    };
}]);
