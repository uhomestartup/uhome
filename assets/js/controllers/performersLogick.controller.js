App.controller('performersLogick', ['$scope',
    function($scope) {
        $scope.hired        = 0;
        $scope.applicants   = 0;
        $scope.project      = '';

        $scope.performersLogick = function (item) {
            item.forEach(function (items) {
                if(items.status == 'Hired'){
                    $scope.hired++;
                }else{
                    $scope.applicants++;
                }
            });

        };
        $scope.$watch('project', function () {
            sessionStorage.setItem('projectId', $scope.project.id);
        });
    }
]);/**
 * Created by Admin on 28.02.2017.
 */
