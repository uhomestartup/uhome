App.controller('postProgectSS', ['$scope', '$http', '$q', 'toastr', 'businessHours',
    function($scope, $http, $q, toastr, businessHours) {
        $scope.identificationCustomer   = 'serviceSeeker';
        // $scope.states                  = businessHours.states;
        $scope.files                    = [];
        $scope.output                   = [];
        $scope.successCreate            = false;
        $scope.token                    = sessionStorage.getItem('token');
        $scope.budget                   = /^[0-9 $ -]{0,100}$/,
        $scope.postprojectForm = {
            loading: false,
            subcategory: [],

        };

        angular.element('#attachment').change(function() {
            /*let files = document.getElementById('attachment').files;
            files  = Object.keys(files).map(function(index){return files[index]});
            if (files.length > 1) {
                files.forEach(function (item, i) {
                    $scope.files[i] = item;
                });
                // $scope.registerForm(files);
            }*/
            // $scope.registerForm(files);
        });

        $scope.registerForm = function (files) {
            $q.all(files)
                .then(function (response) {
                    console.log(response);
                    var i = 0;
                    angular.forEach($scope.outputSequense, function (index, value) {
                        var obj = {};
                        obj[index] = response[i].data.url;
                        $scope.output.push(obj);
                        i++;
                    });
                }).catch(function (err) {
            });
        };

        $scope.submitPostprojectForm = function (images) {
            $scope.postprojectForm.loading = true;
            let url = '/createProject?imageType=attachment[]';
             let fd = new FormData();
             let files = document.getElementById('attachment').files;
             files  = Object.keys(files).map(function(index){return files[index]});

            fd.append('service', $scope.postprojectForm.category);
            fd.append('subService', $scope.postprojectForm.subcategory);
            fd.append('jobName', $scope.postprojectForm.jobTitle);
            fd.append('jobDescription', $scope.postprojectForm.jobDescription);
            fd.append('estimatedBudget', $('#estimatedBudget').val());
            fd.append('startTime', $scope.postprojectForm.startTime==''?$scope.postprojectForm.startTime:' ');
            files.forEach(function (items) {
                fd.append('attachment[]', items);
            });

            // fd.append('attachment[]', document.getElementById('attachment').files[1]);



            // };
            // //
            // fd.append('param', data);
            // console.log(data);
            $http({
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined},
                url: url,
                method: 'POST',
                data:fd
            })
                .then(function onSuccess(sailsResponse) {
                    $scope.successCreate = true;
                })
                .catch(function onError(sailsResponse) {
                    toastr.error(sailsResponse.data.msg, sailsResponse.data.toBigImageError);
                    return;
                })
                .finally(function eitherWay() {
                    $scope.successCreate = true;
                });
        };

        $scope.updateSubServices = function() {
            $http.post('/getSubServicesByService', {

                serviceId: $scope.postprojectForm.category

            })
                .then(function onSuccess(sailsResponse) {
                    $scope.subservices = sailsResponse.data;
                })
                .catch(function onError(sailsResponse) {
                    var invalidCredentials = sailsResponse.status == 404;
                    if (invalidCredentials) {
                        toastr.error('Request failed.' , 'Error');
                        return;
                    }
                })
                .finally(function eitherWay() {
                    $scope.postprojectForm.loading = false;
                });
        }

    }
]);