App.filter('replace', function () {
    return function (text) {
        let textChanged = text.replace(/\n/g, '<br />');
        return textChanged;
    }
});