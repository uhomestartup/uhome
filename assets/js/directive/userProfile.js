'use strict';

App.directive('userProfile', [ '$location', function ($location) {
    return function (scope, element) {

        $(".cover-banner-upload").click(function () {
            $("#userBanner").trigger('click');
        });

        $(".profile-upload").click(function () {
            $("#profilePhoto").trigger('click');
        });
    }
}]);

$('.right-panel').on('click', function () {
    $('#profile-dropdown-mobile').show();
});