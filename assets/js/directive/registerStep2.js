'use strict';

App.directive('stepTwo', function () {
    return function (scope, element) {

        var firstNamePattern = /^[a-z `]{2,65}$/i,
            lastNamePattern = /^[a-z `]{2,65}$/i,
            //phonePattern = /^[0-9-+()]{0,65}$/,
            zipcodePattern = /^[0-9]{0,5}$/,
            streetAddressPattern = /^[a-z 0-9 /\// , # _ . -]{0,165}$/i,
            firstName = $('#firstName'),
            lastName = $('#lastName'),
            phone = $('#phone'),
            zipcode = $('#zipcode'),
            streetAddress = $('#streetAddress');
            //phone.mask("(+999)999-999-9999");

        function searchError(inputElement, searchParent, type, msg){

            var errorStr = inputElement.parent('.input-group');
            function showError(){
                errorStr.addClass('has-error');
                //errorStr.removeClass('displayNone');
            }
            function hideError(){
                errorStr.removeClass('has-error');
                //errorStr.addClass('displayNone');
            }
            if(type == 1){
                if(inputElement.val().search(searchParent) == 0){
                    hideError();
                }else{
                    toastr.error(msg, 'Error');
                    showError();
                    //return false;
                }
            }
            else if(type == 2){
                if(inputElement.val() == ''){
                    toastr.error(msg, 'Error');
                    showError();
                    //return false;
                }else if(inputElement.val().search(searchParent) == 0){
                    hideError();
                }else{
                    toastr.error(msg, 'Error');
                    showError();
                    //return false;
                }
            }
            else if(type == 3){
                if(inputElement.val() == ''){
                    hideError();
                }else{
                    toastr.error(msg, 'Error');
                    showError();
                    //return false;
                }
            }
        }

        function clickSaveBtn(){
            angular.element('#step2-submit').unbind('click');
            angular.element('#step2-submit').bind('click', function(){
                searchError(firstName, firstNamePattern, 1, "First name should contain 2+ only characters");
                searchError(lastName, lastNamePattern, 1, "Last name should contain 2+ only characters");
                //searchError(phone, phonePattern, 2, "Phone should contain only numbers");
                searchError(zipcode, zipcodePattern, 2, "Zipcode should contain only numbers");
                searchError(streetAddress, streetAddressPattern, 2, 'Missing street address!');


                angular.element('.input-group').each( function(){
                    var self = angular.element(this);
                    if(self.hasClass('has-error')){
                        scope.flageRegNext = false;
                        return false;
                    }
                    else{
                        scope.flageRegNext = true;
                    }
                });
                if(scope.flageRegNext){
                    $(".signup-body-step2").addClass('hidden');
                    $(".signup-body-step3").removeClass('hidden');
                }
            });
        }

        clickSaveBtn();
        angular.element('.basicInfInputs input').each(function() {
            var self = angular.element(this);
            //inputEmpty(self);
        });




        $("#step3-submit").click(function () {
            $(".signup-body-step3").addClass('hidden');
            $(".signup-body-step4").removeClass('hidden');
            return false;
        });

        $(".cover-banner-upload").click(function () {
            $("#coverBanner").trigger('click');
        });

        $(".profile-upload").click(function () {
            $("#profilePhoto").trigger('click');
        });

        $("input[name=zipcode]").blur(function(e) {
            $.post('./getzipcodedetails', {
                zipcode: $("input[name=zipcode]").val()
            }).then(function(data) {
                $("input[name=state]").val(data.state);
                $("input[name=city]").val(data.city);
                $("input[name=lng]").val(data.lng);
                $("input[name=lat]").val(data.lat);
                $("input[name=zipcodeStatus]").val('true');
            }).fail(function(err) {
                $("input[name=state]").val('');
                $("input[name=city]").val('');
                $("input[name=lng]").val('');
                $("input[name=lat]").val('');
                $("input[name=zipcodeStatus]").val('false');
                toastr.error(err.responseJSON.msg, 'Error');
                return false;
            });
        });
        function getURLParameter(name) {
            return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [null, ''])[1].replace(/\+/g, '%20')) || null;
        }
        $("#email").val(getURLParameter('email'));
        $("#token").val(getURLParameter('token'));



    }
});