'use strict';

App.directive('bStepTwo', [ '$location', function ($location) {
    return function (scope, element) {

        var companyFirstNamePattern = /^[a-z`]{2,65}$/i,
            companyLastNamePattern = /^[a-z`]{2,65}$/i,
            //mailPattern = /^[a-z0-9-_.]+@[a-z0-9-]+\.[a-z]{0,65}$/i,
            userFirstNamePattern = /^[a-z`]{2,65}$/i,
            userLastNamePattern = /^[a-z`]{2,65}$/i,
            zipcodePattern = /^[0-9]{0,5}$/,
            companyUnitPattern = /^[0-9]{0,5}$/,
            companyBusinessNamePattern = /^[a-z 0-9 ]{2,65}$/i,
            companyStreetAddressPattern = /^[a-z 0-9 /\// , # _ . -]{0,165}$/i,
            userStreetAddressPattern = /^[a-z 0-9 /\// , # _ . -]{0,165}$/i,
            companyBusinessPhonePattern = /^[0-9-+()]{0,14}$/,
            companyBusinessEINPattern = /^[0-9-]{0,10}$/,
            companyWebsiteUrl = $('#companyWebsiteURL'),
            companyWebsiteUrlPattern =  /(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i,
            userWebsiteUrl = $('#userWebsiteURL'),
            userWebsiteUrlPattern =  /(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i,
            companyFirstName = $('#companyFirstName'),
            companyLastName = $('#companyLastName'),
            userFirstName = $('#userFirstName'),
            userLastName = $('#userLastName'),
            userZipcode = $('#userZipcode'),
            companyZipcode = $('#companyZipcode'),
            companyUnit = $('#companyUnit'),
            companyBusinessName = $('#companyBusinessName'),
            companyStreetAddress = $('#companyStreetAddress'),
            userStreetAddress = $('#userStreetAddress'),
            companyBusinessPhone = $('#companyBusinessPhone'),
            userWorkPhone = $('#userWorkPhone'),
            companyBusinessEIN = $('#companyBusinessEIN'),
            mail = $('#email'),
            password = element.find('#password'),
            confirmPassword = element.find('#confirmPassword');


        function searchError(inputElement, searchParent, type, msg){
            var errorStr = inputElement.parent('.input-group');
            function showError(){
                errorStr.addClass('has-error');
                //errorStr.removeClass('displayNone');
            }
            function hideError(){
                errorStr.removeClass('has-error');
                //errorStr.addClass('displayNone');
            }
            if(type == 1){
                if(inputElement.val().search(searchParent) == 0){
                    hideError();
                }else{
                    toastr.error(msg, 'Error');
                    showError();
                    return false;
                }
            }
            else if(type == 2){
                if(inputElement.val() == ''){
                    toastr.error(msg, 'Error');
                    showError();
                    return false;
                }else if(inputElement.val().search(searchParent) == 0){
                    hideError();
                }else{
                    toastr.error(msg, 'Error');
                    showError();
                    return false;
                }
            }
            else if(type == 3){
                if(inputElement.val() != ''){
                    hideError();
                }else{
                    toastr.error(msg, 'Error');
                    showError();
                    return false;
                }
            }
            else if (type == 4) {
                if (inputElement.val() === searchParent.val() && inputElement.val() !== '') {
                    hideError();
                } else {
                    //toastr.error(msg, 'Error');
                    showError();
                    return false;
                }
            }
            else if(type == 5){
                if(inputElement.val() == ''){
                    hideError();
                }else if(inputElement.val().search(searchParent) == 0){
                    hideError();
                } else {
                    toastr.error(msg, 'Error');
                    showError();
                    return false;
                }
            }
        }

        function clickSaveBtnCompany(){
            angular.element('#step2-company-submit').unbind('click');
            angular.element('#step2-company-submit').bind('click', function(){
                searchError(companyFirstName, companyFirstNamePattern, 2, "First name should contain 2+ only characters");
                searchError(companyLastName, companyLastNamePattern, 2, "Last name should contain 2+ only characters");
                searchError(companyBusinessName, companyBusinessNamePattern, 2, "Company Business Name should contain 2+ only characters");
                searchError(companyUnit, companyUnitPattern, 1, "Company Unit should contain only numbers");
                searchError(companyZipcode, zipcodePattern, 2, "Zipcode should contain only numbers");
                searchError(companyStreetAddress, companyStreetAddressPattern, 2, 'Missing Company street address!');
                searchError(companyBusinessPhone, companyBusinessPhonePattern, 5, 'Missing Company Business Phone!');
                searchError(companyBusinessEIN, companyBusinessEINPattern, 5, 'Missing Company Business EIN!');
                searchError(companyWebsiteUrl, companyWebsiteUrlPattern, 5, 'Missing Website Url address!');
                angular.element('.input-group').each( function(){
                    var self = angular.element(this);
                    if(self.hasClass('has-error')){
                        scope.flageRegNext = false;
                        return false;
                    }
                    else{
                        scope.flageRegNext = true;
                    }
                });
                if(scope.flageRegNext == true){
                    $(".signup-body-step2-company").addClass('hidden');
                    $(".signup-body-step3").removeClass('hidden');
                }
            });
        }
        function clickSaveBtnUser(){
            angular.element('#step2-user-submit').unbind('click');
            angular.element('#step2-user-submit').bind('click', function(){
                searchError(userFirstName, userFirstNamePattern, 1, "First name should contain 2+ only characters");
                searchError(userLastName, userLastNamePattern, 1, "Last name should contain 2+ only characters");
                searchError(userZipcode, zipcodePattern, 2, "Zipcode should contain only numbers");
                searchError(userStreetAddress, userStreetAddressPattern, 2, 'Missing street address!');
                searchError(userWebsiteUrl, userWebsiteUrlPattern, 5, 'Missing Website Url!');
                //searchError(userWorkPhone, companyBusinessPhonePattern, 3, 'Missing User Work Phone street address!');

                angular.element('.input-group').each( function(){
                    var self = angular.element(this);
                    if(self.hasClass('has-error')){
                        scope.flageRegNext = false;
                        return false;
                    }
                    else{
                        scope.flageRegNext = true;
                    }
                });
                if(scope.flageRegNext == true){
                    $(".signup-body-step2-user").addClass('hidden');
                    $(".signup-body-step3").removeClass('hidden');
                }
            });
        }

        function clickBtnConfirm(){
            angular.element('#signup-button').unbind('click');
            angular.element('#signup-button').bind('click', function(){
                searchError(confirmPassword, password, 4, "Password does not match the confirm password");
                //searchError(mail, mailPattern, 1, "Password does not match the confirm password");

                angular.element('.input-group').each( function(){
                    var self = angular.element(this);
                    if(self.hasClass('has-error')){
                        scope.flageRegPass = false;
                        return false;
                    }
                    else{
                        scope.flageRegPass = true;
                    }
                });
            });
        }

        clickSaveBtnCompany();
        clickSaveBtnUser();
        clickBtnConfirm();
        angular.element('.basicInfInputs input').each(function() {
            var self = angular.element(this);
            //inputEmpty(self);
        });


        $(".cover-banner-upload").click(function () {
            $("#coverBanner").trigger('click');
        });

        $(".profile-upload").click(function () {
            $("#profilePhoto").trigger('click');
        });

        $(".logo-upload").click(function () {
            $("#logoPhoto").trigger('click');
        });

        function getURLParameter(name) {
            return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [null, ''])[1].replace(/\+/g, '%20')) || null;
        }
        $("#emailHidden").val(getURLParameter('email'));
        $("#tokenHidden").val(getURLParameter('token'));

        $('.date-own').datepicker({
            minViewMode: 2,
            // format: 'yyyy',
            autoclose: true
        });

        //$('#calendar').fullCalendar({
        //    defaultDate: '2016-06-12',
        //    editable: true,
        //    header: {
        //        right: 'next',
        //        center: 'title',
        //        left: 'prev'
        //    }
        //});
        //if($location.path()=='bregister'){
        // scope.hoursStyle = function(){
        //     scope.$watch(function () {
        //         return scope.bregisterForm.companySunStart;
        //     }, function () {
        //         $.each($(".business-days"), function (index, value) {
        //             if ($(this).children(".weekday").val() != 'Close') {
        //                 $(this).children(".day").css('background-color', '#f07246');
        //             }
        //         });
        //     });
        // }
        //
        // $(".weekday").change(function () {
        //     var day = $(this).data('id');
        //     if ($(this).val() == "Close") {
        //         $("." + day).css('background-color', '#bdbdbd');
        //         $("." + day + "-end").addClass('hidden');
        //     } else {
        //         $("." + day).css('background-color', '#f07246');
        //         $("." + day + "-end").removeClass('hidden');
        //     }
        // });

        $(".btn-company").click(function () {
            $(".btn-company").addClass('active');
            $(".btn-user").removeClass('active');
            $(".btn-company img").attr('src', '/images/signup/company-selected.png');
            $(".btn-user img").attr('src', '/images/signup/user.png');
            $(".btn-companyRadio").prop('checked', 'checked');
            $(".btn-userRadio").prop('checked', false);
            $(".profileImageContiner").removeClass('col-md-6').addClass('col-md-12');
            $(".logoImageContainer").removeClass('col-md-6').addClass('col-md-12');
            return false;
        });

        $(".btn-user").click(function () {
            $(".btn-user").addClass('active');
            $(".btn-company").removeClass('active');
            $(".btn-user img").attr('src', '/images/signup/user-selected.png');
            $(".btn-company img").attr('src', '/images/signup/company.png');
            $(".btn-userRadio").prop('checked', 'checked');
            $(".btn-companyRadio").prop('checked', false);
            $(".profileImageContiner").removeClass('col-md-3').addClass('col-md-6');
            $(".logoImageContainer").removeClass('col-md-6').addClass('col-md-3 hidden');
            return false;
        });

        $("#start-registration").click(function () {
            if (
                !(
                    $(".btn-user").hasClass('active') ||
                    $(".btn-company").hasClass('active')
                )
            ) {
                toastr.error('You need to select an option!');
                return false;
            } else {
                if ($(".btn-user").hasClass('active')) {
                    $(".signup-bodys").addClass('hidden');
                    $(".signup-body-step2-user").removeClass('hidden');
                    return false;
                } else {
                    $(".signup-bodys").addClass('hidden');
                    $(".signup-body-step2-company").removeClass('hidden');
                    return false;
                }
            }
        });

        $("#step3-submit").click(function () {
            // var error = false;
            // if ($("textarea[name=aboutUs]").val() == "") {
            //     error = true;
            //     var msg = 'Missing about us description!';
            // }
            // if (error) {
            //     toastr.error(msg, 'Error');
            //     return false;
            // }
            $(".signup-body-step3").addClass('hidden');
            $(".signup-body-step4").removeClass('hidden');
            return false;
        });

        // $(".choose-line").click( function () {
        //     $(this).find('#category').hide();
        //     $("#inputCategory").show();
        // });

        $(window).on("click.Bst", function(event){
            var obj = $(".choose-line");
            var select =  $(this).find('#categoryLine');
            var input = $("#inputCategory");
            var dropdown = $(".select-dropdown-list");
            if ( obj.has(event.target).length == 0 && !obj.is(event.target) ){
                select.show();
                input.hide();
                dropdown.hide();

            } else {
                // select.hide();
                simulateClick();
                input.show().focus();
                select.click();
                dropdown.show();
            }
        });


        function simulateClick() {
            var evt = document.createEvent("MouseEvents");
            evt.initMouseEvent("click", true, true, window,
            1, 0, 0, 0, 0, false, false, false, false, 0, null);
            var cb = document.getElementById("categoryLine");
            var canceled = !cb.dispatchEvent(evt);
        }

        // element.find("#serviceAdd").focus(function() {
        //     var self = angular.element(this);
        //     self.keypress(function( event ) {
        //         if (event.which == 13) {
        //             scope.serviceArray.push(scope.newService);
        //             self.attr('value', '');
        //         }
        //     });
        // });


        scope.addService = function () {
            scope.serviceArray.push({name: scope.newService});
            scope.bregisterForm.serviceArray = scope.serviceArray;
            element.find("#serviceAdd").attr('value', '');
            scope.newService='';
        };
        scope.deleteService = function (key) {
            scope.serviceArray.splice(key, 1);
        };


        function isUrlValid(url) {
            return /(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(url);
        }
    }
}]);