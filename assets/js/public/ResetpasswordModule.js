App.controller('ResetpasswordController', ['$scope', '$http', 'toastr', function($scope, $http, toastr) {
  $scope.resetpasswordForm = {
    loading: false
  };

  $scope.submitResetpasswordForm = function () {
    $scope.resetpasswordForm.loading = true;
    $scope.master = {};
    console.log($scope.resetpasswordForm.userId);
    $http.post('/resetpassword', {

      userId: $scope.resetpasswordForm.userId,
      password: $scope.resetpasswordForm.password,
      confirmPassword: $scope.resetpasswordForm.confirmPassword

    })
      .then(function onSuccess(sailsResponse) {
        toastr.success(sailsResponse.data.msg , 'Success');
        return;
      })
      .catch(function onError(sailsResponse) {
        var invalidCredentials = sailsResponse.status == 404;
        var badRequest = sailsResponse.status == 400;
        if (invalidCredentials) {
          toastr.error('Invalid email, please try again.' , 'Error');
          return;
        } else if (badRequest) {
          toastr.error('Missing required field, please try again.' , 'Error');
          return;
        }
      })
      .finally(function eitherWay() {
        $scope.resetpasswordForm.loading = false;
      });
  };
}]);

