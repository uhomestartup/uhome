angular.module('PostprojectModule', ['toastr']).controller('PostprojectController', ['$scope', '$http', 'toastr', function($scope, $http, toastr) {
  $scope.postprojectForm = {
    loading: false
  };

  $scope.submitPostprojectForm = function () {
    $scope.postprojectForm.loading = true;
    $scope.master = {};

    $http.post('/submitProject', {

      serviceId: $scope.postprojectForm.category,
      subServiceId: $scope.postprojectForm.subcategory,
      jobTitle: $scope.postprojectForm.jobTitle,
      jobDescription: $scope.postprojectForm.jobDescription,
      attachment: $scope.postprojectForm.attachment,
      estimatedBuget: $scope.postprojectForm.budget,
      startTime: $scope.postprojectForm.startTime

    })
      .then(function onSuccess(sailsResponse) {
        if (sailsResponse.data == true) {
          window.location = '/postSubmitProject';
        }
      })
      .catch(function onError(sailsResponse) {
        var invalidCredentials = sailsResponse.status == 404;
        if (invalidCredentials) {
          toastr.error('Request failed.' , 'Error');
          return;
        }
      })
      .finally(function eitherWay() {
        $scope.postprojectForm.loading = false;
      });
  };

  $scope.updateSubServices = function() {
    $http.post('/getSubServicesByService', {

      serviceId: $scope.postprojectForm.category

    })
      .then(function onSuccess(sailsResponse) {
        $scope.subservices = sailsResponse.data;
      })
      .catch(function onError(sailsResponse) {
        var invalidCredentials = sailsResponse.status == 404;
        if (invalidCredentials) {
          toastr.error('Request failed.' , 'Error');
          return;
        }
      })
      .finally(function eitherWay() {
        $scope.postprojectForm.loading = false;
      });
  }
}]);

