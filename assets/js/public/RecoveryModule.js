angular.module('RecoveryModule', ['toastr']).controller('RecoveryController', ['$scope', '$http', 'toastr', function($scope, $http, toastr) {
  $scope.recoveryForm = {
    loading: false
  };

  $scope.submitRecoveryForm = function () {
    $scope.recoveryForm.loading = true;
    $scope.master = {};

    console.log($scope.recoveryForm.userType);

    $http.post('/recovery', {

      email: $scope.recoveryForm.email,
      userType: $scope.recoveryForm.userType

    })
      .then(function onSuccess(sailsResponse) {
        if (sailsResponse.data.msg == 'Please check you email') {
          toastr.success(sailsResponse.data.msg , 'Success');
          return;
        } else if (sailsResponse.data.msg == 'Email addresss not found!') {
          toastr.error(sailsResponse.data.msg , 'Failed');
          return;
        } else {
          toastr.success(sailsResponse.data.msg , 'Success');
          return;
        }
      })
      .catch(function onError(sailsResponse) {
        var invalidCredentials = sailsResponse.status == 404;
        if (invalidCredentials) {
          toastr.error('Invalid email, please try again.' , 'Error');
          return;
        }
      })
      .finally(function eitherWay() {
        $scope.recoveryForm.loading = false;
      });
  };
}]);
