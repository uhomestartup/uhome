angular.module('ServiceModule', ['toastr']).controller('ServiceController', ['$scope', '$http', 'toastr', function($scope, $http, toastr) {
  $scope.recoveryForm = {
    loading: false
  };

  $scope.get = function () {

    $http.post('/service')
      .then(function onSuccess(sailsResponse) {
        console.log(sailsResponse.data);
        //toastr.success(sailsResponse.data.msg , 'Success');
        return;
      })
      .catch(function onError(sailsResponse) {
        var invalidCredentials = sailsResponse.status == 404;
        if (invalidCredentials) {
          toastr.error('Invalid email, please try again.' , 'Error');
          return;
        }
      })
      .finally(function eitherWay() {
        //$scope.recoveryForm.loading = false;
      });
  };
}]);

