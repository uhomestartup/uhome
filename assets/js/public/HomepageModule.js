App.controller('HomepageController', ['$scope', '$http', 'toastr', function($scope, $http, toastr) {
  $scope.userType = 'user';
  $scope.signupForm = {
    loading: false
  };

  $scope.account = function(userType){
    $scope.userType = userType;
    sessionStorage.setItem('userType', $scope.userType);
    console.log($scope.userType);
  };
}]);
