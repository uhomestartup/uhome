App.controller('BregisterController', ['$scope', '$http', 'toastr', '$q', '$location', 'config', 'businessHours', function ($scope, $http, toastr, $q, $location, config, businessHours) {

    $scope.companyWebsiteUrlPattern =  /(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i;
    $scope.statusUploadedTrue       = false;
    $scope.teamForm = {
        loading: false
    };
    $scope.close                    = false;
    $scope.allTime                  = false;
    $scope.bregisterForm            = [];
    $scope.isDisabled               = false;
    $scope.teamData                 = window.teamData;
    $scope.token                    = document.cookie.match( '(^|;) ?token=([^;]*)(;|$)' );
    $scope.hourlyRate               = businessHours.hourlyRate;
    $scope.states                   = businessHours.states;
    $scope.businessHours            = businessHours.arrayHours;
    $scope.userAlwaysOpen           = false;

    $scope.hoursPushInArray = function (key) {
        if (key == false) {
            $scope.businessHours = businessHours.arrayHours;
        } else {
            $scope.businessHours = businessHours.arrayIs24Hours;
        }
        $scope.teamForm = {
            loading: false
        };
    };

    $scope.parseURLParams = function() {
        $scope.bregisterForm.email = getParameterByName('email');
        $scope.bregisterForm.token = getParameterByName('token');

    };

    $scope.parseURLParams = function() {
        $scope.bregisterForm.email = getParameterByName('email');
        $scope.bregisterForm.token = getParameterByName('token');

    };

    // $scope.clickSelectClose = function () {
    //     $scope.businessHoursSunEnd = $scope.businessHoursSunEnd!='Close'?'05:00:00 PM':'Close';
    //     $scope.businessHoursMonEnd = $scope.businessHoursMonEnd!='Close'?'05:00:00 PM':'Close';
    //     $scope.businessHoursTueEnd = $scope.businessHoursTueEnd!='Close'?'05:00:00 PM':'Close';
    //     $scope.businessHoursWedEnd = $scope.businessHoursWedEnd!='Close'?'05:00:00 PM':'Close';
    //     $scope.businessHoursThuEnd = $scope.businessHoursThuEnd!='Close'?'05:00:00 PM':'Close';
    //     $scope.businessHoursFriEnd = $scope.businessHoursFriEnd!='Close'?'05:00:00 PM':'Close';
    //     $scope.businessHoursSatEnd = $scope.businessHoursSatEnd!='Close'?'05:00:00 PM':'Close';
    // };

    $scope.allTime = function (event) {
        if(event){
            $scope.businessHoursSunStart = $scope.businessHoursSunStart!='Close'?'24hr':'Close';
            $scope.businessHoursMonStart = $scope.businessHoursMonStart!='Close'?'24hr':'Close';
            $scope.businessHoursTueStart = $scope.businessHoursTueStart!='Close'?'24hr':'Close';
            $scope.businessHoursWedStart = $scope.businessHoursWedStart!='Close'?'24hr':'Close';
            $scope.businessHoursThuStart = $scope.businessHoursThuStart!='Close'?'24hr':'Close';
            $scope.businessHoursFriStart = $scope.businessHoursFriStart!='Close'?'24hr':'Close';
            $scope.businessHoursSatStart = $scope.businessHoursSatStart!='Close'?'24hr':'Close';

            $scope.businessHoursSunEnd = $scope.businessHoursSunEnd!='Close'?'24hr':'Close';
            $scope.businessHoursMonEnd = $scope.businessHoursMonEnd!='Close'?'24hr':'Close';
            $scope.businessHoursTueEnd = $scope.businessHoursTueEnd!='Close'?'24hr':'Close';
            $scope.businessHoursWedEnd = $scope.businessHoursWedEnd!='Close'?'24hr':'Close';
            $scope.businessHoursThuEnd = $scope.businessHoursThuEnd!='Close'?'24hr':'Close';
            $scope.businessHoursFriEnd = $scope.businessHoursFriEnd!='Close'?'24hr':'Close';
            $scope.businessHoursSatEnd = $scope.businessHoursSatEnd!='Close'?'24hr':'Close';
        }else{
            $scope.businessHoursSunStart = $scope.businessHoursSunStart!='Close'?'09:00 AM':'Close';
            $scope.businessHoursMonStart = $scope.businessHoursMonStart!='Close'?'09:00 AM':'Close';
            $scope.businessHoursTueStart = $scope.businessHoursTueStart!='Close'?'09:00 AM':'Close';
            $scope.businessHoursWedStart = $scope.businessHoursWedStart!='Close'?'09:00 AM':'Close';
            $scope.businessHoursThuStart = $scope.businessHoursThuStart!='Close'?'09:00 AM':'Close';
            $scope.businessHoursFriStart = $scope.businessHoursFriStart!='Close'?'09:00 AM':'Close';
            $scope.businessHoursSatStart = $scope.businessHoursSatStart!='Close'?'09:00 AM':'Close';

            $scope.businessHoursSunEnd = $scope.businessHoursSunEnd!='Close'?'05:00 PM':'Close';
            $scope.businessHoursMonEnd = $scope.businessHoursMonEnd!='Close'?'05:00 PM':'Close';
            $scope.businessHoursTueEnd = $scope.businessHoursTueEnd!='Close'?'05:00 PM':'Close';
            $scope.businessHoursWedEnd = $scope.businessHoursWedEnd!='Close'?'05:00 PM':'Close';
            $scope.businessHoursThuEnd = $scope.businessHoursThuEnd!='Close'?'05:00 PM':'Close';
            $scope.businessHoursFriEnd = $scope.businessHoursFriEnd!='Close'?'05:00 PM':'Close';
            $scope.businessHoursSatEnd = $scope.businessHoursSatEnd!='Close'?'05:00 PM':'Close';
        }
        if (event) {
            $scope.businessHours = businessHours.arrayIs24Hours;
        } else {
            $scope.businessHours = businessHours.arrayHours;
        }

        $scope.teamForm = {
            loading: false
        };
    };


    $scope.businessHoursSunStart = 'Close';
    $scope.businessHoursSunEnd = '05:00 PM';
    $scope.businessHoursMonStart = '09:00 AM';
    $scope.businessHoursMonEnd = '05:00 PM';
    $scope.businessHoursTueStart = '09:00 AM';
    $scope.businessHoursTueEnd = '05:00 PM';
    $scope.businessHoursWedStart = '09:00 AM';
    $scope.businessHoursWedEnd = '05:00 PM';
    $scope.businessHoursThuStart = '09:00 AM';
    $scope.businessHoursThuEnd = '05:00 PM';
    $scope.businessHoursFriStart = '09:00 AM';
    $scope.businessHoursFriEnd = '05:00 PM';
    $scope.businessHoursSatStart = 'Close';
    $scope.businessHoursSatEnd = '05:00 PM';
    $scope.bregisterForm = {
        loading: false,
        userTypeImgSelect: true,
        companyHourlyRate: '$$',
        userHourlyRate: '$$'
    };


    $scope.submitTeamMemberForm = function () {
        if (
            $scope.teamForm.firstName == undefined ||
            $scope.teamForm.lastName == undefined ||
            $scope.teamForm.designation == undefined
        ) {
            toastr.error('Please fill in the required fields', 'Error');
            return false;
        }

        var profilePhoto = document.getElementById('upload-team-member').files[0];
        var files = [];

        if (document.getElementById('upload-team-member').files.length > 0) {
            files.push(uploadImage(document.getElementById('upload-team-member').files[0]));
        } else {
            toastr.error('Please select profile picture', 'Error');
            return false;
        }
        toastr.info('Please wait while processing your request', 'Processing');
        $scope.teamForm.loading = true;

        $q.all(files)
            .then(function (response) {
                var data = {
                    firstName: $scope.teamForm.firstName,
                    lastName: $scope.teamForm.lastName,
                    designation: $scope.teamForm.designation,
                    userPhoto: response[0].data.url
                };
                $http.post('/addTeamMember', data)
                    .then(function onSuccess(sailsResponse) {
                        window.location = '/editTeam';
                        return;
                    })
                    .catch(function onError(sailsResponse) {
                        var invalidCredentials = sailsResponse.status == 404;
                        if (invalidCredentials) {
                            toastr.error('Invalid username/password, please try again.', 'Error');
                            return;
                        }
                    })
                    .finally(function eitherWay() {
                        $scope.teamForm.loading = false;
                    });
            }).catch(function (err) {
                console.log(err);
            });
    };

    $scope.files = [];
    $scope.outputSequense = [];
    $scope.output = [];

    angular.element('#profilePhoto').change(function() {
        toastr.info('Please wait while processing your request', 'Processing');
        if (document.getElementById('profilePhoto').files.length > 0) {
            var closeProfilePhoto = setInterval(function(){
                if(!$scope.statusUploadedTrue){
                    $scope.files.push(uploadImage(document.getElementById('profilePhoto').files[0], 'profilePhoto'));
                    $scope.statusUploadedTrue = true;
                    $q.all($scope.files)
                        .then(function (response) {
                            console.log(response);
                            $scope.respInfo = response;
                            $scope.respInfo.forEach(function(item){
                                if(item.data.imageType == 'profilePhoto'){
                                    $scope.profilePhoto = item.data.url;
                                    $scope.statusUploadedTrue = false;
                                    clearInterval(closeProfilePhoto)
                                }
                            });
                            $scope.profilePhotoGif = false;
                        }).catch(function onError(sailsResponse) {
                            toastr.error(sailsResponse.data.msg, sailsResponse.data.toBigImageError);
                            return;
                        });
                    $scope.outputSequense.push('profilePhoto');
                }
            },1000);

        }
    });

    angular.element('#logoPhoto').change(function() {
        toastr.info('Please wait while processing your request', 'Processing');
        if (document.getElementById('logoPhoto').files.length > 0) {
            var closeImageLogo = setInterval(function(){
                if(!$scope.statusUploadedTrue){
                    $scope.files.push(uploadImage(document.getElementById('logoPhoto').files[0], 'imageLogo'));
                    $scope.statusUploadedTrue = true;
                    $q.all($scope.files)
                        .then(function (response) {
                            console.log(response);
                            $scope.respInfo = response;
                            $scope.respInfo.forEach(function(item){
                                if(item.data.imageType == 'imageLogo'){
                                    $scope.imageLogo = item.data.url;
                                    $scope.statusUploadedTrue = false;
                                    clearInterval(closeImageLogo)
                                }
                            });
                            $scope.imageLogoGif = false;
                        }).catch(function onError(sailsResponse) {
                            toastr.error(sailsResponse.data.msg, sailsResponse.data.toBigImageError);
                            return;
                        });
                    $scope.outputSequense.push('logoPhoto');
                }
            },1000);
        }
    });

    angular.element('#coverBanner').change(function() {
        toastr.info('Please wait while processing your request', 'Processing');
        if (document.getElementById('coverBanner').files.length > 0) {
            var closeCoverBanner = setInterval(function(){
                if(!$scope.statusUploadedTrue){
                    $scope.files.push(uploadImage(document.getElementById('coverBanner').files[0], 'coverBanner'));
                    $scope.statusUploadedTrue = true;
                    $q.all($scope.files)
                        .then(function (response) {
                            $scope.respInfo = response;
                            $scope.respInfo.forEach(function(item){
                                if(item.data.imageType == 'coverBanner'){
                                    $scope.coverBanner = item.data.url;
                                    $scope.statusUploadedTrue = false;
                                    clearInterval(closeCoverBanner)
                                }
                            });
                            $scope.coverBannerGif = false;
                        }).catch(function onError(sailsResponse) {
                            toastr.error(sailsResponse.data.msg, sailsResponse.data.toBigImageError);
                            return;
                        });
                    $scope.outputSequense.push('coverBanner');
                }
            },1000);

        }
    });

    $scope.registerForm = function () {
        toastr.info('Please wait while processing your request', 'Processing');
    };

    var uploadImage = function (file, name) {
        if(name == 'profilePhoto'){
            $scope.profilePhotoGif = true;
        }else if(name == 'coverBanner'){
            $scope.coverBannerGif  = true;
        }else{
            $scope.imageLogoGif  = true;
        }
        var url = config.local+'/upload/upload?token='+$scope.bregisterForm.token+'&imageType='+name;
        var fd = new FormData();
        fd.append(name, file);
        return $http.post(url, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        });
    };

    $scope.setUserType = function (val) {
        $scope.parseURLParams();
        $scope.bregisterForm.userType = val;
        $scope.userTypeSelected = true;
        return false;
    };

    $scope.validateZipcode = function () {
        if ($scope.bregisterForm.userType == 'Business') {
            var zipcode = $scope.bregisterForm.companyZipcode;
        } else {
            var zipcode = $scope.bregisterForm.userZipcode;
        }
        $http.post('/getzipcodedetails', {
            zipcode: zipcode
        })
        .then(function onSuccess(response) {
            if ($scope.bregisterForm.userType == 'Business') {
                if (response.status == 200) {
                    $scope.bregisterForm.companyState = response.data.state;
                    $scope.bregisterForm.companyCity = response.data.city;
                    $scope.bregisterForm.companyLng = response.data.lng;
                    $scope.bregisterForm.companyLat = response.data.lat;
                } else {
                    $scope.bregisterForm.companyState = '';
                    $scope.bregisterForm.companyCity = '';
                    $scope.bregisterForm.companyLng = '';
                    $scope.bregisterForm.companyLat = '';
                }
            } else {
                if (response.status == 200) {
                    $scope.bregisterForm.userState = response.data.state;
                    $scope.bregisterForm.userCity = response.data.city;
                    $scope.bregisterForm.userLng = response.data.lng;
                    $scope.bregisterForm.userLat = response.data.lat;
                } else {
                    $scope.bregisterForm.userState = '';
                    $scope.bregisterForm.userCity = '';
                    $scope.bregisterForm.userLng = '';
                    $scope.bregisterForm.userLat = '';
                }
            }
        })
        .catch(function onError(sailsResponse) {
            toastr.error(sailsResponse.data.msg, 'Error');
            return;
        });
    }

    $scope.updateUserType = function () {
        if ($scope.bregisterForm.userTypeImgSelect) {
            $scope.bregisterForm.userTypeImgSelect = false;
        } else {
            $scope.bregisterForm.userTypeImgSelect = true;
        }
    };

    $scope.submitRegistrationForm = function () {
        toastr.info('Please wait while processing your request', 'Processing');

                $scope.bregisterForm.loading = true;
                $scope.isDisabled = true;
                if ($scope.bregisterForm.userType == 'Business') {
                    var data = {
                        userType: 'Business',
                        companyFirstName: $scope.bregisterForm.companyFirstName,
                        companyLastName: $scope.bregisterForm.companyLastName,
                        companyStreetAddress: $scope.bregisterForm.companyStreetAddress,
                        companyUnit: $scope.bregisterForm.companyUnit,
                        companyBusinessName: $scope.bregisterForm.companyBusinessName,
                        companyZipcode: $scope.bregisterForm.companyZipcode,
                        companyCity: $scope.bregisterForm.companyCity,
                        companyLat: $scope.bregisterForm.companyLat,
                        companyLng: $scope.bregisterForm.companyLng,
                        companyState: $scope.bregisterForm.companyState,
                        companyBusinessPhone: $('#companyBusinessPhone').val(),
                        companyBusinessEIN: $('#companyBusinessEIN').val(),
                        companyHourlyRate: $scope.bregisterForm.companyHourlyRate,
                        companyWebsiteURL: $scope.bregisterForm.companyWebsiteURL,
                        companySunStart: $scope.businessHoursSunStart,
                        companySunEnd: $scope.businessHoursSunEnd,
                        companyMonStart: $scope.businessHoursMonStart,
                        companyMonEnd: $scope.businessHoursMonEnd,
                        companyTueStart: $scope.businessHoursTueStart,
                        companyTueEnd: $scope.businessHoursTueEnd,
                        companyWedStart: $scope.businessHoursWedStart,
                        companyWedEnd: $scope.businessHoursWedEnd,
                        companyThuStart: $scope.businessHoursThuStart,
                        companyThuEnd: $scope.businessHoursThuEnd,
                        companyFriStart: $scope.businessHoursFriStart,
                        companyFriEnd: $scope.businessHoursFriEnd,
                        companySatStart: $scope.businessHoursSatStart,
                        companySatEnd: $scope.businessHoursSatEnd,
                        companyAlwaysOpen: $scope.userAlwaysOpen,
                        aboutUs: $scope.bregisterForm.aboutUs,
                        token: $scope.bregisterForm.token,
                        email: $scope.bregisterForm.email
                    };
                    angular.forEach($scope.output, function (value, index) {
                        angular.forEach(value, function (val, key) {
                            data[key] = val;
                        })
                    });
                    $http.post('/bcompleteprofile', data)
                        .then(function onSuccess(data) {
                            if (data.data.success == true && data.status == 200) {
                                window.location = '/';
                                return;
                            }
                        })
                        .catch(function onError(sailsResponse) {
                            toastr.error(sailsResponse.data.msg, 'Error');
                            return;
                        })
                        .finally(function eitherWay() {
                            $scope.bregisterForm.loading = false;
                        });

                } else {
                    $scope.bregisterForm.loading = true;
                    var data = {
                        userType: 'Individual',
                        userFirstName: $scope.bregisterForm.userFirstName,
                        userLastName: $scope.bregisterForm.userLastName,
                        userStreetAddress: $scope.bregisterForm.userStreetAddress,
                        userZipcode: $scope.bregisterForm.userZipcode,
                        userCity: $scope.bregisterForm.userCity,
                        userLat: $scope.bregisterForm.userLat,
                        userLng: $scope.bregisterForm.userLng,
                        userState: $scope.bregisterForm.userState,
                        userWorkPhone: $('#userWorkPhone').val(),
                        userHourlyRate: $scope.bregisterForm.userHourlyRate,
                        userWebsiteURL: $scope.bregisterForm.userWebsiteURL,
                        userSunStart: $scope.businessHoursSunStart,
                        userSunEnd: $scope.businessHoursSunEnd,
                        userMonStart: $scope.businessHoursMonStart,
                        userMonEnd: $scope.businessHoursMonEnd,
                        userTueStart: $scope.businessHoursTueStart,
                        userTueEnd: $scope.businessHoursTueEnd,
                        userWedStart: $scope.businessHoursWedStart,
                        userWedEnd: $scope.businessHoursWedEnd,
                        userThuStart: $scope.businessHoursThuStart,
                        userThuEnd: $scope.businessHoursThuEnd,
                        userFriStart: $scope.businessHoursFriStart,
                        userFriEnd: $scope.businessHoursFriEnd,
                        userSatStart: $scope.businessHoursSatStart,
                        userSatEnd: $scope.businessHoursSatEnd,
                        userAlwaysOpen: $scope.userAlwaysOpen,
                        aboutUs: $scope.bregisterForm.aboutUs,
                        token: $scope.bregisterForm.token,
                        email: $scope.bregisterForm.email
                    };

                    $scope.businessHoursSunStart = 'Close';
                    $scope.businessHoursSunEnd = '05:00 PM';
                    $scope.businessHoursMonStart = '09:00 AM';
                    $scope.businessHoursMonEnd = '05:00 PM';
                    $scope.businessHoursTueStart = '09:00 AM';
                    $scope.businessHoursTueEnd = '05:00 PM';
                    $scope.businessHoursWedStart = '09:00 AM';
                    $scope.businessHoursWedEnd = '05:00 PM';
                    $scope.businessHoursThuStart = '09:00 AM';
                    $scope.businessHoursThuEnd = '05:00 PM';
                    $scope.businessHoursFriStart = '09:00 AM';
                    $scope.businessHoursFriEnd = '05:00 PM';
                    $scope.businessHoursSatStart = 'Close';
                    $scope.businessHoursSatEnd = '05:00 PM';

                    angular.forEach($scope.output, function (value, index) {
                        angular.forEach(value, function (val, key) {
                            data[key] = val;
                        })
                    });
                    $http.post('/bcompleteprofile', data)
                    .then(function onSuccess(data) {
                      if (data.data.success == true && data.status == 200) {
                          window.location = '/';
                          return;
                        }
                    })
                    .catch(function onError(sailsResponse) {
                      toastr.error(sailsResponse.data.msg , 'Error');
                      return;
                    })
                    .finally(function eitherWay() {
                      $scope.bregisterForm.loading = false;
                    });
                }


    };

}]);
function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}