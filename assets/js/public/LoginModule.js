App.controller('LoginController', ['$scope', '$http', 'toastr', 'config', function ($scope, $http, toastr, config) {
    $scope.loginForm = {
        loading: false
    };

    $scope.forgotForm = {
        loading: false
    };
    $scope.emailError = document.cookie.match('(^|;) ?emailError=([^;]*)(;|$)');
    if ($scope.emailError != null) {
        sessionStorage.setItem('emailError', true);
    }
    if (sessionStorage.getItem('emailError')) {
        setTimeout(function () {
            toastr.error('This email has been already taken.', 'Error');
            sessionStorage.removeItem('emailError');
            return;

        }, 500);
        //

    }


    $scope.submitLoginForm = function () {
        $scope.loginForm.loading = true;
        $scope.master = {};

        $http.post('/login', {

            email: $scope.loginForm.email,
            password: $scope.loginForm.password

        })
            .then(function onSuccess(sailsResponse) {
                //console.log(sailsResponse);
                window.location = '/';

            })
            .catch(function onError(sailsResponse) {
                var invalidCredentials = sailsResponse.status == 404;
                if (invalidCredentials) {
                    toastr.error('Invalid username/password, please try again.', 'Error');
                    return;
                }
            })
            .finally(function eitherWay() {
                $scope.loginForm.loading = false;
            });
    };

    $scope.submitForgotForm = function () {
        $scope.forgotForm.loading = true;
        $scope.master = {};

        $http.post('/forgot', {

            email: $scope.forgotForm.email,
            password: $scope.forgotForm.password

        })
            .then(function onSuccess(sailsResponse) {
                window.location = '/';
            })
            .catch(function onError(sailsResponse) {
                var invalidCredentials = sailsResponse.status == 404;
                if (invalidCredentials) {
                    toastr.error('Invalid username/password, please try again.', 'Error');
                    return;
                }
            })
            .finally(function eitherWay() {
                $scope.forgotForm.loading = false;
            });

    };

    function get_cookie() {
        $scope.authType = document.cookie.match('(^|;) ?authType=([^;]*)(;|$)');
        $scope.token = document.cookie.match('(^|;) ?token=([^;]*)(;|$)');
        $scope.email = document.cookie.match('(^|;) ?email=([^;]*)(;|$)');
        $scope.emailError = document.cookie.match('(^|;) ?emailError=([^;]*)(;|$)');
        if ($scope.emailError != null) {
            sessionStorage.setItem('emailError', true);
        }
        if ($scope.token != null) {
            sessionStorage.setItem('token', unescape($scope.token[2]));
        }

        sessionStorage.setItem('authType', $scope.authType);
        window.location = config.local + '/ssactivate?token=' + unescape($scope.token[2]) + '&email=' + unescape($scope.email[2]);

        //console.log($scope.token, $scope.email);
    }


    $scope.loginBtn = function (name) {
        if ('facebook' == name) {
            var win = window.open('/auth/facebook', "_blank", "toolbar=no, scrollbars=no, resizable=no, top=100, left=100, width=600, height=600");
        } else {
            var win = window.open('/auth/google', "_blank", "toolbar=no, scrollbars=no, resizable=no, top=100, left=100, width=600, height=600");
        }

        var closeCheck = setInterval(function (type) {
            try {
                (win == null || win.closed) && (clearInterval(closeCheck), get_cookie());
            } catch (ex) {
            }
        }, 500);
    };

}]);
