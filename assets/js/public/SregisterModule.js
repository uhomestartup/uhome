'use strict'
App.controller('SregisterController', ['$scope', '$http', 'toastr', '$q', 'config', function ($scope, $http, toastr, $q, config) {
    $scope.respInfo         = [];
    $scope.profilePhotoGif  = false;
    $scope.coverBannerGif   = false;
    $scope.sregisterForm    = [];
    $scope.url              = '';
    $scope.statusUploadedTrue = false;

    $scope.initFunc = function(stringifiedArray) {
        $scope.sregisterForm.firstName = JSON.parse(stringifiedArray);
    }

    $scope.sregisterForm = {
        loading: false,
        services: 'Home Cleaning'
    };

    //debugger;
    $scope.token = document.cookie.match( '(^|;) ?token=([^;]*)(;|$)' );
    if($scope.token != null){
        setTimeout(function(){
            //sessionStorage.removeItem('token');
            close();
        }, 500);
    }

    //$scope.updateTokenAndEmail = function () {
        $scope.sregisterForm.email = getParameterByName('email');
        $scope.sregisterForm.token = getParameterByName('token');
    //}

        $scope.files = [];
        $scope.outputSequense = [];
        $scope.output = [];

    angular.element('#profilePhoto').change(function() {
        $scope.profilePhotoGif = true;
        toastr.info('Please wait while processing your request', 'Processing');
        if (document.getElementById('profilePhoto').files.length > 0) {
            var closeProfilePhoto = setInterval(function(){
                if(!$scope.statusUploadedTrue){
                    $scope.files.push(uploadImage(document.getElementById('profilePhoto').files[0], 'profilePhoto'));
                    $scope.statusUploadedTrue = true;
                    $q.all($scope.files)
                        .then(function (response) {
                            $scope.respInfo = response;
                            $scope.respInfo.forEach(function(item){
                                if(item.data.imageType == 'profilePhoto'){
                                    $scope.profilePhoto = item.data.url;
                                    $scope.statusUploadedTrue = false;
                                    clearInterval(closeProfilePhoto);
                                }
                            });
                            $scope.profilePhotoGif = false;
                        }).catch(function onError(sailsResponse) {
                            toastr.error(sailsResponse.data.msg, sailsResponse.data.toBigImageError);
                            return;
                        });
                    $scope.outputSequense.push('profilePhoto');
                }
            },1000);

        }
    });

    angular.element('#coverBanner').change(function() {
        $scope.coverBannerGif  = true;
        toastr.info('Please wait while processing your request', 'Processing');
        if (document.getElementById('coverBanner').files.length > 0) {
            var closeCoverBanner = setInterval(function(){
                if(!$scope.statusUploadedTrue){
                    $scope.files.push(uploadImage(document.getElementById('coverBanner').files[0], 'coverBanner'));
                    $scope.statusUploadedTrue = true;
                    $q.all($scope.files)
                        .then(function (response) {
                            $scope.respInfo = response;
                            $scope.respInfo.forEach(function(item){
                                if(item.data.imageType == 'coverBanner'){
                                    $scope.coverBanner = item.data.url;
                                    $scope.statusUploadedTrue = false;
                                    clearInterval(closeCoverBanner);
                                }
                            });
                            $scope.coverBannerGif = false;
                        }).catch(function onError(sailsResponse) {
                            toastr.error(sailsResponse.data.msg, sailsResponse.data.toBigImageError);
                            return;
                        });
                    $scope.outputSequense.push('coverBanner');
                }
            },1000);

        }
    });

    $scope.registerForm = function () {
        toastr.info('Please wait while processing your request', 'Processing');
        $q.all($scope.files)
            .then(function (response) {
                console.log(response);
                var i = 0;
                angular.forEach($scope.outputSequense, function (index, value) {
                    var obj = {};
                    obj[index] = response[i].data.url;
                    $scope.output.push(obj);
                    i++;
                });
                $scope.submitRegistrationForm($scope.output);
            }).catch(function (err) {
            });
    };

    $scope.validateZipcode = function () {

        var zipcode = $scope.sregisterForm.zipcode;

        $http.post('/getzipcodedetails', {
            zipcode: zipcode
        })
            .then(function onSuccess(response) {
                if (response.status == 200) {
                    $scope.sregisterForm.state = response.data.state;
                    $scope.sregisterForm.city = response.data.city;
                    $scope.sregisterForm.lng = response.data.lng;
                    $scope.sregisterForm.lat = response.data.lat;
                } else {
                    toastr.error(sailsResponse.data.msg, 'Error');
                    return;
                }
            })
            .catch(function onError(sailsResponse) {
                toastr.error(sailsResponse.data.msg, 'Error');
                return;
            });
    }

    var uploadImage = function (file, name) {
        var url = config.local+'/upload/upload?token='+$scope.sregisterForm.token+'&imageType='+name;
        var fd = new FormData();
        fd.append(name,file);
        console.log(fd);
        return $http.post(url, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        });
    };

    $scope.submitRegistrationForm = function (images) {
        $scope.sregisterForm.loading = true;
        var data = {
            firstName: $scope.sregisterForm.firstName,
            lastName: $scope.sregisterForm.lastName,
            phone: $('#phone').val(),
            address: $scope.sregisterForm.streetAddress,
            zipcode: $scope.sregisterForm.zipcode,
            city: $scope.sregisterForm.city,
            state: $scope.sregisterForm.state,
            aboutMe: $scope.sregisterForm.aboutUs,
            email: $scope.sregisterForm.email,
            token: $scope.sregisterForm.token,
            lng: $scope.sregisterForm.lng,
            lat: $scope.sregisterForm.lat
        };
        angular.forEach(images, function (value, index) {
            angular.forEach(value, function (val, key) {
                data[key] = val;
            })
        });

        $http.post('/profileComplete', data)
            .then(function onSuccess(data) {
                if (data.data.success == true && data.status == 200) {
                    window.location = '/';
                    return;
                }
            })
            .catch(function onError(data) {
                var emailAddressAlreadyInUse = data.status == 409;
                if (emailAddressAlreadyInUse) {
                    toastr.error('That email address has already been taken, please try again,', 'Error');
                    return;
                }
            })
            .finally(function eitherWay() {
                $scope.sregisterForm.loading = false;
            });
    };

    $scope.authType = sessionStorage.getItem('authType');

    function getParameterByName(name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    }
}]);

