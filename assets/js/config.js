'use strict';
App.factory('config',
    ['$http','$location',
        function ( $http, $location ) {
            let config = {};
            if($location.host()=='localhost'){
                config.local = 'http://localhost:'+$location.port();
            }else{
                config.local = 'http://'+$location.host()+':'+$location.port();
            }
            //console.log(config);
            return config;
        }
    ]
);