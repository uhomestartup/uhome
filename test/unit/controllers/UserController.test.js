var url = 'http://localhost:1337/';
var request = require('supertest')(url);

describe("User model", function() {
	it("insert new User", function(done) {
		var req = request.post('user/create');
		req.send({
				firstName: 	"Talib",
				lastName:  	"Allauddin",
				email: 		"talib@digitemb.com",
				status: 	"Inactive"
		});
		req.end(function(err, res) {
			if (err) throw err;	
			console.log(res.text);
			done();
		});
	});
});