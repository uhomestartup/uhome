'use strict';
/**
 * Sends to Homepage
 *
 * Usage:
 * return res.backToHomePage();
 * return res.backToHomePage(statusCode);
 *
 * e.g.:
 * ```
 * return res.backToHomePage(
 * 	statusCode
 * );
 * ```
 */
 module.exports = function backToHomePage (statusCode) {
  let req = this.req,
      res = this.res;

  if(req.wantsJSON) {
    return res.send(statusCode||200);
  }

  return res.redirect('/');
};
