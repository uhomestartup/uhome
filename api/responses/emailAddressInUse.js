/**
 * 409 Send error messages, Email address is already taken
 *
 * Usage:
 * return res.emailAddressInUse();
 *
 * e.g.:
 * ```
 * return res.emailAddressInUse();
 * ```
 */
module.exports = function emailAddressInUse() {
  var res = this.res;

  return res.send(409, 'Email address is already taken by another user.');
};
