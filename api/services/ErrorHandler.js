'use strict';
/**
 * ErrorHandler
 *
 * Author       :: Talib Allauddin
 * Email        :: talib@bitcloudglobal.com
 * @description :: Handles the response containing some error
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
module.exports = {

    errorResponse: (err, req, res, next) => {

        return res.json({status: 'error', code: err.code, details: err.details});
    }
};