'use strict';

let _ = require('lodash');

module.exports = {

/*  get: function(callback) {
    // Sets Searching parameters
    let conditionsApply = {status: 'Active', deleted: 'false'};

    Service.find({where: conditionsApply, sort: 'title'}).populate('SubService').exec(function( err, service ) {
      if (err) ErrorHandler.errorResponse(err, req, res, next);

      callback(service);
    });
  },*/

  /*getBy: function(keyword, callback) {
    
  },*/

    parseBusinessDaysAndServicesForPublicProfile: (contractor) => {
        let contractorData = contractor;
        let businessDays = contractor.businessDays;
        let services = contractor.services;
        delete contractorData['businessDays'];
        delete contractorData['services'];

        for (let day in businessDays) {
            if (businessDays[day] !== 'Close' && businessDays[day] !== '24hr') {
                if (contractor.is24Hours !== true) {
                    contractorData[day] = businessDays[day].start + " - " + businessDays[day].end;
                } else {
                    contractorData[day] = "24hr";
                }

            } else if(businessDays[day] === '24hr') {
                contractorData[day] = "24hr";
            }
            else {
                contractorData[day] = 'Close';
            }
        }
        contractorData.services = [];
        services.forEach((service) => {
            contractorData.services.push(service.name);
        });

        return contractorData;
    },


  parseBusinessDaysAndServices: (contractor) => {
    let contractorData = contractor;
    let businessDays = contractor.businessDays;
    let services = contractor.services;
    delete contractorData['businessDays'];
    delete contractorData['services'];

    for (let day in businessDays) {
        if (businessDays[day] !== 'Close' && businessDays[day] !== '24hr') {
            if (contractor.is24Hours !== true) {
                contractorData[day + "End"] = businessDays[day].end;
                contractorData[day + "Start"] = businessDays[day].start;
            } else {
                contractorData[day + "Start"] = "24hr";
                contractorData[day + "End"] = "05:00 PM"
            }

        } else if(businessDays[day] === '24hr') {
            contractorData[day + "Start"] = "24hr";
            contractorData[day + "End"] = "05:00 PM"
        }
        else {
            contractorData[day + "Start"] = 'Close';
        }
    }
    contractorData.services = [];
    services.forEach((service) => {
        contractorData.services.push(service.name);
    });

    return contractorData;
},

    getCurrentUserData: (session, callback) => {
    if(session.userType === 'ss'){
        Serviceseeker.find({
            id: session.me
        },{fields:{activationToken: 0}}, function foundUser(err, user) {

            if (err) callback (null, err);

            if (!user)  callback (null, {error: 'User not found!'});

            callback({currentUser: user[0]})
        })
    } else if(session.userType === 'sp'){
        Contractor.find({
            id: session.me
        },{fields:{activationToken: 0}}, function foundUser(err, user) {

            if (err) callback (null, err);

            if (!user)  callback (null, {error: 'User not found!'});

            callback({currentUser: user[0]})
        })
    }
},

  checkIsSubServicesExistInTable: (newSubServices, callback) => {

      Subservices.find((err, subservices) => {
            callback (_.xorBy(subservices, newSubServices, 'name'));
        });
  }
};
