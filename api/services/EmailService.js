'use strict';
/**
 * ErrorHandler
 *
 * Author       :: Talib Allauddin
 * Email        :: talib@bitcloudglobal.com
 * @description :: Handles the errorResponse containing some error
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

let Nodemailer = require('nodemailer');

module.exports = {

  sendEmail: function(options) {

      // create reusable transporter object using the default SMTP transport
      let transporter = Nodemailer.createTransport({
          service: 'gmail',
          auth: {
              user: 'uhomemailservice@gmail.com',
              pass: 'ironmaide'
          }
      });

      // setup email data with unicode symbols
      let mailOptions = {
          from: 'no-reply@uhomeusa.com', // sender address
          to: options.to, // list of receivers
          subject: options.subject, // Subject line
          html: options.html // html body
      };

      // send mail with defined transport object
      transporter.sendMail(mailOptions, (error, info) => {
          if (error) return console.log(error);

          console.log('Message %s sent: %s', info.messageId, info.errorResponse);
          return;
      });

  },

    sendActivationEmail: (email, activationToken, userType) => {
        let link = undefined;
        if (userType == 'business') {
            link = sails.config.hostname + 'bregister?token='+activationToken+'&email='+email;
        } else if (userType == 'user') {
            link = sails.config.hostname + 'ssactivate?token='+activationToken+'&email='+email;
        }
        let subject = 'Welcome to Uhome!',
            message = 'Thankyou for signing up on Uhome, Please click on the following link for account activation<br/>';

        message += '<a href="'+link+'">'+link+'</a>';

        EmailService.sendEmail({
            fromName: 'Uhome USA',
            from: 'no-reply@uhomeusa.com',
            to: email,
            subject: subject,
            html: message
        });
    },

    sendRecoveryEmail: (email, activationToken) => {
        let link = sails.config.hostname + 'recover?token='+activationToken+'&email='+email,
            subject = 'Welcome back to Uhome!',
            message = 'Seems like you are lost, please follow this link to recover your account<br/>';

        message += '<a href="'+link+'">'+link+'</a>';

        EmailService.sendEmail({
            fromName: 'Uhome USA',
            from: 'no-reply@uhomeusa.com',
            to: email,
            subject: subject,
            html: message
        });
    },

    sendForgotPasswordEmail: (email, token) => {
        let link = sails.getBaseUrl() + '/' + 'forgotCallback?token='+token+'&email='+email,
            subject = 'Uhome password recovery!',
            message = 'Please follow the link to reset your password<br/>';

        message += '<a href="'+link+'">'+link+'</a>';

        EmailService.sendEmail({
            fromName: 'Uhome USA',
            from: 'no-reply@uhomeusa.com',
            to: email,
            subject: subject,
            html: message
        });
    }
};
