'use strict';

/**
 * UtilityHandler Methods
 *
 * Author       :: Talib Allauddin
 * Email        :: talib@bitcloudglobal.com
 * Date            :: June 30th, 2016
 * @description :: Implements utility functions
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
let _ = require('lodash');

let Promise = require('q');

module.exports = {

    verifyZipcode: (zipcode, callback) => {

        let request = require('request');

        request({
            url: 'https://maps.googleapis.com/maps/api/geocode/json?address=' + zipcode + '&sensor=true&key=AIzaSyBTywfP5WXKkULkOFRLkWJYDnFipc4BCS8',
            headers: {
                'User-Agent': 'request'
            }
        }, (error, response, body) => {
            if (body) {
                let info = JSON.parse(body);

                if (info.status == 'ZERO_RESULTS' || info.results[0] == undefined) {
                    return callback({
                        error: 'Invalid Zipcode',
                        status: 409
                    });
                }

                let zipcode = {
                    lng: info.results[0].geometry.location.lng,
                    lat: info.results[0].geometry.location.lat,
                };

                _.each(info.results[0].address_components, (component) => {
                    let types = component.types;
                    _.each(types, (type) => {
                        if (type == 'locality' || type == 'neighborhood' || type == 'sublocality') {
                            zipcode.city = component.long_name;
                        }
                        if (type == 'administrative_area_level_1') {
                            zipcode.state = component.long_name;
                        }
                        if (type == 'country') {
                            zipcode.country = component.short_name;
                        }
                    });
                });

                if (!error && response.statusCode == 200) {

                    if (
                        zipcode.country != 'US') {
                        return callback({
                            error: 'Signup only allowed from United States',
                            status: 404
                        });
                    }

                    return callback({
                        data: zipcode,
                        status: 200
                    });
                }
            }

            return callback({
                error: 'Invalid Zipcode',
                status: 409
            });
        });
    },

    // Generates speical key for different purposes
    generateKey: () => {
        let text = "",
            possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for (let i = 0; i < 64; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));

        return text;
    },

    // Generates speical key for different purposes
    generateKeyByLength: (n) => {
        let text = "",
            possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for (let i = 0; i < n; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));

        return text;
    },

    // Generates the random name for uploaded file
    randomString: (length, chars) => {
        let mask = '';
        if (chars.indexOf('a') > -1) mask += 'abcdefghijklmnopqrstuvwxyz';
        if (chars.indexOf('A') > -1) mask += 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        if (chars.indexOf('#') > -1) mask += '0123456789';
        if (chars.indexOf('!') > -1) mask += '~`!@#$%^&*()_+-={}[]:";\'<>?,./|\\';
        let result = '';
        for (let i = length; i > 0; --i) result += mask[Math.floor(Math.random() * mask.length)];
        return result;
    }

};
