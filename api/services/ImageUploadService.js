'use strict';
/**
 * ErrorHandler
 *
 * Author       :: Talib Allauddin
 * Email        :: talib@bitcloudglobal.com
 * @description :: Handles the errorResponse containing some error
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

let configs = {
    adapter: require('skipper-s3'),
    key: 'AKIAJVNE6CJ3CKWFL6JA',
    secret: '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',
    bucket: 'uhome',
    headers: {
        'x-amz-acl': 'public-read'
    }
};
let request = require('request'),
    fs = require('fs'),
    _ = require('lodash');


let userType = undefined,
    imageType = undefined,
    imageDir = undefined,
    convertedImageName = undefined,
    imagesNames = [];


module.exports = {
    uploadImage: (req, res, callback) => {
        let dirname = undefined;
        imageType = req.query.imageType;
        //convertedImageName = UtilityHandler.randomString(32, '#aA') + '.jpg';


        if (imageType === 'profilePhoto') {
            dirname = require('path').resolve(sails.config.appPath, 'assets/images/profilePhotos');
            imageDir = 'profilePhotos';
        } else if (imageType === 'coverBanner') {
            dirname = require('path').resolve(sails.config.appPath, 'assets/images/bannerPhotos');
            imageDir = 'bannerPhotos';
        } else if (imageType === 'imageLogo') {
            dirname = require('path').resolve(sails.config.appPath, 'assets/images/logoPhotos');
            imageDir = 'logoPhotos';
        } else if (imageType === 'attachment[]') {
            dirname = require('path').resolve(sails.config.appPath, 'assets/images/attachmentPhotos');
            imageDir = 'attachmentPhotos';
        }

        let activationTokenKey = undefined,
            email = undefined,
            userId = undefined,
            imageData = undefined,
            userProfilePhoto = undefined,
            userBannerPhoto = undefined,
            userLogoPhoto = undefined;

        Contractor.findOne({
            //activationToken:req.session.activationToken
            activationToken: req.param('token')
        }).exec((err, contractor) => {
            Serviceseeker.findOne({
                //activationToken:req.session.activationToken
                activationToken: req.param('token')
            }).exec((err, serviceseeker) => {

                if(imageType !== 'attachment[]'){
                    userProfilePhoto = contractor === undefined ? serviceseeker.profilePicture : contractor.profilePicture;
                    userBannerPhoto = contractor === undefined ? serviceseeker.bannerPicture : contractor.bannerPicture;
                    userLogoPhoto = contractor === undefined ? serviceseeker.logoPhoto : contractor.logoPhoto;
                    userType = contractor === undefined ? 'user' : 'business';
                    activationTokenKey = contractor === undefined ? serviceseeker.activationToken : contractor.activationToken;
                    email = contractor === undefined ? serviceseeker.email : contractor.email;
                    userId = contractor === undefined ? serviceseeker.id : contractor.id;

                    if (imageType === 'profilePhoto' && userProfilePhoto !== undefined) {
                        try {
                            fs.unlinkSync(userProfilePhoto);
                        } catch (err) {
                            console.log('File not found');

                        }

                    } else if (imageType === 'coverBanner' && userBannerPhoto !== undefined) {
                        try {
                            fs.unlinkSync(userBannerPhoto);
                        } catch (err) {
                            console.log('File not found');
                        }

                    } else if (imageType === 'imageLogo' && userLogoPhoto !== undefined) {
                        try {
                            fs.unlinkSync(userLogoPhoto);
                        } catch (err) {
                            console.log('File not found');
                        }

                    }
                }

                let i = 0;
                req.file(imageType).upload({
                    maxBytes: 10000000,
                    saveAs: (__newFileStream, next) => {
                        i++;
                        convertedImageName = UtilityHandler.randomString(32, '#aA') + i +'.jpg'
                        imagesNames.push(convertedImageName);
                        return next(undefined, convertedImageName);
                    },//convertedImageName,
                    dirname: dirname
                }, function whenDone(err, uploadedFiles) {
                    if (err) {
                        console.log(err);
                        if(err.code === 'E_EXCEEDS_UPLOAD_LIMIT'){
                            err['toBigImageError'] = 'The uploading image is to big';
                            return res.negotiate(err);
                        } else{
                            return res.negotiate(err);
                        }

                    }
                    if (uploadedFiles.length === 0) {
                        return callback ({
                            imageType: imageType,
                            url: undefined
                        });
                    }


                    if (imageType === 'profilePhoto') {
                        imageData = {
                            profilePicture: sails.config.hostname + "images/" + imageDir + "/" + imagesNames[0],
                        };
                    } else if (imageType === 'coverBanner') {
                        imageData = {
                            bannerPicture: sails.config.hostname + "images/" + imageDir + "/" + imagesNames[0],
                        };
                    } else if (imageType === 'imageLogo') {
                        imageData = {
                            logoPhoto: sails.config.hostname + "images/" + imageDir + "/" + imagesNames[0],
                        };
                    } else if (imageType === 'attachment[]') {
                            imageData = [];
                            _.forEach(imagesNames, (imageName) => {
                            imageData.push(sails.config.hostname + "images/" + imageDir + "/" + imageName);
                        });
                    }

                    if (userType === 'user' && imageType !== 'attachment[]') {
                        Serviceseeker.update({
                                activationToken: req.param('token')
                            },
                            imageData
                        ).exec((err, data) => {
                            if (err) return res.negotiate(err);

                            getImage(imageType, imagesNames[0], imageDir, res, callback);
                        });

                    } else if (userType !== 'user' && imageType !== 'attachment[]') {
                        Contractor.update({
                                activationToken: req.param('token')
                            },
                            imageData
                        ).exec((err, data) => {
                            if (err) return res.negotiate(err);
                            getImage(imageType, imagesNames[0], imageDir, res, callback);
                        });
                    } else if (imageType === 'attachment[]') {

                        /*books.findOne(refid).exec(function(err,book) {
                         book.attachments.push( arr[0] );
                         book.save(function(err){
                         // something here
                         });
                         });*/
                                imagesNames = [];
                                callback({
                                    imageData: imageData,
                                });

                    }
                });
            });
        });
    }
};

function getImage(imageType, convertedImageName, imageDir, res, callback) {


    let isNeed = true;
    let setIntervalRepeats = 0;
    let interval = setInterval((imageType, url, host) => {
        console.log("URL - " + url);
        console.log("Host - " + host);
        imagesNames = [];
        request(host + url, (error, response, body) => {
            setIntervalRepeats++;
            if (!error && response.statusCode == 200 && isNeed) {
                isNeed = false;
                clearInterval(interval);
                callback({
                    imageType: imageType,
                    url: url
                }); /*res.json({
                    imageType: imageType,
                    url: url
                })*/
            } else if (setIntervalRepeats === 10) {
                isNeed = false;
                clearInterval(interval);
                callback({
                    imageType: imageType,
                    url: url
                });
                /*return res.json({
                    imageType: imageType,
                    url: undefined
                });*/
            }
        });
    }, 500, imageType, "/images/" + imageDir + "/" + convertedImageName, sails.config.hostname);
}