/**
 * Serviceseeker.js
 * Author		:: Talib Allauddin
 * Email		:: talib@bitcloudglobal.com
 * Date			:: June 28th, 2016
 * @description :: Service seekers are the users who'll be using this service as customers.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

    schema: true,
	  attributes: {

		firstName: {
			type: 'string',
	  		required: true
		},

		middleName: {
			type: 'string'
		},

		lastName: {
			type: 'string',
			required: true
		},

		email: {
			type: 'string',
			email: true,
			required: true,
			unique: true
		},

		phone: {
			type: 'string'
		},

		address: {
			type: 'string'
		},

		city: {
			type: 'string'
		},

		state: {
			type: 'string'
		},

		country: {
			type: 'string'
		},

		zipcode: {
			type: 'string'
		},

		longitute: {
			type: 'string'
		},

		latitude: {
			type: 'string'
		},

		gender: {
			type: 'string'
		},

		interestedIn: [{
        	model: 'subservice'
		}],

		recommendedBy: {
			type: 'string'
		},

		aboutMe: {
			type: 'string'
		},

		profilePicture: {
			type: 'string',
		},

		bannerPicture: {
			type: 'string',
		},

		encryptedPassword: {
			type: 'string'
		},

		favouriteGroups: [{
			type: 'string'
		}],

		uid: {
			type: 'string'
		},

		provider: {
			type: 'string'
		},

		phoneVerified: {
			type: 'boolean',
			defaultsTo: false
		},

		emailVerified: {
			type: 'boolean',
			defaultsTo: false
		},

		status: {
			type: 'string',
			required: true,
			defaultsTo: 'Inactive',
			enum: ['Active', 'Inactive']
		},

		invited: {
            type: 'boolean',
            defaultsTo: true
		},

		deleted: {
			type: 'string',
			required: true,
			defaultsTo: 'false',
    		enum: ['true', 'false']
		},

		activationToken: {
			type: 'text'
		},

		userType: {
		  type: 'string',
          defaultsTo: 'ServiceSeeker'
		},

		online: {
			type: 'boolean',
			defaultsTo: false
		},

		lastLogin: {
			type: 'string'
		},

		lastLoginIP: {
			type: 'string',
			ip: true
		},

	    Projects: {
	      collection: 'projects',
	      via: 'serviceSeekerId'
	    },

	    residentSince: {
	    	type: 'string'
	    }
	},

  	beforeCreate: function( values, next ) {
	    next();
  	}
};
