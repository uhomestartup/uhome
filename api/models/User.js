/**
 * User.js
 * Author		:: Talib Allauddin
 * Email		:: talib@bitcloudglobal.com
 * Date			:: June 23rd, 2016
 * @description :: These are the backend admin users, which will be responsible for handling website content and policies.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  	schema: true,
  	attributes: {
	  	firstName: {
	  		type: 'string',
	  		required: true
	  	},

	  	middleName: {
	  		type: 'string',
	  		defaultsTo: ''
	  	},

	  	lastName: {
	  		type: 'string',
	  		required: true
	  	},

	  	cell: {
	  		type: 'string'
	  	},

	  	address: {
	  		type: 'string'
	  	},

	  	city: {
	  		type: 'string'
	  	},

	  	state: {
	  		type: 'string'
	  	},

	  	postcode: {
	  		type: 'string'
	  	},

	  	country: {
	  		type: 'string',
	  		defaultsTo: 'US'
	  	},

	  	online: {
	  		type: 'string',
	  		boolean: true
	  	},

	  	email: {
	  		type: 'string',
	  		required: true,
	  		email: true,
	  		unique: true
	  	},

	  	encryptedPassword: {
	  		type: 'string'
	  	},

	  	cellVerified: {
	  		type: 'string',
	  		boolean: true
	  	},

	  	emailVerified: {
	  		type: 'string',
	  		boolean: true
	  	},

	  	ip: {
	  		type: 'string',
	  		ip: true
	  	},

	  	lastLogin: {
	  		type: 'string',
	  		datetime: true
	  	},

	  	status: {
	  		type: 'string',
	  		required: true
	  	},

	  	deleted: {
	  		type: 'boolean'
	  	}
  	},

  	beforeCreate: function( values, next ) {

	    values.deleted = 'false';
	    values.cellVerified = 'false';
	    values.emailVerified = 'false';
	    next();
  	}
};