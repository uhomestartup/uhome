/**
 * Performers.js
 *
 * @description :: The list of connections between projects and contractors.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
      projectId: {
          type: 'string',
          required: true
      },

      contractorId: {
          model: 'contractor'
      },
      status: {
          type: 'string',
          required: true,
          defaultsTo: 'Applicant',
          enum: ['Applicant', 'Hired', 'Denied']
      },
      description: {
          type: 'string',
          required: true
      }
  }
};

