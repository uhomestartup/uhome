/**
 * Worklines.js
 *
 * @description :: The list of worklines (or it can be called 'services' on the front-end ) which are allowed in UHome. You can find the insert command in the /docs/db-collections-data.md
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
      name: {
          type: 'string'
      }
  }
};

