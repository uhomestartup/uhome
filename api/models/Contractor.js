/**
 * Contractor.js
 *
 * Date			:: June 28th, 2016
 * @description :: Contractors of UHome. There are two type of contractors: Individual and Company. Both of them are saved to 'contractor' collection.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

	attributes: {

		firstName: {
			type: 'string',
	  		required: true
		},

		middleName: {
			type: 'string'
		},

		lastName: {
			type: 'string',
			required: true
		},

		email: {
			type: 'string',
			email: true,
			required: true,
			unique: true
		},

		phone: {
			type: 'string'
		},

		address: {
			type: 'string'
		},

		city: {
			type: 'string'
		},

		unit: {
			type: 'string'
		},

		state: {
			type: 'string'
		},

        licenseState: {
            type: 'string'
        },

		country: {
			type: 'string'
		},

		zipcode: {
			type: 'string'
		},

		longitute: {
			type: 'string'
		},

		latitude: {
			type: 'string'
		},

		businessDays: {
			type: 'json'
		},

		businessHours: {
			type: 'string'
		},

		hourlyRate: {
			type: 'string'
		},

		is24Hours: {
			type: 'boolean'
		},

		url: {
			type: 'string'
		},

		skills: {
			type: 'array'
		},

		userType: {
			type: 'string',
			enum: ['Individual', 'Business']
		},

        invited: {
            type: 'boolean',
            defaultsTo: true
        },

		businessName: {
			type: 'string'
		},

		businessEIN: {
			type: 'string'
		},

		bbbLink: {
			type: 'string'
		},

		language: {
			type: 'string',
            defaultsTo: 'English - [United States]'

		},

		services:{
            type: 'array',
            defaultsTo: []
		},

		licenseNumber: {
			type: 'array'
		},

        licenseExpiration: {
            type: 'string'
		},

		yearFounded: {
			type: 'string'
		},

		googleBusiness: {
			type: 'string'
		},

		employees: {
			type: 'json'
		},

        accreditationNumber: {
            type: 'string'
        },

        accreditation: {
            type: 'string'
        },

		aboutUs: {
			type: 'string'
		},

		profilePicture: {
			type: 'string'
		},

    	logoPhoto: {
			type: 'string'
		},

		bannerPicture: {
			type: 'string'
		},

		teamMembers: {
			type: 'array'
		},

		encryptedPassword: {
			type: 'string'
		},

		uid: {
			type: 'string'
		},

		provider: {
			type: 'string'
		},

		phoneVerified: {
			type: 'boolean',
			defaultsTo: false
		},

		emailVerified: {
			type: 'boolean',
			defaultsTo: false
		},

		status: {
			type: 'string',
			required: true,
			defaultsTo: 'Inactive',
			enum: ['Active', 'Inactive']
		},

		deleted: {
			type: 'string',
			required: true,
			defaultsTo: 'false',
      		enum: ['true', 'false']
		},

		activationToken: {
			type: 'string'
		},

        lineOfWork:{
            type: 'string'
		},

		online: {
			type: 'boolean',
			defaultsTo: false
		},

		lastLogin: {
			type: 'string'
		},

		lastLoginIP: {
			type: 'string',
			ip: true
		}
	},

  	beforeCreate: function( values, next ) {
	    next();
  	}
};
