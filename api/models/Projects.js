/**
 * Serviceseekerproject.js
 *
 * @description :: The list of projects which were created by serviceseeker
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

    schema: true,
    attributes: {

        service: {
            type: 'string',
            required: true
        },

        subService: {
            type: 'string',
        },

        serviceSeekerId: {
            type: 'string',
            required: true
        },

        startTime: {
            type: 'string'
        },

        jobName: {
            type: 'string',
            required: true
        },

        jobDescription: {
            type: 'string',
            required: true
        },

        attachments: {
            type: 'array'
        },

        estimatedBudget: {
            type: 'string'
        },

        status: {
            type: 'string',
            required: true,
            defaultsTo: 'Open',
            enum: ['Open', 'Closed']
        },

        whoCanSeePublication: {
            type: 'string',
            required: true,
            defaultsTo: 'All',
            enum: ['All', 'Recommended']
        }
    }
};
