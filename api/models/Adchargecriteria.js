/**
 * Adchargecriteria.js
 *
 * @description :: TODO: The purpose of this model is unknown
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

	schema: true,
	attributes: {
		charges: {
			type: 'string',
			required: true
		},

		createdBy: {
			model: 'user'
		},

		modifiedBy: {
			model: 'user'
		},
	}
};