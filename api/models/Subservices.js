/**
 * Subservices.js
 *
 * @description :: The list of subservices which are allowed in UHome. You can find the insert command in the /docs/db-collections-data.md
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
      name: {
          type: 'string',
          required: true
      },
  }
};

