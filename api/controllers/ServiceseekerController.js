'use strict';
/**
 * ServiceseekerController
 *
 * Author    :: Talib Allauddin
 * Email    :: talib@bitcloudglobal.com
 * Date      :: June 28th, 2016
 * @description :: Server-side logic for managing service seeker
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
let _ = require('lodash');

module.exports = {

    myProjects: (req, res) => {
        let hiredContractors = [];
        let i = 0;

        if (!req.session.me) {
            return res.view('common/homepage');
        }

        Serviceseeker.find({
            activationToken: req.session.activationToken
        }, {fields:{activationToken: 0}}).exec((err, serviceseeker) => {


            Projects.find({serviceSeekerId: serviceseeker[0].id},(err, projects) => {
                if (err) ErrorHandler.errorResponse(err, req, res, next);

                if(projects.length > 0){
                    projects.forEach((project) => {
                        Performers.find({
                            projectId: project.id,
                            status: 'Hired'
                        }).populate('contractorId').exec((err, performers) => {
                            if (err) ErrorHandler.errorResponse(err, req, res, next);
                            i++;

                            if(performers.length > 0){
                                performers.forEach((performer) => {
                                    delete performer.contractorId.activationToken;
                                    hiredContractors.push({
                                        contractorProject: project,
                                        performer: performer
                                    });
                                });
                            }
                            if(i === projects.length){
                                return res.view('ss/myprojects', {
                                    user: serviceseeker[0],
                                    contractors: JSON.stringify(hiredContractors),
                                    projects: JSON.stringify(projects),
                                });
                            }


                        });
                    });
                } else{
                    return res.view('ss/myprojects', {
                        user: serviceseeker[0],
                        contractors: JSON.stringify([]),
                        projects: JSON.stringify([]),
                    });
                }


            });

        });
    },

    update: (req, res, next) => {

        let updatedParams = req.body;

        Serviceseeker.update({'activationToken': req.session.activationToken}, updatedParams, (err, serviceseeker) => {

            if (err) ErrorHandler.errorResponse(err, req, res, next);

            res.json({"aboutMe": serviceseeker[0].aboutMe});
        });
    },

    editProfilePage: (req, res, next) => {

        if (!req.session.me) return res.view('common/homepage');

        Serviceseeker.find({
            activationToken: req.session.activationToken
        }, {fields:{activationToken: 0}}, (err, serviceseeker) => {

            if (err) ErrorHandler.errorResponse(err, req, res, next);

            return res.view('ss/editprofile', {user: serviceseeker[0]});
        });


    },

    projectDetails: (req, res, next) => {

        if (!req.session.me) {
            return res.view('common/homepage');
        }

        Serviceseeker.find({
            activationToken: req.session.activationToken
        }, {fields:{activationToken: 0}}).exec((err, serviceseeker) => {

            if (err) ErrorHandler.errorResponse(err, req, res, next);

            Projects.findOne({
                id: req.param('id')
            }, (err, project) => {
                if (err) ErrorHandler.errorResponse(err, req, res, next);

                Performers.find({projectId: req.param('id')}).populate('contractorId').exec((err, performers) => {
                    if (err) ErrorHandler.errorResponse(err, req, res, next);

                    /*_.forEach(performers, (performer)=> {
                        delete performer.contractorId.activationToken;
                    });*/

                    _.map(performers, (performer) => {delete performer.contractorId.activationToken;});

                    return res.view('ss/projectDetails', {
                        user: serviceseeker[0],
                        performers: JSON.stringify(performers),
                        project: JSON.stringify(project)
                    });
                });


            });

        });

    },

    newsFeed: (req, res) => {
        if (!req.session.me) {
            return res.view('common/homepage');
        }

        Serviceseeker.find({
            activationToken: req.session.activationToken
        }, {fields:{activationToken: 0}}).exec((err, serviceseeker) => {

            if (err) ErrorHandler.errorResponse(err, req, res, next);

            return res.view('ss/newsFeed', {user: serviceseeker[0]});
        });
    },

    findBusiness: (req, res) => {
        if (!req.session.me) {
            return res.view('common/homepage');
        }

        Serviceseeker.find({
            activationToken: req.session.activationToken
        }, {fields:{activationToken: 0}}).exec((err, serviceseeker) => {

            if (err) ErrorHandler.errorResponse(err, req, res, next);

            return res.view('ss/findBusiness', {user: serviceseeker[0]});
        });
    },

    recommendations: (req, res, next) => {

        if (!req.session.me) {
            return res.view('common/homepage');
        }

        Serviceseeker.find({
            activationToken: req.session.activationToken
        }, {fields:{activationToken: 0}}).exec((err, serviceseeker) => {

            if (err) ErrorHandler.errorResponse(err, req, res, next);

            return res.view('ss/recommendation', {user: serviceseeker[0]});
        });
    },

    getServiceSubServiceStartTimeForSSProject: (req, res, next) => {

        Projects.find({
           id: req.param('projectId')
        }).exec((err, project) => {

            if (err) ErrorHandler.errorResponse(err, req, res, next);

            return res.json({
                result:
                {
                    service: project[0].service,
                    subService: project[0].subService,
                    startTime: project[0].startTime
                }
            });
        });
    },

    activateAccount: (req, res, next) => {
        Serviceseeker.findOne({
            email: req.param('email'),
            activationToken: req.param('token')
        }).exec((err, user) => {

            if (!user) {
                return res.json({
                    status: 409,
                    notFound: "Account not found!"
                });
            } else {

                let profilePhoto = user.profilePicture,
                    coverBanner = user.bannerPicture;

                if (profilePhoto === undefined) profilePhoto = 'https://s3-us-west-2.amazonaws.com/uhome/avatar.png';
                if (coverBanner === undefined) coverBanner = 'https://s3-us-west-2.amazonaws.com/uhome/timeline.jpg';


                Serviceseeker.update({
                    email: req.param('email'),
                    activationToken: req.param('token')
                }, {
                    firstName: req.param('firstName'),
                    lastName: req.param('lastName'),
                    phone: req.param('phone'),
                    zipcode: req.param('zipcode'),
                    address: req.param('address'),
                    city: req.param('city'),
                    state: req.param('state'),
                    longitute: req.param('lng'),
                    latitude: req.param('lat'),
                    aboutMe: req.param('aboutMe'),
                    emailVerified: true,
                    status: 'Active',
                    profilePicture: profilePhoto,
                    bannerPicture: coverBanner,
                    online: true,
                    lastLogin: new Date(),
                    lastLoginIP: req.ip
                }, (err, updatedUser) => {
                    if (err) {
                        return res.json({
                            status: 409,
                            err: err
                        })
                    }

                    req.session.me = user.id;
                    res.cookie('token', user.activationToken, {
                        expires: new Date(Number(new Date()) + 10000),
                        httpOnly: false,

                    });
                    req.session.activationToken = updatedUser[0].activationToken;
                    req.session.userType = 'ss';

                    return res.json({
                        status: 200,
                        success: true
                    })
                });
            }
        });
    },

    //TODO March 1st, 2017 - functions below did not tested
    create: (req, res, next) => {

        Serviceseeker.create(req.params.all(), (err, serviceseeker) => {

            if (err) ErrorHandler.errorResponse(err, req, res, next);

            res.json(serviceseeker);
        });
    },

    fetch: (req, res, next) => {

        // Sets the parameters for Sorting and Paging
        let sortBy = req.param('sortBy') + ' ' + req.param('orderBy'),
            recordLimit = req.param('limit'),
            page = req.param('page');

        if (page === undefined) {
            let page = 0;
        } else {
            page = (page - 1) * recordLimit;
        }

        // Sets Searching parameters
        let conditionsApply = req.param('data');

        if (conditionsApply === undefined) {
            conditionsApply = {};
        }

        Serviceseeker.find({
            where: conditionsApply,
            sort: sortBy,
            limit: recordLimit,
            skip: page
        }).exec((err, serviceseekers) => {

            if (err) ErrorHandler.errorResponse(err, req, res, next);

            res.json({
                serviceseekers: serviceseekers
            });
        });
    },

    fetchById: (req, res, next) => {

        Serviceseeker.find({
            id: req.param('id'),
            deleted: req.param('deleted')
        }).exec((err, serviceseeker) => {

            if (err) ErrorHandler.errorResponse(err, req, res, next);

            res.json(serviceseeker);
        });
    },

    destroy: (req, res, next) => {

        Serviceseeker.update({
            id: req.param('id')
        }, {
            deleted: true
        }).exec((err, serviceseeker) => {

            if (err) ErrorHandler.errorResponse(err, req, res, next);

            res.json(serviceseeker);
        });
    },

    dashboard: (req, res, next) => {

        if (!req.session.me) return res.view('common/homepage');

        return res.view('ss/dashboard');
    },

    messages: (req, res, next) => {

        if (!req.session.me) {
            return res.view('common/homepage');
        }
        return res.view('ss/messages');
    },

    register: (req, res, next) => {

        Contractor.findOne({
            email: req.param('email')
        }).exec((err, contractor) => {
            Serviceseeker.findOne({
                    email: req.param('email'),
                    activationToken: req.param('token')
                }
            ).exec((err, serviceseeker) => {
                if (serviceseeker !== undefined && contractor === undefined) {
                    if (serviceseeker.invited === true && serviceseeker.status === "Inactive") {
                        Serviceseeker.update({
                                email: req.param('email'),
                                activationToken: req.param('token')
                            },
                            {
                                invited: /*serviceseeker.provider !== undefined*/ true
                            }).exec((err, user) => {

                            if (!user) {
                                return res.view('common/homepage', {
                                    notFound: "Account not found!"
                                });
                            } else {
                                if (serviceseeker.provider !== undefined) {
                                    res.cookie('authType', false, {
                                        expires: new Date(Number(new Date()) + 10000),
                                        httpOnly: false,
                                    });
                                }
                                res.view('ss/ssregister', {
                                    firstName: user[0].firstName,
                                    lastName: user[0].lastName
                                });
                            }
                        });
                    } else if ((serviceseeker.status === "Active" && req.cookies.authType === undefined) || (serviceseeker.status === "Active" && req.cookies.authType === serviceseeker.provider)) {
                        req.session.me = serviceseeker.id;
                        req.session.activationToken = serviceseeker.activationToken;
                        req.session.userType = 'ss';

                        res.redirect('/')
                    } else if (serviceseeker.status === "Active" && req.cookies.authType !== undefined && serviceseeker.provider === undefined
                        || serviceseeker.status === "Active" && req.cookies.authType !== serviceseeker.provider) {

                        res.cookie('emailError', true, {
                            expires: new Date(Number(new Date()) + 10000),
                            httpOnly: false,
                        });
                        res.redirect('/login');
                    } else {
                        res.redirect('/invitDepr')
                    }
                } else if (serviceseeker === undefined && contractor !== undefined || serviceseeker !== undefined && contractor !== undefined) {
                    res.cookie('emailError', true, {
                        expires: new Date(Number(new Date()) + 10000),
                        httpOnly: false,
                    });
                    res.redirect('/login');
                } else {
                    res.view('404');
                }
            });
        });
    },

    recovery: (req, res, next) => {

        if (!req.param('email')) {
            return res.badRequest();
        }

        let emailRecovery = req.param('email');

        Serviceseeker.find({
            email: req.param('email'),
        }).exec((err, serviceseeker) => {

            if (err) ErrorHandler.errorResponse(err, req, res, next);

            return;
        });

        let token = 'abc123212513';
        let subject = 'recovery from uhome';
        let host = sails.config.hostname;
        let message = '<h1> Click link below for activate your service seeker account </h1><br><br>';
        message += '<a href= "' + host + 'serviceseeker/activation?email=' + emailRecovery + '&token=' + token + '">click here to activate</a>'

        EmailHandler.sendEmail('talib@bitcloudglobal.com', [emailRecovery], subject, message);

        User.update({
            email: req.param('email')
        }, {
            token: token,
            status: 'Active'
        }).exec((err, user) => {

            if (err) ErrorHandler.errorResponse(err, req, res, next);

            res.json(user);
        });
    },

    activation: (req, res, next) => {

        let emailRecovery = req.param('email');
        let token = 'abc123212513';
        let subject = 'account activated';
        let message = 'Your password is changed and service seeker account is activated...';

        User.find({email: emailRecovery}, {token: token}).exec((err, user) => {
            if (err) {
                ErrorHandler.errorResponse(err, req, res, next);
            }
            else {
                User.update({email: req.param('email')}, {encryptedPassword: 'subhanshah'}).exec((err1, user1) => {
                    if (err1) {
                        ErrorHandler.errorResponse(err1, req, res, next);
                    }
                    res.json(user1);
                    EmailHandler.sendEmail('talib@bitcloudglobal.com', [emailRecovery], subject, message);
                });
            }
        });
    },

    hireContractor: (req, res, next) => {
        if (!req.session.me) {
            return res.view('common/homepage');
        }
    },

    addResidenceYear: (req, res, next) => {

        if (req.param('year') == '') {
            res.badRequest({'msg': 'Year founded not found!'});
            return;
        }

        Serviceseeker.update(req.session.me, {residentSince: req.param('year')}, function (err, user) {

            if (err) ErrorHandler.errorResponse(err, req, res, next);

            req.session.User.residentSince = req.param('year');

            res.json({'msg': 'Updated'});
        });
    },

    gallery: (req, res, next) => {

        if (!req.session.me) {
            return res.view('common/homepage');
        }
        return res.view('ss/gallery');
    },

    groups: (req, res, next) => {

        if (!req.session.me) {
            return res.view('common/homepage');
        }

        return res.view('ss/groups');
    }
};
