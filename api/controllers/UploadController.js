'use strict';

(() => {

    module.exports = {
        upload: (req, res) => {

            ImageUploadService.uploadImage(req, res, (result, err) => {

                return res.json(result);
            });

        }
    };

})();
