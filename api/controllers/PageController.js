'use strict';
/**
 * PageController
 *
 * @description :: Server-side logic for managing pages
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

//let parseBusinessDaysAndServices = require()

module.exports = {
  showHomePage: function (req, res, next) {

    if(!req.session.me) {
      return res.view('common/homepage');
    }

    if (req.session.userType == 'ss') {
      Serviceseeker.find(req.session.me, {fields:{activationToken: 0}}, (err, user) => {
          if (err) {
              ErrorHandler.errorResponse(err, req, res, next);
          } else{
              if(!user) {
                  return res.view('common/homepage');
              } else {
                  return res.view('ss/dashboard', {
                      user: user[0]
                  });
              }
          }
      });
    } else if (req.session.userType == 'sp') {
      Contractor.find(req.session.me, {fields:{activationToken: 0}}, (err, user) => {
        if (err) {
          ErrorHandler.errorResponse(err, req, res, next);
        } else{
            if(!user) {
                return res.view('common/homepage');
            } else {
                if ( req.session.businessType == 'Business') {

                    req.session.me = user[0].id;
                    req.session.userType = 'sp';
                    req.session.businessType = user[0].userType.toLowerCase();

                    return res.view('sp/bdashboard', {
                        user: user[0]
                    });

                } else {
                    req.session.me = user[0].id;
                    req.session.userType = 'sp';
                    req.session.businessType = user[0].userType.toLowerCase();

                    return res.view('sp/idashboard', {
                        user: user[0]
                    });
                }
            }
        }
      });
    } else {
      return res.view('common/homepage');
    }
  },

    publicProfile: (req, res, next) => {

      let currentUser = undefined;

      if(!req.session.me) return res.view('common/homepage');

        UserService.getCurrentUserData(req.session, (result, err) => {

            if(err) return res.negotiate(err);

            currentUser = result.currentUser;

            Contractor.find({
                id: req.param('id')

            }, {fields:{activationToken: 0}}, function foundUser(err, user) {

                if (err) return res.negotiate(err);

                if (user.length === 0) {
                    Serviceseeker.find({
                        id: req.param('id')
                    }, {fields:{activationToken: 0}}, function foundUser(err, user) {
                        if (err) return res.negotiate(err);

                        if (!user) return res.notFound();

                        delete user['activationToken'];
                        delete currentUser['activationToken'];
                        return res.view('ss/publicpage', {
                            user: currentUser,
                            viewedUser: user[0]
                        });
                    })
                }
                else {

                    let contractorParsedData = UserService.parseBusinessDaysAndServicesForPublicProfile(user[0]);
                    contractorParsedData.services = JSON.stringify(contractorParsedData.services);
                    return res.view('sp/publicpage', {
                        user: currentUser,
                        viewedUser: contractorParsedData
                    });
                }
            });
        });


  },

    publicProjectPage: (req, res, next) => {

        let currentUser = undefined;

        if(!req.session.me) return res.view('common/homepage');

        UserService.getCurrentUserData(req.session, (result, err) => {

            if(err) return res.negotiate(err);

            currentUser = result.currentUser;

            Projects.findOne({id: req.param('id')}, (err, project) => {
                let prokect = project;
                if(err) return res.negotiate(err);

                Serviceseeker.find({id: project.serviceSeekerId}, {fields:{activationToken: 0}}, (err, serviceseeker) => {

                    if(err) return res.negotiate(err);

                    return res.view('ss/projectReview', {
                        user: currentUser,
                        project : JSON.stringify(project),
                        jobProvider: serviceseeker[0]

                    });

                });
            });
        });
    },

  login: function (req, res) {

    Contractor.findOne({
      email: req.param('email'),
      status: 'Active'
    }, function foundUser(err, user) {
      if (err) return res.negotiate(err);
      if (!user || user.provider!== undefined) {
        Serviceseeker.findOne({
          email: req.param('email'),
          status: 'Active'
        }, function foundUser(err, user) {
          if (err) return res.negotiate(err);
          if (!user || user.provider!== undefined) return res.notFound();

          require('machinepack-passwords').checkPassword({
            passwordAttempt: req.param('password'),
            encryptedPassword: user.encryptedPassword
          }).exec({
            error: function (err) {
              return res.negotiate(err);
            },

            incorrect: function () {
              return res.notFound();
            },

            success: function() {
              req.session.me = user.id;
              req.session.activationToken = user.activationToken;
              req.session.userType = 'ss';
              res.cookie('token', user.activationToken, {
                  expires: new Date(Number(new Date()) + 20000),
                  httpOnly: false,
              });
              res.redirect('/');
            }
          })
        })
      }
      else {
        require('machinepack-passwords').checkPassword({
          passwordAttempt: req.param('password'),
          encryptedPassword: user.encryptedPassword
        }).exec({
          error: function (err) {
            return res.negotiate(err);
          },

          incorrect: function () {
            return res.notFound();
          },

          success: function() {
              req.session.me = user.id;
              req.session.userType = 'sp';

              req.session.activationToken = user.activationToken;
              req.session.businessType = user.userType.toLowerCase();
              res.cookie('token', user.activationToken, {
                  expires: new Date(Number(new Date()) + 20000),
                  httpOnly: false,
              });
              res.redirect('/');
          }
        })
      }

    });
  },

  // Logs out Service Seeker/Contractor
  logout: function (req, res) {

    if(!req.session.me) {
      return res.view('common/homepage');
    }

    // Checks if user is a contractor
    Contractor.findOne(req.session.me, function foundUser(err, user) {

      // If user not found
      if (!user) {

        // Checks if user is a service seeker
        Serviceseeker.findOne(req.session.me, function foundUser(err, user) {

          // If user is not even service seeker we simply removes session and
          // redirects to homepage
          if (!user) {

            req.session.me = null;
            req.session.activationToken = null;
            req.session.userType = null;

            return res.backToHomePage();
          } else {
            // If user is a service seeker we update his online status and logs out
            Serviceseeker.update(req.session.me, {
              online: false
            }, function (err, updatedUser) {
              if(err) {
                res.negotiate(err);
              }
              req.session.me = null;
              req.session.activationToken = null;
              req.session.userType = null;

              return res.backToHomePage();
            });
          }
        });
      } else {
        // If user is a contractor we update his online status and logs out
        Contractor.update(req.session.me, {
          online: false
        }, function (err, updatedUser) {
          if(err) {
            res.negotiate(err);
          }

          req.session.me = null;
          req.session.activationToken = null;
          req.session.userType = null;
          req.session.businessType = null;

          return res.backToHomePage();
        });
      }
    });
  },

  recovery: function (req, res, next) {

    if (!req.param('email') || !req.param('userType')) {
      return res.badRequest();
    }

    var emailRecovery = req.param('email').toLowerCase(),
        userType = req.param('userType');

    if (userType == 'business') {
      Contractor.find({
        email: emailRecovery,
      }).exec(function (err, contractor) {
        if (err) {

          // Handles the errorResponse containing some error
          return ErrorHandler.errorResponse(err, req, res, next);
        }

        if (contractor.length == 0) {
          return res.json({
            msg: 'Email address not found!'
          })
        }

        if (contractor[0].deleted == 'true') {
          return res.ok({msg: "Account has been disabled"});
        }

        if (!contractor[0].emailVerified) {
          var activationTokenKey = UtilityHandler.generateKey();
          EmailService.sendActivationEmail(emailRecovery, activationTokenKey, 'business');
          Contractor.update({
            email: emailRecovery
          }, {
            activationToken: activationTokenKey
          }, function (err, updatedUser) {
            if(err) {
              res.negotiate(err);
            }

            return res.json({
              msg: 'Please check you email'
            });
          });
        } else {
          var activationTokenKey = UtilityHandler.generateKey();
          EmailService.sendRecoveryEmail(emailRecovery, activationTokenKey);
          Contractor.update({
            email: emailRecovery
          }, {
            activationToken: activationTokenKey
          }, function (err, updatedUser) {
            if(err) {
              res.negotiate(err);
            }

            return res.json({
              msg: 'Please check you email'
            });
          });
        }
      });
    } else if (userType == 'user') {
      Serviceseeker.find({
        email: emailRecovery,
      }).exec(function (err, serviceseeker) {
        if (err) {

          // Handles the errorResponse containing some error
          return ErrorHandler.errorResponse(err, req, res, next);
        } else{
            if (!serviceseeker[0]) return res.notFound();

            if (serviceseeker[0].deleted == 'true') {
                return res.ok({msg: "Account has been disabled"});
            }

            if (!serviceseeker[0].emailVerified) {
                var activationTokenKey = UtilityHandler.generateKey();
                EmailService.sendActivationEmail(emailRecovery, activationTokenKey, 'user');
                Serviceseeker.update({
                    email: emailRecovery
                }, {
                    activationToken: activationTokenKey
                }, function (err, updatedUser) {
                    if(err) {
                        res.negotiate(err);
                    }

                    return res.json({
                        msg: 'Please check you email'
                    });
                });

            } else {
                var activationTokenKey = UtilityHandler.generateKey();
                EmailService.sendRecoveryEmail(emailRecovery, activationTokenKey);
                Serviceseeker.update({
                    email: emailRecovery
                }, {
                    activationToken: activationTokenKey
                }, function (err, updatedUser) {
                    if(err) {
                        res.negotiate(err);
                    }

                    return res.json({
                        msg: 'Please check you email'
                    });
                });
            }
        }


      });
    } else {
      return res.badRequest();
    }
  },

  recover: function (req, res, next) {
    if (req.param('email') == undefined || req.param('email') == '' || req.param('token') == undefined || req.param('token') == '') {
      return res.json({
        err: 'Invalid request!',
        status: 409
      });
    } else {
      Serviceseeker.findOne({
        email: req.param('email'),
        activationToken: req.param('token')
      }).exec(function (err, user) {

        if (!user) {
          Contractor.findOne({
            email: req.param('email'),
            activationToken: req.param('token')
          }).exec(function (err, user) {

            if (!user) {
              return res.view('common/homepage', {
                notFound: "Account not found!"
              });
            } else {
              console.log(user.id);
              return res.view('common/recover',{
                user: user.id
              })
            }
          });
        } else {
          Serviceseeker.findOne({
            email: req.param('email'),
            activationToken: req.param('token')
          }).exec(function (err, user) {

            if (!user) {
              return res.view('common/homepage', {
                notFound: "Account not found!"
              });
            } else {
              return res.view('common/recover',{
                user: user.id
              })
            }
          });
        }
      });
    }
  },

  resetpassword: function(req, res, next) {

    let userType = undefined;

    function saveNewPassword(userType, userId, data){
      if(userType ===  'serviceseeker'){
          Serviceseeker.update({
              id: userId
          }, data, function userCreated(err, newUser) {
              return res.json({
                  msg: 'Password changed successfully!'
              });
          });
      } else{
          Contractor.update({
              id: userId
          }, data, function userCreated(err, newUser) {
              return res.json({
                  msg: 'Password changed successfully!'
              });
          });
      }

    }

    if (req.param('userId') == "" ||
      req.param('password') == "" ||
      req.param('confirmPassword') == "")
      return res.badRequest({
        msg: 'Missing required field(s)'
      });

    var userId = req.param('userId'),
        password = req.param('password'),
        confirmPassword = req.param('confirmPassword');

    Serviceseeker.findOne({
      id: userId
    }).exec(function (err, serviceseeker) {
      /*if (!user) {*/
        Contractor.findOne({
          id: userId
        }).exec(function (err, contrator) {
          if (!serviceseeker && !contrator) {
            return res.json({
              msg: 'User not found!',
              status: 409
            })
          } else {
            userType = contrator === undefined ? 'serviceseeker' : 'contrator';
            require('machinepack-passwords').encryptPassword({
              password: password,
              difficulty: 10
            }).exec({
              // An unexpected error occurred.
              error: function (err) {
                return res.json({
                  msg: err,
                  status: 409
                })
              },
              // OK.
              success: function (encryptedPassword) {
                var data = {
                  encryptedPassword: encryptedPassword
                };


                saveNewPassword(userType, userId, data);
                /*Contractor.update({
                  id: userId
                }, data, function userCreated(err, newUser) {
                  return res.json({
                    msg: 'Password changed successfully!'
                  });
                });*/
              }
            });
          }
        });
      /*}*/ /*else {
        Serviceseeker.findOne({
          id: userId
        }).exec(function (err, user) {

          if (!user) {
            return res.json({
              msg: "Account not found!",
              status: 409
            });
          } else {
            Serviceseeker.findOne({
              id: userId
            }).exec(function (err, user) {

              if (!user) {
                return res.json({
                  msg: "Account not found!",
                  status: 409
                });
              } else {
                require('machinepack-passwords').encryptPassword({
                  password: password,
                  difficulty: 10
                }).exec({
                  // An unexpected error occurred.
                  error: function (err) {
                    return res.json({
                      msg: err,
                      status: 409
                    })
                  },
                  // OK.
                  success: function (encryptedPassword) {
                    var data = {
                      encryptedPassword: encryptedPassword
                    };

                    Serviceseeker.update({
                      id: userId
                    }, data, function userCreated(err, newUser) {
                      return res.json({
                        msg: 'Password changed successfully!'
                      });
                    });
                  }
                });
              }
            });
          }
        });
      }*/
    });
  },

  forgot: function(req, res, next) {
    Serviceseeker.findOne({
      email: req.param('email'),
      status: 'Active'
    }).exec(function (err, user) {

      if (!user) {
        Contractor.findOne({
          email: req.param('email'),
          status: 'Active'
        }).exec(function (err, user) {
          var activationTokenKey = UtilityHandler.generateKey();

          Contractor.update({activationToken: activationTokenKey}, user.id).exec(function(err, updated){
            if (err) {
              return;
            }
            EmailService.sendForgotPasswordEmail(user.email, activationTokenKey);
          });
        });
      } else {
      }
    });
  },

  forgotCallback: function(req, res, next) {

    if (req.param('email') == undefined || req.param('email') == '' || req.param('token') == undefined || req.param('token') == '') {
      return res.json({
        err: 'Invalid request!',
        status: 409
      });
    } else {
      Serviceseeker.findOne({
        email: req.param('email'),
        token: req.param('token'),
        status: 'Active'
      }).exec(function (err, user) {

        if (!user) {
          Contractor.findOne({
            email: req.param('email'),
            token: req.param('token'),
            status: 'Active'
          }).exec(function (err, user) {
            return res.view('common/recover',{
              user: user.id
            })
          });
        } else {
        }
      });
    }
  },
};

