'use strict';
/**
 * SignupController
 *
 * @description :: Server-side logic for managing signups
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

  signup: (req, res) => {

    Contractor.findOne({
      email: req.param('email')
    }).exec((err, user) => {
      Serviceseeker.findOne({
        email: req.param('email')
      }).exec((err, user2) => {
        if (user || user2) {
          res.emailAddressInUse();
        } else {
          require('machinepack-passwords').encryptPassword({
            password: req.param('password'),
            difficulty: 10
          }).exec({
            error: (err) => {
              return res.negotiate(err);
            },
            success: (encryptedPassword) => {
              let email = req.param('email').toLowerCase(),
                  activationTokenKey = UtilityHandler.generateKey();

              if (req.param('userType') == 'user') {

                let data = {
                  firstName: " ",
                  lastName: " ",
                  email: email,
                  encryptedPassword: encryptedPassword,
                  lastLoggedIn: new Date(),
                  activationToken:  activationTokenKey
                };

                Serviceseeker.create(data, function userCreated(err, newUser) {
                  if (err) {

                    if (
                      err.invalidAttributes &&
                      err.invalidAttributes.email &&
                      err.invalidAttributes.email[0] &&
                      err.invalidAttributes.email[0].rule === 'unique'
                    ) {
                      return res.emailAddressInUse();
                    }
                    return res.negotiate(err);
                  }

                    EmailService.sendActivationEmail(email, activationTokenKey, 'user');

                  req.session.me = newUser.id;

                  return res.json({
                    id: newUser.id
                  });
                });
              }
              else if (req.param('userType') == 'business') {
                Contractor.create({
                  firstName: " ",
                  lastName: " ",
                  email: email,
                  encryptedPassword: encryptedPassword,
                  lastLoggedIn: new Date(),
                  activationToken:  activationTokenKey
                }, function userCreated(err, newUser) {
                  if (err) {

                    if (
                      err.invalidAttributes &&
                      err.invalidAttributes.email &&
                      err.invalidAttributes.email[0] &&
                      err.invalidAttributes.email[0].rule === 'unique'
                    ) {
                      return res.emailAddressInUse();
                    }
                    return res.negotiate(err);
                  }

                    EmailService.sendActivationEmail(email, activationTokenKey, 'business');

                  req.session.me = newUser.id;

                  return res.json({
                    id: newUser.id
                  });
                });
              }
              else {
                return res.badRequest();
              }
            }
          });
        }
      });
    });
  }
};
