'use strict';
/**
 * ContractorController
 *
 * Date      :: June 28th, 2016
 * @description :: Server-side logic for managing contractors
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
let Promise = require('q');

module.exports = {

    register: (req, res, next) => {
        Serviceseeker.findOne({
                email: req.param('email')
            }
        ).exec((err, serviceseeker) => {
            Contractor.findOne({
                email: req.param('email'),
                activationToken: req.param('token')
            }).exec((err, contractor) => {

                if (contractor !== undefined && serviceseeker === undefined) {
                    if (contractor.invited === true) {
                        Contractor.update({
                                email: req.param('email'),
                                activationToken: req.param('token')
                            },
                            {
                                invited: /*contractor.provider !== undefined*/ true
                            }).exec((err, user) => {

                            if (!user) {
                                return res.view('common/homepage', {
                                    notFound: "Account not found!"
                                });
                            } else {
                                if (contractor.provider !== undefined) {
                                    res.cookie('authType', false, {
                                        expires: new Date(Number(new Date()) + 10000),
                                        httpOnly: false,
                                    });
                                }
                                res.view('sp/registration');
                            }
                        });
                    } else if ((contractor.status === "Active" && req.cookies.authType === undefined) || (contractor.status === "Active" && req.cookies.authType === contractor.provider)) {
                        req.session.me = contractor.id;
                        req.session.activationToken = contractor.activationToken;
                        req.session.userType = 'ss';

                        res.redirect('/')
                    } else if (contractor.status === "Active" && req.cookies.authType !== undefined && contractor.provider === undefined
                        || contractor.status === "Active" && req.cookies.authType !== contractor.provider) {

                        res.cookie('emailError', true, {
                            expires: new Date(Number(new Date()) + 10000),
                            httpOnly: false,
                        });

                        res.redirect('/login');
                    } else {
                        res.redirect('/invitDepr')
                    }
                } else if (contractor === undefined && serviceseeker !== undefined || contractor !== undefined && serviceseeker !== undefined) {
                    res.redirect('/login');

                } else {
                    res.view('404');
                }
            });
        });

    },

    activateAccount: (req, res, next) => {
        getContractorByEmailAndToken(req, res)
            .then((user) => {
                if (req.param('userType') == 'Business') {
                    updateContractorBuisness(req, res);
                } else if (req.param('userType') == 'Individual') {
                    updateContractorIndividual(req, res);
                }
            }).catch((error) => {
            return res.view('common/homepage', {
                notFound: "Account not found!"
            });
        });
    },

    editProfilePage: (req, res, next) => {

        if (!req.session.me) return res.redirect('/');

        Contractor.find({
            activationToken: req.session.activationToken
        }, {fields:{activationToken: 0}}).exec((err, contractor) => {

            if (err) {
                ErrorHandler.errorResponse(err, req, res, next);
            } else{
                Languages.find().exec((err, languages) => {
                    if (err) {
                        ErrorHandler.errorResponse(err, req, res, next);
                    } else{
                        Worklines.find().exec((err, worklines) => {
                            if (err) {
                                ErrorHandler.errorResponse(err, req, res, next);
                            } else{
                                let contractorParsedData = UserService.parseBusinessDaysAndServices(contractor[0]);

                                return res.view("sp/profile", {
                                    user: contractorParsedData,
                                    languages: languages,
                                    worklines: worklines
                                });
                            }
                        });
                    }
                });
            }



        });
    },

    getLanguagesWorklinesAccreditationTypes: (req, res, next) => {
        let resultData = {};

        Contractor.findOne({
            activationToken: req.session.activationToken
        }).exec((err, contractor) => {

            if (err) {
                ErrorHandler.errorResponse(err, req, res, next);
            } else{
                if(contractor === undefined){
                    next();
                } else {
                    resultData['contractorAccreditation'] = contractor.accreditation;
                    resultData['contractorLanguage'] = contractor.language;
                    resultData['contractorWorkLine'] = contractor.workLine;

                    Languages.find().exec((err, languages) => {
                        if (err) {
                            ErrorHandler.errorResponse(err, req, res, next);
                        } else{
                            resultData['languages'] = languages;

                            Worklines.find().exec((err, worklines) => {
                                if (err) {
                                    ErrorHandler.errorResponse(err, req, res, next);
                                } else{
                                    resultData['worklines'] = worklines;

                                    Accreditations.find().exec((err, accreditation) => {
                                        if (err) {
                                            ErrorHandler.errorResponse(err, req, res, next);
                                        } else{
                                            resultData['accreditation'] = accreditation;

                                            return res.json({resultData: resultData});
                                        }

                                    });
                                }


                            });
                        }


                    });
                }
            }

        });

    },

    updateProfile: (req, res, next) => {

        let params = req.body,
            updatedParams = {},
            flags = 'i',
            regExp = new RegExp(req.body.workLine, flags);

        updatedParams.businessDays = {
            "sunday": {},
            "monday": {},
            "tuesday": {},
            "wednesday": {},
            "thursday": {},
            "friday": {},
            "saturday": {}
        };

        for (let prop in params) {

            if ((prop === 'sundayStart' || prop === 'sundayEnd')) {
                if (params[prop] != 'Close' && params[prop] != '' && params[prop] !== '24hr') {
                    if (req.body.is24Hours === true) {
                        updatedParams.businessDays.sunday = '24hr';
                    } else {
                        if (prop === 'sundayStart') updatedParams.businessDays.sunday.start = params[prop];
                        if (prop === 'sundayEnd') updatedParams.businessDays.sunday.end = params[prop];
                    }
                } else if (params[prop] === '24hr') {
                    updatedParams.businessDays.sunday = '24hr';
                } else {
                    updatedParams.businessDays.sunday = 'Close';
                }
            }
            else if ((prop === 'mondayStart' || prop === 'mondayEnd')) {
                if (params[prop] !== 'Close' && params[prop] !== '' && params[prop] !== '24hr') {
                    if (req.body.is24Hours === true) {
                        updatedParams.businessDays.monday = '24hr';
                    } else {
                        if (prop === 'mondayStart') updatedParams.businessDays.monday.start = params[prop];
                        if (prop === 'mondayEnd') updatedParams.businessDays.monday.end = params[prop];
                    }
                } else if (params[prop] === '24hr') {
                    updatedParams.businessDays.monday = '24hr';
                } else {
                    updatedParams.businessDays.monday = 'Close';
                }

            }

            else if ((prop === 'tuesdayStart' || prop === 'tuesdayEnd')) {
                if (params[prop] !== 'Close' && params[prop] !== '' && params[prop] !== '24hr') {
                    if (req.body.is24Hours === true) {
                        updatedParams.businessDays.tuesday = '24hr';
                    } else {
                        if (prop === 'tuesdayStart') updatedParams.businessDays.tuesday.start = params[prop];
                        if (prop === 'tuesdayEnd') updatedParams.businessDays.tuesday.end = params[prop];
                    }
                } else if (params[prop] === '24hr') {
                    updatedParams.businessDays.tuesday = '24hr';
                } else {
                    updatedParams.businessDays.tuesday = 'Close';
                }

            }

            else if ((prop === 'wednesdayStart' || prop === 'wednesdayEnd')) {
                if (params[prop] !== 'Close' && params[prop] !== '' && params[prop] !== '24hr') {
                    if (req.body.is24Hours === true) {
                        updatedParams.businessDays.wednesday = '24hr';
                    } else {
                        if (prop === 'wednesdayStart') updatedParams.businessDays.wednesday.start = params[prop];
                        if (prop === 'wednesdayEnd') updatedParams.businessDays.wednesday.end = params[prop];
                    }
                } else if (params[prop] === '24hr') {
                    updatedParams.businessDays.wednesday = '24hr';
                } else {
                    updatedParams.businessDays.wednesday = 'Close';
                }

            }

            else if ((prop === 'thursdayStart' || prop === 'thursdayEnd')) {
                if (params[prop] !== 'Close' && params[prop] !== '' && params[prop] !== '24hr') {
                    if (req.body.is24Hours === true) {
                        updatedParams.businessDays.thursday = '24hr';
                    } else {
                        if (prop === 'thursdayStart') updatedParams.businessDays.thursday.start = params[prop];
                        if (prop === 'thursdayEnd') updatedParams.businessDays.thursday.end = params[prop];
                    }
                } else if (params[prop] === '24hr') {
                    updatedParams.businessDays.thursday = '24hr';
                } else {
                    updatedParams.businessDays.thursday = 'Close';
                }
            }

            else if ((prop === 'fridayStart' || prop === 'fridayEnd')) {
                if (params[prop] !== 'Close' && params[prop] !== '' && params[prop] !== '24hr') {
                    if (req.body.is24Hours === true) {
                        updatedParams.businessDays.friday = '24hr';
                    } else {
                        if (prop === 'fridayStart') updatedParams.businessDays.friday.start = params[prop];
                        if (prop === 'fridayEnd') updatedParams.businessDays.friday.end = params[prop];
                    }
                } else if (params[prop] === '24hr') {
                    updatedParams.businessDays.friday = '24hr';
                } else {
                    updatedParams.businessDays.friday = 'Close';
                }
            }

            else if ((prop === 'saturdayStart' || prop === 'saturdayEnd')) {
                if (params[prop] !== 'Close' && params[prop] !== '' && params[prop] !== '24hr') {
                    if (req.body.is24Hours === true) {
                        updatedParams.businessDays.saturday = '24hr';
                    } else {
                        if (prop === 'saturdayStart') updatedParams.businessDays.saturday.start = params[prop];
                        if (prop === 'saturdayEnd') updatedParams.businessDays.saturday.end = params[prop];
                    }
                } else if (params[prop] === '24hr') {
                    updatedParams.businessDays.saturday = '24hr';
                } else {
                    updatedParams.businessDays.saturday = 'Close';
                }

            }

            else if ((prop === 'workLine' || prop === 'accreditation' || prop === 'language')) {
                if (params[prop] === "") {
                    updatedParams[prop] = null;
                } else {
                    updatedParams[prop] = params[prop];
                }
            }
            else {
                updatedParams[prop] = params[prop];
            }
        }

        Worklines.find({"workLines": regExp.source}, (err, workline) => {

            if (err) {
                ErrorHandler.errorResponse(err, req, res, next);
            } else{
                if (workline.length === 0 && updatedParams.workLine !== null) {
                    Worklines.create({workLines: req.body.workLine}, (err) => {
                        if (err) ErrorHandler.errorResponse(err, req, res, next);
                    });
                }

                Contractor.update({'activationToken': req.session.activationToken}, updatedParams, (err, contractor) => {

                    if (err) {
                        ErrorHandler.errorResponse(err, req, res, next);
                    } else{
                        Contractor.native(function(err,coll){
                            coll.distinct("services", function(err,subServices){
                                if (err) {
                                    ErrorHandler.errorResponse(err, req, res, next);
                                } else{
                                    UserService.checkIsSubServicesExistInTable(subServices, (result, err) => {
                                        Subservices.create(result, (err) => {

                                            if (err) {
                                                ErrorHandler.errorResponse(err, req, res, next);
                                            } else{
                                                let contractorParsedData = UserService.parseBusinessDaysAndServices(contractor[0]);

                                                res.json(contractorParsedData);
                                            }
                                        });
                                    });
                                }



                            });
                        });
                    }
                });
            }



        });


    },

/*    recovery: (req, res, next) => {
        let emailRecovery = req.param('email'),
            host = sails.config.hostname,
            token = 'abc123212513',
            subject = 'recovery from uhome',
            message = '<h1> Click link below for activate your contractor account </h1><br><br>';
        message += '<a href= "' + host + 'Contractor/activation?email=' + emailRecovery + '&token=' + token + '">click here to activate</a>';

        EmailHandler.sendEmail('talib@bitcloudglobal.com', [emailRecovery], subject, message);

        User.update({
            email: req.param('email')
        }, {
            token: token,
            status: 'Active'
        }).exec((err, user) => {
            if (err) ErrorHandler.errorResponse(err, req, res, next);

            res.json(user);
        });
    },

    activation: (req, res, next) => {

        let emailRecovery = req.param('email'),
            token = 'abc123212513',
            subject = 'account activated',
            message = 'Your password is changed and contractor account is activated...';

        User.find({email: emailRecovery}, {token: token}).exec((err, user) => {
            if (err) {
                ErrorHandler.errorResponse(err, req, res, next);
            }
            else {
                User.update({email: req.param('email')}, {encryptedPassword: 'subhanshah'}).exec((err1, user1) => {
                    if (err1) {
                        ErrorHandler.errorResponse(err1, req, res, next);
                    }
                    res.json(user1);

                    EmailHandler.sendEmail('talib@bitcloudglobal.com', [emailRecovery], subject, message);
                });
            }
        });
    },*/

    //TODO March 1st, 2017 - functions below did not tested
    create: (req, res, next) => {
        Contractor.create(req.params.all(), (err, serviceseeker) => {

            if (err) ErrorHandler.errorResponse(err, req, res, next);

            res.json(serviceseeker);
        });
    },

    fetch: (req, res, next) => {

        let sortBy = req.param('sortBy') + ' ' + req.param('orderBy'),
            recordLimit = req.param('limit'),
            page = req.param('page');

        if (page === undefined) {
            let page = 0;
        } else {
            page = (page - 1) * recordLimit;
        }
        // Sets Searching parameters
        let conditionsApply = req.param('data');

        if (conditionsApply === undefined) conditionsApply = {};

        Contractor.find({
            where: conditionsApply,
            sort: sortBy,
            limit: recordLimit,
            skip: page
        }).exec((err, serviceseekers) => {

            if (err) ErrorHandler.errorResponse(err, req, res, next);

            res.json({
                serviceseekers: serviceseekers
            });
        });
    },

    fetchById: (req, res, next) => {

        Contractor.find({
            id: req.param('id'),
            deleted: req.param('deleted')
        }).exec(function (err, serviceseeker) {

            if (err) ErrorHandler.errorResponse(err, req, res, next);

            res.json(serviceseeker);
        });
    },

    destroy: (req, res, next) => {

        Contractor.update({
            id: req.param('id')
        }, {
            deleted: true
        }).exec((err, serviceseeker) => {

            if (err) ErrorHandler.errorResponse(err, req, res, next);

            res.json(serviceseeker);
        });
    },

    getTeamMember: (req, res, next) => {
        if (!req.session.me) {
            return res.redirect('/');
        }
        Contractor.findOne(req.session.me).exec((err, updatedUser) => {
            if (err) {
                return res.json('/', {
                    msg: 'Unexpected error occured!'
                });
            } else {
                return res.json({
                    teamMembers: updatedUser.teamMembers
                });
            }
        });
    },

    deleteTeam: (req, res, next) => {
        if (!req.session.me) {
            return res.redirect('/');
        }
        Contractor.findOne(req.session.me).exec((err, updatedUser) => {
            if (err) {
                return res.json({
                    msg: 'Unexpected error occured!'
                });
            } else {
                Contractor.findOne({
                    "$and": [
                        {"teamMembers.firstName": req.param('firstName')},
                        {"teamMembers.lastName": req.param('lastName')}
                    ]
                }, (err, data) => {
                    return res.ok();
                });
            }
        });
    },

    findContractor: (req, res, next) => {
        if (!req.session.me) {
            return res.redirect('/');
        }
        return res.view("sp/find-contractor");
    },

    spprojects: (req, res, next) => {

        if (!req.session.me) return res.redirect('/');

        return res.view("sp/spprojects");
    },

    spmessages: (req, res, next) => {

        if (!req.session.me) return res.redirect('/');

        Contractor.findOne({
            activationToken: req.session.activationToken
        }).exec((err, contractor) => {

            if (err) ErrorHandler.errorResponse(err, req, res, next);

            return res.view("sp/spmessages", {user: contractor});
        });
    },

    spads: (req, res, next) => {

        if (!req.session.me) return res.redirect('/');

        return res.view("sp/spads");
    },

    spverifications: (req, res, next) => {

        if (!req.session.me) return res.redirect('/');

        return res.view("sp/spverifications");
    },

    spbadges: (req, res, next) => {

        if (!req.session.me) return res.redirect('/');

        return res.view("sp/spbadges");
    },

    sprecommendations: (req, res, next) => {

        if (!req.session.me) return res.redirect('/');

        return res.view("sp/sprecommendations");
    },

    ourteam: (req, res, next) => {

        if (!req.session.me) return res.view('common/homepage');

        Contractor.findOne(req.session.me).exec((err, updatedUser) => {
            if (err) {
                return res.json('sp/ourteam', {
                    msg: 'Unexpected error occured!'
                });
            } else {
                return res.view('sp/ourteam', {
                    teamMembers: updatedUser.teamMembers
                });
            }
        });
    },

    reviews: (req, res, next) => {

        if (!req.session.me) return res.view('common/homepage');

        return res.view('sp/reviews');
    },

    ourprojects: (req, res, next) => {

        if (!req.session.me) return res.view('common/homepage');

        return res.view('sp/ourprojects');
    },

    addBBBUrl: (req, res, next) => {

        if (req.param('url') == '') {
            res.badRequest({'msg': 'BBB Url not found!'});
            return;
        }

        Contractor.update(req.session.me, {bbbLink: req.param('url')}, (err, contractor) => {
            if (err) ErrorHandler.errorResponse(err, req, res, next);

            res.json({'msg': 'Updated'});
        });
    },

    addYearFounded: (req, res, next) => {

        if (req.param('year') == '') {
            res.badRequest({'msg': 'Year founded not found!'});
            return;
        }

        Contractor.update(req.session.me, {yearFounded: req.param('year')}, (err, contractor) => {
            if (err) ErrorHandler.errorResponse(err, req, res, next);

            res.json({'msg': 'Updated'});
        });
    },

    addLanguages: (req, res, next) => {

        if (req.param('languages').length < 1) {
            res.badRequest({'msg': 'Please select atleast one language!'});
            return;
        }

        Contractor.update(req.session.me, {languages: req.param('languages')}, (err, contractor) => {
            if (err) ErrorHandler.errorResponse(err, req, res, next);

            res.json({'msg': 'Updated'});
        });
    },

    addLicense: (req, res, next) => {

        if (req.param('states') == '') {
            res.badRequest({'msg': 'State not selected!'});
            return;
        }

        if (req.param('accreditation') == '') {
            res.badRequest({'msg': 'Accreditation not selected!'});
            return;
        }

        if (req.param('accreditation_number') == '') {
            res.badRequest({'msg': 'Please enter accreditation number!'});
            return;
        }

        if (req.param('expiration') == '') {
            res.badRequest({'msg': 'Please enter expiration date!'});
            return;
        }

        Contractor.findOne(req.session.me).exec((err, contractor) => {
            let licenseNumber = [];
            if (!contractor.licenseNumber) {
                licenseNumber = [
                    {
                        states: req.param('states'),
                        accreditation: req.param('accreditation'),
                        accreditation_number: req.param('accreditation_number'),
                        expiration: req.param('expiration')
                    }
                ];
            } else {
                licenseNumber = contractor.licenseNumber;
                licenseNumber.push(
                    {
                        states: req.param('states'),
                        accreditation: req.param('accreditation'),
                        accreditation_number: req.param('accreditation_number'),
                        expiration: req.param('expiration')
                    }
                );
            }

            Contractor.update(req.session.me, {licenseNumber: licenseNumber}, (err, contractor) => {

                if (err) ErrorHandler.errorResponse(err, req, res, next);

                return res.json({'msg': 'Updated'});
            });
        });
    },

    addServicesOffered: (req, res, next) => {

        if (req.param('services').length < 1) {
            res.badRequest({'msg': 'Please select atleast one skill!'});
            return;
        }

        Contractor.update(req.session.me, {skills: req.param('services')}, (err, contractor) => {

            if (err) ErrorHandler.errorResponse(err, req, res, next);

            SubService.find({id: {"$in": req.param('services')}}).populate('serviceId').exec((err, subservice) => {

                if (err) ErrorHandler.errorResponse(err, req, res, next);

                res.json({'msg': 'Updated'});
            });
        });
    },

    addTeamMember: (req, res, next) => {
        if (!req.session.me) {
            return res.forbidden({
                err: 'Please login to process'
            });
        }

        if (req.session.User.userType == 'Individual') {
            return res.badRequest({
                err: "Team members can't be added in individual's profile"
            })
        } else {

            if (
                req.param('firstName') == undefined || req.param('firstName') == '' ||
                req.param('lastName') == undefined || req.param('lastName') == '' ||
                req.param('designation') == undefined || req.param('designation') == '' ||
                req.param('userPhoto') == undefined || req.param('userPhoto') == ''
            ) {
                return res.badRequest({
                    err: 'Missing required fields'
                })
            } else {
                Contractor.findOne(req.session.me).exec((err, user) => {
                    let data = {
                        firstName: req.param('firstName'),
                        lastName: req.param('lastName'),
                        designation: req.param('designation'),
                        userPhoto: req.param('userPhoto')
                    };

                    if (user.teamMembers == undefined || user.teamMembers.length == 0) {
                        Contractor.update(req.session.me, {teamMembers: [data]}).exec((err, updatedUser) => {
                            if (err) {
                                return res.serverError({
                                    msg: 'Unable to add team member!'
                                });
                            } else {
                                return res.json({
                                    teamMembers: updatedUser.teamMembers,
                                    msg: 'Team member added successfully'
                                });
                            }
                        });
                    } else {
                        let teamMembers = user.teamMembers;
                        teamMembers.push(data);
                        Contractor.update(req.session.me, {teamMembers: teamMembers}).exec((err, updatedUser) => {
                            if (err) {
                                return res.serverError({
                                    msg: 'Unable to add team member!'
                                });
                            } else {
                                return res.json({
                                    teamMembers: updatedUser.teamMembers,
                                    msg: 'Team member added successfully'
                                });
                            }
                        });
                    }
                });
            }
        }
    },

    editTeam: (req, res, next) => {

        if (!req.session.me) return res.redirect('/');

        Contractor.findOne(req.session.me).exec((err, updatedUser) => {
            if (err) {
                return res.redirect('/', {
                    msg: 'Unexpected error occured!'
                });
            } else {
                return res.view('sp/editTeam', {
                    teamMembers: updatedUser.teamMembers
                });
            }
        });
    },

    myjobs: (req, res, next) => {

        if (!req.session.me) return res.redirect('/');

        Contractor.find({
            activationToken: req.session.activationToken
        }, {fields:{activationToken: 0}}).exec((err, contractor) => {
            if (err) ErrorHandler.errorResponse(err, req, res, next);

            return res.view("sp/myjobs", {user: contractor[0]});
        });

    },

    spreviews: (req, res, next) => {

        if (!req.session.me) return res.redirect('/');

        Contractor.find({
            activationToken: req.session.activationToken
        }, {fields:{activationToken: 0}}).exec((err, contractor) => {

            if (err) ErrorHandler.errorResponse(err, req, res, next);

            return res.view("sp/spreviews", {user: contractor[0]});
        });
    }



};


function getContractorByEmailAndToken(req, res) {
    let deffered = Promise.defer();

    if (req.param('email') == '' || req.param('email') == undefined ||
        req.param('token') == '' || req.param('token') == undefined) {
        deffered.reject({
            status: 409,
            msg: 'Invalid request'
        });
    } else {
        Contractor.findOne({
            email: req.param('email'),
            activationToken: req.param('token')
        }).then((user) => {
            deffered.resolve(user);
        }).fail((err) => {
            deffered.reject(err);
        });
    }
    return deffered.promise;
}

function updateContractorBuisness(req, res) {
    let businessDays = {};
    let businessHours = 0;

    Contractor.findOne({
        activationToken: req.param('token')
    }).exec((err, user) => {

        let profilePhoto = user.profilePicture,
            coverBanner = user.bannerPicture,
            logoPhoto = user.logoPhoto;

        if (!user) {
            return res.json({
                status: 409,
                notFound: "Account not found!"
            });
        } else {

            if (req.param('companySunStart') != 'Close') {
                if (req.param('companyAlwaysOpen') === true) {
                    businessDays.sunday = 'Open24h';
                    businessHours++;
                } else {
                    businessDays.sunday = {
                        start: req.param('companySunStart'),
                        end: req.param('companySunEnd')
                    };
                    businessHours++;
                }
            } else {
                businessDays.sunday = 'Close';
            }

            if (req.param('companyMonStart') != 'Close') {
                if (req.param('companyAlwaysOpen') === true) {
                    businessDays.monday = 'Open24h';
                    businessHours++;
                } else {
                    businessDays.monday = {
                        start: req.param('companyMonStart'),
                        end: req.param('companyMonEnd')
                    };
                    businessHours++;
                }

            } else {
                businessDays.monday = 'Close';
            }

            if (req.param('companyTueStart') != 'Close') {
                if (req.param('companyAlwaysOpen') === true) {
                    businessDays.tuesday = 'Open24h';
                    businessHours++;
                } else {
                    businessDays.tuesday = {
                        start: req.param('companyTueStart'),
                        end: req.param('companyTueEnd')
                    };
                    businessHours++;
                }
            } else {
                businessDays.tuesday = 'Close';
            }

            if (req.param('companyWedStart') != 'Close') {
                if (req.param('companyAlwaysOpen') === true) {
                    businessDays.wednesday = 'Open24h';
                    businessHours++;
                } else {
                    businessDays.wednesday = {
                        start: req.param('companyWedStart'),
                        end: req.param('companyWedEnd')
                    };
                    businessHours++;
                }

            } else {
                businessDays.wednesday = 'Close';
            }

            if (req.param('companyThuStart') != 'Close') {
                if (req.param('companyAlwaysOpen') === true) {
                    businessDays.thursday = 'Open24h';
                    businessHours++;
                } else {
                    businessDays.thursday = {
                        start: req.param('companyThuStart'),
                        end: req.param('companyThuEnd')
                    };
                    businessHours++;
                }
            } else {
                businessDays.thursday = 'Close';
            }

            if (req.param('companyFriStart') != 'Close') {
                if (req.param('companyAlwaysOpen') === true) {
                    businessDays.friday = 'Open24h';
                    businessHours++;
                } else {
                    businessDays.friday = {
                        start: req.param('companyFriStart'),
                        end: req.param('companyFriEnd')
                    };
                    businessHours++;
                }
            } else {
                businessDays.friday = 'Close';
            }

            if (req.param('companySatStart') != 'Close') {
                if (req.param('companyAlwaysOpen') === true) {
                    businessDays.saturday = 'Open24h';
                    businessHours++;
                } else {
                    businessDays.saturday = {
                        start: req.param('companySatStart'),
                        end: req.param('companySatEnd')
                    };
                    businessHours++;
                }
            } else {
                businessDays.saturday = 'Close';
            }


            if (profilePhoto === undefined) profilePhoto = 'https://s3-us-west-2.amazonaws.com/uhome/avatar.png';
            if (logoPhoto === undefined) logoPhoto = 'https://s3-us-west-2.amazonaws.com/uhome/logo.png';
            if (coverBanner === undefined) coverBanner = 'https://s3-us-west-2.amazonaws.com/uhome/timeline.jpg';


            Contractor.update({
                activationToken: req.param('token')
            }, {
                firstName: req.param('companyFirstName'),
                lastName: req.param('companyLastName'),
                phone: req.param('companyBusinessPhone'),
                address: req.param('companyStreetAddress'),
                unit: req.param('companyUnit'),
                city: req.param('companyCity'),
                state: req.param('companyState'),
                longitute: req.param('companyLng'),
                latitude: req.param('companyLat'),
                country: 'US',
                zipcode: req.param('companyZipcode'),
                businessDays: businessDays,
                businessHours: businessHours,
                hourlyRate: req.param('companyHourlyRate'),
                is24Hours: req.param('companyAlwaysOpen'),
                url: req.param('companyWebsiteURL'),
                userType: req.param('userType'),
                businessName: req.param('companyBusinessName'),
                businessEIN: req.param('companyBusinessEIN'),
                aboutUs: req.param('aboutUs'),
                profilePicture: profilePhoto,
                logoPhoto: logoPhoto,
                bannerPicture: coverBanner,
                emailVerified: true,
                status: 'Active',
                online: true,
                lastLogin: new Date(),
                lastLoginIP: req.ip
            }).exec((err, user) => {

                if (err) return res.json({
                    err: err,
                    msg: 'Profile update failed'
                });

                req.session.me = user[0].id;
                req.session.userType = 'sp';
                res.cookie('token', user[0].activationToken, {
                    expires: new Date(Number(new Date()) + 10000),
                    httpOnly: false,

                });
                req.session.businessType = 'business';

                return res.json({
                    status: 200,
                    success: true
                })
            });

        }});


}

function updateContractorIndividual(req, res) {
    let businessDays = {},
        businessHours = 0,
        profilePhoto = undefined,
        coverBanner = undefined;

    Contractor.findOne({
        activationToken: req.param('token')
    }).exec((err, user) => {

        let profilePhoto = user.profilePicture,
            coverBanner = user.bannerPicture;

        if (!user) {
            return res.json({
                status: 409,
                notFound: "Account not found!"
            });
        } else {
            if (req.param('userSunStart') != 'Close') {
                if (req.param('userAlwaysOpen') === true) {
                    businessDays.sunday = 'Open24h';
                    businessHours++;
                } else {
                    businessDays.sunday = {
                        start: req.param('userSunStart'),
                        end: req.param('userySunEnd')
                    };
                    businessHours++;
                }
            } else {
                businessDays.sunday = 'Close';
            }

            if (req.param('userMonStart') != 'Close') {
                if (req.param('userAlwaysOpen') === true) {
                    businessDays.monday = 'Open24h';
                    businessHours++;
                } else {
                    businessDays.monday = {
                        start: req.param('userMonStart'),
                        end: req.param('userMonEnd')
                    };
                    businessHours++;
                }

            } else {
                businessDays.monday = 'Close';
            }

            if (req.param('userTueStart') != 'Close') {
                if (req.param('userAlwaysOpen') === true) {
                    businessDays.tuesday = 'Open24h';
                    businessHours++;
                } else {
                    businessDays.tuesday = {
                        start: req.param('userTueStart'),
                        end: req.param('userTueEnd')
                    };
                    businessHours++;
                }
            } else {
                businessDays.tuesday = 'Close';
            }

            if (req.param('useryWedStart') != 'Close') {
                if (req.param('userAlwaysOpen') === true) {
                    businessDays.wednesday = 'Open24h';
                    businessHours++;
                } else {
                    businessDays.wednesday = {
                        start: req.param('userWedStart'),
                        end: req.param('userWedEnd')
                    };
                    businessHours++;
                }

            } else {
                businessDays.wednesday = 'Close';
            }

            if (req.param('userThuStart') != 'Close') {
                if (req.param('userAlwaysOpen') === true) {
                    businessDays.thursday = 'Open24h';
                    businessHours++;
                } else {
                    businessDays.thursday = {
                        start: req.param('userThuStart'),
                        end: req.param('userThuEnd')
                    };
                    businessHours++;
                }
            } else {
                businessDays.thursday = 'Close';
            }

            if (req.param('userFriStart') != 'Close') {
                if (req.param('userAlwaysOpen') === true) {
                    businessDays.friday = 'Open24h';
                    businessHours++;
                } else {
                    businessDays.friday = {
                        start: req.param('userFriStart'),
                        end: req.param('userFriEnd')
                    };
                    businessHours++;
                }
            } else {
                businessDays.friday = 'Close';
            }

            if (req.param('userSatStart') != 'Close') {
                if (req.param('userAlwaysOpen') === true) {
                    businessDays.saturday = 'Open24h';
                    businessHours++;
                } else {
                    businessDays.saturday = {
                        start: req.param('userSatStart'),
                        end: req.param('userSatEnd')
                    };
                    businessHours++;
                }
            } else {
                businessDays.saturday = 'Close';
            }

            if (profilePhoto === undefined) profilePhoto = 'https://s3-us-west-2.amazonaws.com/uhome/avatar.png';
            if (coverBanner === undefined) coverBanner = 'https://s3-us-west-2.amazonaws.com/uhome/timeline.jpg';

            Contractor.update({
                email: req.param('email'),
                activationToken: req.param('token')
            }, {
                firstName: req.param('userFirstName'),
                lastName: req.param('userLastName'),
                phone: req.param('userWorkPhone'),
                address: req.param('userStreetAddress'),
                city: req.param('userCity'),
                state: req.param('userState'),
                longitute: req.param('userLng'),
                latitude: req.param('userLat'),
                country: 'US',
                zipcode: req.param('userZipcode'),
                businessDays: businessDays,
                businessHours: businessHours,
                hourlyRate: req.param('userHourlyRate'),
                is24Hours: req.param('userAlwaysOpen'),
                url: req.param('userWebsiteURL'),
                userType: req.param('userType'),
                aboutUs: req.param('aboutUs'),
                profilePicture: profilePhoto,
                bannerPicture: coverBanner,
                emailVerified: true,
                status: 'Active',
                online: true,
                lastLogin: new Date(),
                lastLoginIP: req.ip
            }).exec((err, user) => {

                if (err) return res.json({
                    err: err,
                    msg: 'Profile update failed'
                });

                req.session.me = user[0].id;
                res.cookie('token', user[0].activationToken, {
                    expires: new Date(Number(new Date()) + 10000),
                    httpOnly: false,

                });
                req.session.userType = 'sp';
                req.session.businessType = 'individual';

                return res.json({
                    status: 200,
                    success: true
                })
            });


        }});


}

function updateContractor(req, res) {
    let businessDays = {},
        businessHours = 0,
        profilePhoto = undefined,
        coverBanner = undefined;

    if (req.param('sunStart') != 'Close') {
        if (req.param('is24Hours') === true) {
            businessDays.sunday = '24h';
            businessHours++;
        } else {
            businessDays.sunday = {
                start: req.param('sunStart'),
                end: req.param('sunEnd')
            };
            businessHours++;
        }
    } else {
        businessDays.sunday = 'Close';
    }

    if (req.param('monStart') != 'close') {
        if (req.param('is24Hours') === true) {
            businessDays.monday = '24h';
            businessHours++;
        } else {
            businessDays.monday = {
                start: req.param('monStart'),
                end: req.param('monEnd')
            };
            businessHours++;
        }

    } else {
        businessDays.monday = 'Close';
    }

    if (req.param('tueStart') != 'Close') {
        if (req.param('is24Hours') === true) {
            businessDays.tuesday = '24h';
            businessHours++;
        } else {
            businessDays.tuesday = {
                start: req.param('tueStart'),
                end: req.param('tueEnd')
            };
            businessHours++;
        }
    } else {
        businessDays.tuesday = 'Close';
    }

    if (req.param('wedStart') != 'Close') {
        if (req.param('is24Hours') === true) {
            businessDays.wednesday = '24h';
            businessHours++;
        } else {
            businessDays.wednesday = {
                start: req.param('wedStart'),
                end: req.param('wedEnd')
            };
            businessHours++;
        }

    } else {
        businessDays.wednesday = 'Close';
    }

    if (req.param('thuStart') != 'Close') {
        if (req.param('is24Hours') === true) {
            businessDays.thursday = '24h';
            businessHours++;
        } else {
            businessDays.thursday = {
                start: req.param('thuStart'),
                end: req.param('thuEnd')
            };
            businessHours++;
        }
    } else {
        businessDays.thursday = 'Close';
    }

    if (req.param('friStart') != 'Close') {
        if (req.param('is24Hours') === true) {
            businessDays.friday = '24h';
            businessHours++;
        } else {
            businessDays.friday = {
                start: req.param('friStart'),
                end: req.param('friEnd')
            };
            businessHours++;
        }
    } else {
        businessDays.friday = 'Close';
    }

    if (req.param('satStart') != 'Close') {
        if (req.param('is24Hours') === true) {
            businessDays.saturday = '24h';
            businessHours++;
        } else {
            businessDays.saturday = {
                start: req.param('satStart'),
                end: req.param('satEnd')
            };
            businessHours++;
        }
    } else {
        businessDays.saturday = 'Close';
    }

    Contractor.update({
        email: req.param('email'),
        activationToken: req.param('token')
    }, {
        firstName: req.param('userFirstName'),
        lastName: req.param('userLastName'),
        phone: req.param('userWorkPhone'),
        address: req.param('userStreetAddress'),
        city: req.param('userCity'),
        state: req.param('userState'),
        longitute: req.param('userLng'),
        latitude: req.param('userLat'),
        country: 'US',
        zipcode: req.param('userZipcode'),
        businessDays: businessDays,
        businessHours: businessHours,
        hourlyRate: req.param('userHourlyRate'),
        is24Hours: req.param('userAlwaysOpen') !== undefined,
        url: req.param('userWebsiteURL'),
        userType: req.param('userType'),
        aboutUs: req.param('aboutUs'),
        profilePicture: profilePhoto,
        bannerPicture: coverBanner,
        emailVerified: true,
        status: 'Active',
        online: true,
        lastLogin: new Date(),
        lastLoginIP: req.ip
    }).exec((err, user) => {

        if (err) return res.json({
            err: err,
            msg: 'Profile update failed'
        });

        req.session.me = user[0].id;
        req.session.activationToken = user[0].activationToken;
        req.session.userType = 'sp';
        req.session.businessType = 'individual';

        return res.json({
            status: 200,
            success: true
        })
    });
}
