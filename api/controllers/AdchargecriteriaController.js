'use strict';
/**
 * AdchargecriteriaController
 *
 * Date            :: June 28th, 2016
 * @description :: Server-side logic for managing adchargecriterias
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

    //TODO March 1st, 2017 - functions below are used nowhere on the front-end

    // Action for fetching adchargecriteria by ID
    fetch: (req, res, next) => {

        Adchargecriteria.find().exec((err, adchargecriteria) => {

            if (err) {
                ErrorHandler.errorResponse(err, req, res, next);
            } else{
                res.json(adchargecriteria);
            }
        });
    },

    // Action for updating a specified adchargecriteria
    update: (req, res, next) => {

        Adchargecriteria.update(req.param('id'), req.params.all(), (err, adchargecriteria) => {

            if (err) {
                ErrorHandler.errorResponse(err, req, res, next);
            } else{
                res.json(adchargecriteria);
            }
        });
    }
};