'use strict';
/**
 * ResendEmailController
 *
 * @description :: Server-side logic for managing Resendemails
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

    resendEmail: (req, res) => {

        let userType = undefined,
            activationTokenKey = undefined,
            email = undefined,
            userId = undefined;

        Contractor.findOne({
            email: req.param('email')
        }).exec((err, contractor) => {
            Serviceseeker.findOne({
                email: req.param('email')
            }).exec((err, serviceseeker) => {

                userType = contractor === undefined ? 'user' : 'business';
                activationTokenKey = contractor === undefined ? serviceseeker.activationToken : contractor.activationToken;
                email = contractor === undefined ? serviceseeker.email : contractor.email;
                userId = contractor === undefined ? serviceseeker.id : contractor.id;

                if(userType === 'user'){
                    Serviceseeker.update({
                            email: email,
                            activationToken: activationTokenKey},
                        {invited: true
                        }).exec((err, user) => {

                        if (!user) {
                            return res.view('common/homepage', {
                                notFound: "Account not found!"
                            });
                        } else {
                            EmailService.sendActivationEmail(email, activationTokenKey, userType);

                            return res.json({
                                id: userId
                            });
                        }
                    });
                } else {
                    Contractor.update({
                            email: email,
                            activationToken: activationTokenKey
                        },
                        {
                            invited: true
                        }).exec((err, user) => {

                        if (!user) {
                            return res.view('common/homepage', {
                                notFound: "Account not found!"
                            });
                        } else {
                            EmailService.sendActivationEmail(email, activationTokenKey, userType);

                            return res.json({
                                id: userId
                            });
                        }
                    });
                }

            });
        });

    }
	
};

