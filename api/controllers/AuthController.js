'use strict';
/**
 * AuthController.js
 *
 * Date     :: June 28th, 2016
 * @description ::
 * @docs        :: http://sailsjs.org/#!documentation/controllers
 */

let passport = require('passport');

module.exports = {

    index: (req, res) => {
        res.json({'status': 'home page'});
    },

    logout: (req, res) => {
        req.logout();
        res.json({'status': 'logged out!'});
    },

    google: (req, res) => {
        passport.authenticate(
            'google', {
                failureRedirect: '/login'
                , scope: ['https://www.googleapis.com/auth/userinfo.email'
                    , 'https://www.googleapis.com/auth/userinfo.profile']
            }
            , (err, user) => {
                let authType = 'google';
                finishAuthViaSocialNetworks(res, err, user, authType);

            })(req, res);
    },

    facebook: (req, res) => {
        passport.authenticate(
            'facebook'
            , (err, user) => {
                let authType = 'facebook';
                finishAuthViaSocialNetworks(res, err, user, authType);

            })(req, res);
    }
};


function finishAuthViaSocialNetworks(res, err, user, authType) {

    if (err) {
        console.log('Error in AuthController: ' + err);
        res.view('404');
        return;
    }

    Serviceseeker.findOne({
        email: user.email
    }).exec((err) => {

        if (err) {
            console.log('Error in AuthController: ' + err);
            res.view('404');
            return;
        }

        Contractor.findOne({
            email: user.email
        }).exec((err) => {

            if (err) {
                console.log('Error in AuthController: ' + err);
                res.view('404');
                return;
            }

            res.cookie('token', user.activationToken, {
                expires: new Date(Number(new Date()) + 10000),
                httpOnly: false,

            });
            res.cookie('email', user.email, {
                expires: new Date(Number(new Date()) + 10000),
                httpOnly: false,

            });

            res.cookie('authType', authType, {
                expires: new Date(Number(new Date()) + 10000),
                httpOnly: false,

            });
            res.view('common/empty');
        });

    });
}
