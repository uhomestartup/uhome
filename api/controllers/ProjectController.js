'use strict';
/**
 * ServiceseekerprojectController
 *
 * @description :: Server-side logic for managing Serviceseekerprojects
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

let _ = require("lodash");

module.exports = {

    newProjectPage: (req, res, next) => {
        if (!req.session.me) {
            return res.view('common/homepage');
        }

        Serviceseeker.find({
            activationToken: req.session.activationToken
        }, {fields:{activationToken: 0}}).exec((err, serviceseeker) => {

            if (err) ErrorHandler.errorResponse(err, req, res, next);

            Worklines.find((err, workLines) => {

                if (err) ErrorHandler.errorResponse(err, req, res, next);

                Subservices.find((err, subServices) => {
                    if (err) ErrorHandler.errorResponse(err, req, res, next);

                    return res.view('ss/newProject', {
                        user: serviceseeker[0],
                        subServices: subServices.length === 0 ? JSON.stringify([{'name':'Select'},{'name': 'Others'}]) : JSON.stringify(subServices),
                        workLines: JSON.stringify(workLines)});
                });
            });
        });
    },

    editProjectPage: (req, res, next) => {
        if (!req.session.me) {
            return res.view('common/homepage');
        }

        Serviceseeker.findOne({
            activationToken: req.session.activationToken
        }).exec((err, serviceseeker) => {

            Projects.findOne({
                id: req.param('id'),
                serviceSeekerId: serviceseeker.id
            }, (err, project) => {
                if (err) ErrorHandler.errorResponse(err, req, res, next);

                Worklines.find((err, workLines) => {

                    if (err) ErrorHandler.errorResponse(err, req, res, next);

                    Subservices.find((err, subServices) => {
                        if (err) ErrorHandler.errorResponse(err, req, res, next);

                        return res.view('ss/editProject', {
                            user: serviceseeker,
                            subServices: subServices.length === 0 ? JSON.stringify([{'name':'Select'},{'name': 'Others'}]) : JSON.stringify(subServices),
                            workLines: JSON.stringify(workLines),
                            project: JSON.stringify(project),
                            projectWorkline:project.service,
                            projectSubService:project.subService
                        });
                    });

                });
            });

        });
    },

    createProject: (req, res, next) => {

        ImageUploadService.uploadImage(req, res, (result, err) => {
            let data = {
                service:          req.param('service'),
                subService:       req.param('subService'),
                serviceSeekerId:  req.session.me,
                jobName:          req.param('jobName'),
                jobDescription:   req.param('jobDescription'),
                estimatedBudget:  req.param('estimatedBudget'),
                startTime:        req.param('startTime') === " " ? "" : req.param('startTime'),
                attachments:      result.imageData === undefined ? [] : result.imageData

            };

            Projects.create(data, (err, project) => {
                if (err) {
                    ErrorHandler.errorResponse(err, req, res, next);
                } else{
                    res.json({status: 'success'});
                }
            });
        });


    },

    updateProject: (req, res, next) => {

        Projects.find({
            id: req.param('projectId')
        },(err, project) => {
            if (err) {
                ErrorHandler.errorResponse(err, req, res, next);
            } else{

                let projectAttachments = project[0].attachments;

                ImageUploadService.uploadImage(req, res, (result, err) => {

                    let data = {
                        service:          req.param('service'),
                        subService:       req.param('subService'),
                        serviceSeekerId:  req.session.me,
                        jobName:          req.param('jobName'),
                        jobDescription:   req.param('jobDescription'),
                        estimatedBudget:  req.param('estimatedBudget'),
                        startTime:        req.param('startTime'),
                    };

                    data.attachments = result.imageData !== undefined ?  _.concat(projectAttachments, result.imageData) : projectAttachments;

                    Projects.update({
                        id: req.param('projectId')
                    },data, (err, project) => {
                        if (err) {
                            ErrorHandler.errorResponse(err, req, res, next);
                        } else{
                            res.json({status: 'success'});
                        }
                    });
                })
            }
        });

       ;




    },

    deleteProject: (req, res, next) => {

        Projects.destroy({
            id: req.param('projectId')
        }, (err, project) => {
            if (err) {
                ErrorHandler.errorResponse(err, req, res, next);
            } else{
                res.json({test: 3});
            }
        });
    },

    getAllProjects: (req, res, next) => {

        Projects.find((err, projects) => {
            if (err) {
                ErrorHandler.errorResponse(err, req, res, next);
            } else{
                res.json({test: 5});
            }

        });
    }
};

