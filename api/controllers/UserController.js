'use strict';
/**
 * UserController
 *
 * Author        :: Talib Allauddin
 * Email        :: talib@bitcloudglobal.com
 * Date            :: June 23rd, 2016
 * @description :: Server-side logic for managing users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

    zipcode: (req, res, next) => {
        UtilityHandler.verifyZipcode(req.param('zipcode'), (result) => {
            if (result.status == 200) {
                return res.json({
                    lng: result.data.lng,
                    lat: result.data.lat,
                    state: result.data.state,
                    city: result.data.city
                });
            } else if (result.status == 404) {
                return res.notFound({msg: result.error});
            } else {
                return res.notFound({msg: 'Invalid zipcode!'});
            }
        });
    },

    //TODO March 1st, 2017 - functions below did not tested
    create: (req, res, next) => {

        User.create(req.params.all(), (err, user) => {

            if (err) ErrorHandler.errorResponse(err, req, res, next);

            res.json(user);
        });
    },

    fetch: (req, res, next) => {

        let sortBy = req.param('sortBy') + ' ' + req.param('orderBy'),
            recordLimit = req.param('limit'),
            page = req.param('page');

        if (page === undefined) {
            let page = 0;
        } else {
            page = (page - 1) * recordLimit;
        }

        let conditionsApply = req.param('data');

        if (conditionsApply === undefined) {
            conditionsApply = {};
        }

        User.find({where: conditionsApply, sort: sortBy, limit: recordLimit, skip: page}).exec(function (err, users) {

            if (err) ErrorHandler.errorResponse(err, req, res, next);

            res.json({
                users: users
            });
        });
    },

    fetchById: (req, res, next) => {

        User.find({
            id: req.param('id'),
            deleted: req.param('deleted')
        }).exec((err, user) => {

            if (err) ErrorHandler.errorResponse(err, req, res, next);

            res.json(user);
        });
    },

    update: (req, res, next) => {

        User.update(req.param('id'), req.params.all(), (err, user) => {

            if (err) ErrorHandler.errorResponse(err, req, res, next);

            res.json(user);
        });
    },

    destroy: (req, res, next) => {
        let token = 'abc123';

        User.update({
            email: req.param('email')
        }, {
            token: token
        }).exec((err, user) => {

            if (err) ErrorHandler.errorResponse(err, req, res, next);

            res.json(user);
        });
    },

    recovery: (req, res, next) => {
        let emailRecovery = req.param('email');
        let token = 'abc123212513';
        let subject = 'recovery from uhome';
        let host = sails.config.hostname;
        let message = '<h1> Click link below for activate your user account </h1><br><br>';
        message += '<a href= "' + host + 'user/activation?email=' + emailRecovery + '&token=' + token + '">click here to activate</a>';

        EmailHandler.sendEmail('talib@bitcloudglobal.com', [emailRecovery], subject, message);

        User.update({
            email: req.param('email')
        }, {
            token: token,
            status: 'Active'
        }).exec((err, user) => {

            if (err) ErrorHandler.errorResponse(err, req, res, next);

            res.json(user);
        });
    },

    activation: (req, res, next) => {

        let emailRecovery = req.param('email');
        let token = 'abc123212513';
        let subject = 'account activated';
        let message = 'Your user password is changed and user account is activated...';


        User.find({email: emailRecovery}, {token: token}).exec((err, user) => {
            if (err) {
                ErrorHandler.errorResponse(err, req, res, next);
            }
            else {
                User.update({email: req.param('email')}, {encryptedPassword: 'subhanshah'}).exec((err1, user1) => {
                    if (err1) {
                        ErrorHandler.errorResponse(err1, req, res, next);
                    }
                    res.json(user1);

                    EmailHandler.sendEmail('talib@bitcloudglobal.com', [emailRecovery], subject, message);
                });
            }
        });
    }

};
