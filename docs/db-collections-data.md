
# Languages
db.languages.insert([
    {languagesName: "English - [Canada]"},
    {languagesName: "English - [United States]"},
    {languagesName: "English - [United Kingdom]"}
    ])

# Accreditation types
db.accreditations.insert([
    {name:"ACR-SS | Alarm (Single Station - Certificate of Registrartion)"},
    {name:"Alarm (Branch Office - Certificate of Registration)"},
    {name:"Alarm (Certificate of Registration)"},
    {name:"Alarm Systems (Company - Class B)"},
    {name:"Alarm Systems (Company)"},
    {name:"Appliance Installation Contractor (Residential)"},
    {name:"Architecture (Business - Registration)"},
    {name:"Asbestos Agency (Consultant)"},
    {name:"Asbestos Operations & Maintenance Contractor (Restricted)"},
    {name:"Asbestos Transporters"},
    {name:"Electrical Contractor"},
    {name:"Electrical Sign Contractor"},
    {name:"Electrician (Journeyman)"},
    {name:"Electrician (Master)"},
    {name:"Elevator Contractor"},
    {name:"Environmental Air Conditioning Contractor (Class A)"},
    {name:"Environmental Air Conditioning Contractor (Class B)"},
    {name:"Interior Design (Business - Registration)"},
    {name:"Interior Designer"},{name:"Land Surveying (Firm)"},
    {name:"Landscape Architecture (Business - Registration)"},
    {name:"Landscape Irrigator"},
    {name:"Lead Abatement (Firm - Certification)"},
    {name:"Locksmith (Company - Class B)"},
    {name:"Locksmith (Company)"},
    {name:"LSLS | Land Surveying (State License)"},
    {name:"Pest Control (Business)"},
    {name:"Plumber (Master - Responsible)"},
    {name:"Professional Engineering (Firm)"},
    {name:"Refrigeration, Process Cooling & Heating (Class A - Commercial)"},
    {name:"Refrigeration, Process Cooling & Heating (Class B - Commercial)"},
    {name:"RPLS | Professional Land Surveyor (Registered)"},
    {name:"Security Consultant (Company - Class B)"}
    ])

# Worklines
db.worklines.insert([
    {name: "Housekeeping"},
    {name: "Music"},
    {name: "Cooking"}
    ])

# Insert the new performer
db.performers.insert({
 projectId:       "58b827de7144b4570b4edae1",
 contractorId:    "58b82258771927ee04aa6474",
 status:          "Applicant",
 description:     "asddfsafasdfjasdklla alsdfjklasdfl sajdfljkasdjfklasdjfasdlf as fasfad"
 })
