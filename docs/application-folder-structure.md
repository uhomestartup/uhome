# Application Folder Structure

As this application is using `SailsJS` framework and its using the same folder structure as of `SailsJS`.

```
|-- uhome
    |-- api
    |   |-- controllers
    |   |-- models
    |   |-- policies
    |   |-- responses
    |   |-- services
    |-- assets
    |   |-- images
    |   |-- js
    |   |   |-- dependencies
    |   |-- styles
    |   |-- templates
    |-- config
    |   |-- env
    |   |-- locales
    |-- docs
    |-- files
    |-- tasks
    |   |-- config
    |   |-- register
    |-- views
```

For the `api` folder, it will contain the `MongoDB` schema in a sub-directory called `models` with their different life-cycle events. Another sub-directory `controllers` will contain the route actions and the ORM interface.

In the `policies` folder all the users permission related activities are performed, here we can grant a user to perform an specific task or we can restrict a user from accessing some parts of the application.

In the `responses` folder we have sails defined default responses also here we can define our custom response that can be used globaly in the application. After this we have a `services` folder where all the services can be loaded, initialized and used. Like if we have a Email or SMS service we can instantiate it here can be used in our actions without using any require statement.

Moving on to the next folder we have `assets` and as the name suggest it contains all the resources like Images, Stylesheets, Javascripts and Templates.

In the `js` folder we'll have `controllers` and `services` for `AngularJS`.

For the `config` folder, it contains all the application configurations like database, environment(dev, prod), csrf and others. In `docs` folder we have all the related documentation of the project. Similarly in the `files` folder we have project related files.

In `tasks` folder we have Grunts configuration and registered tasks, if we need to change anything related to grunt we'll refer to this folder.

For `views` we have all the templating and base layout of the applciation.