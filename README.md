# uhome

[![bitHound Code](https://www.bithound.io/projects/badges/7ed07540-3380-11e6-ae47-0bd4ac258deb/code.svg)](https://www.bithound.io/bitbucket/taliba/uhome) [![bitHound Overall Score](https://www.bithound.io/projects/badges/7ed07540-3380-11e6-ae47-0bd4ac258deb/score.svg)](https://www.bithound.io/bitbucket/taliba/uhome)

UHome is a web-based social media platform that is solely deal in home related services for the users and local service providers. The target of UHome is to connect service seekers and service providers through an online platform where they share ideas, gain community updates, and complete various home improvement projects and tasks. Unlike other platforms that are connecting service provider to the service seeker where service seeker publish their requirement and service provider send them offer for the services that a customer seeking for, UHome have service seeker to have advices from  home experts, layman and the service providing company to help them taking decision about their house hold works for the improvements via social media platform. 

# Requirements

 - [NodeJS](https://nodejs.org/en/)
 - [MongoDB](https://www.mongodb.com)
 - [SailsJS](http://sailsjs.org/)
 - [Python 2.7](https://www.python.org)
 - [Visual C++ Redistributable for Visual Studio 2012](https://www.microsoft.com/en-pk/download/details.aspx?id=30679)

### Optional

 - [RoboMongo - A GUI for MongoDB](https://robomongo.org/)

# How to setup

To setup the application you'll need the install [`SailsJS`](http://sailsjs.org/) globally using the following command
```
	npm install -g sails
```

After that pull the repo from `https://taliba@bitbucket.org/taliba/uhome.git`.

And run the command `npm install`, after installation of all the dependencies simply run the command
```
	sails lift
```

Now, you should have the project up and running.

# Application Structure

[application-folder-structure](docs/application-folder-structure.md)

# Tests

# License

# Install pm2
sudo npm install pm2 -g

# Start application
sudo pm2 start npm -- start

# Register autostart with operation system (ubuntu)
sudo pm2 startup ubuntu